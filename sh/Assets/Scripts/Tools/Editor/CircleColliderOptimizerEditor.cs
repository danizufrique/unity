using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(CircleColliderOptimizer))]
public class CircleColliderOptimizerEditor : Editor {
    public override void OnInspectorGUI()
    {
        CircleColliderOptimizer myTarget = (CircleColliderOptimizer)target;

        myTarget.radius = EditorGUILayout.FloatField("Radius",myTarget.radius);
        myTarget.nPoints = EditorGUILayout.IntField("Points",myTarget.nPoints);

        if (GUILayout.Button("Optimize"))
        {
            myTarget.Optimize();
            EditorUtility.SetDirty(myTarget);
        }


        if (EditorGUI.EndChangeCheck())
        {
            myTarget.Optimize();
        }
    }
}