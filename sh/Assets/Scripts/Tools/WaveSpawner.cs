using UnityEngine;
using System.Collections;

public class WaveSpawner : MonoBehaviour {

	public Wave[] waves;
	private int nextWave = 0;
	public float timeBetweenWaves;
	private float searchCountdown = 1f;

	void Update()
	{
		if (Controller.state == Controller.GameState.WAITING)   // ESPERANDO A QUE JUGADOR ACABE
		{
			if (Controller.aliveTargets == 0 && Controller.state == Controller.GameState.WAITING)
				WaveCompleted();
		}
		else	// RONDA ACABADA
		{
			if (Controller.state != Controller.GameState.SPAWNING)
			{
				SpawnWave ( waves[nextWave] );
			}
		}
	}

	void WaveCompleted()
	{
		//Debug.Log("Wave Completed!");
		timeBetweenWaves -= Time.deltaTime;

		if (timeBetweenWaves <= 0)
		{

			Controller.state = Controller.GameState.COUNTING;

			if (nextWave + 1 > waves.Length - 1)
			{
				nextWave = 0;
				Debug.Log("ALL WAVES COMPLETE! Looping...");
			}
			else
			{
				nextWave++;
			}
		}
	}

	void SpawnWave(Wave _wave)
	{
		Controller.state = Controller.GameState.SPAWNING;
		timeBetweenWaves = _wave.timeAfterWave;

		_wave.StartRound();	
	}

	void SpawnEnemy(Transform _enemy)
	{
		Debug.Log("Spawning Enemy: " + _enemy.name);
		Vector2[][] spawnPoints = Controller.getSpawnPoints();

		Vector2 _sp = spawnPoints[ Random.Range (0, spawnPoints.Length) ][Random.Range(0, spawnPoints[0].Length)];
		Instantiate(_enemy, _sp, Quaternion.identity);
	}

}
