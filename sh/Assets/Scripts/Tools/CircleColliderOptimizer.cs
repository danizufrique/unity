using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleColliderOptimizer : MonoBehaviour
{
    PolygonCollider2D col;
    public float radius;
    public int nPoints;

    public float getRadius() { return radius; }
    public void Optimize(){
        col = GetComponent<PolygonCollider2D>();

        col.pathCount = 1;
        Vector2[] points = new Vector2[nPoints];
        float ratio = 2*Mathf.PI/nPoints;
        Vector2 coordinates;

        for(int i=0; i<nPoints; i++){
            coordinates = new Vector2(Mathf.Cos(i*ratio), Mathf.Sin(i*ratio));
            points[i] = coordinates  * radius;
        }

        col.points = points;
    }
}