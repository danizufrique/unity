using UnityEngine;
using System.Collections;

[System.Serializable]
public class Wave {
    
    Controller controller;
    public Controller.WaveType type;
    public string name;
    public GameObject enemy;
    public int count;
    public float rate;
    public float timeAfterWave;
    public float timeBetweenSpawn;

    SpawnManager spawnManager;

    public virtual void StartRound()
    {
        Debug.Log("STARTING ROUND" + name);
        if(!controller) controller = GameObject.FindGameObjectWithTag("Management").GetComponent<Controller>();
        
        controller.ProcessWave(this);
        Controller.state = Controller.GameState.WAITING;
    }

}