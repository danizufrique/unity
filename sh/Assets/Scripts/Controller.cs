﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controller : MonoBehaviour
{
    public enum TargetType { Wood, Gold }
    public enum WaveType { Full, GridEdges, ScreenEdges, Corners, Other }
    public enum GameState { SPAWNING, WAITING, COUNTING };
    public static GameState state { get; set; } = GameState.COUNTING;
    public static int aliveTargets { get; set; } = 0;

    private static int playerPoints = 0;

    public static Vector2[][] getSpawnPoints()
    {
        return GameObject.FindGameObjectWithTag("Management").GetComponent<GridManager>().CalculateSpawners();
    }

    public static void addPoints(int p)
    {
        playerPoints += p;

        // if record ...
    }

    public static Vector2 topLeftCorner()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.nearClipPlane));
    }
        
    public static Vector2 topRightCorner()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
    }
        
    public static Vector2 botLeftCorner()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
    }
        
    public static Vector2 botRightCorner()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(1, 0, Camera.main.nearClipPlane));
    }

    public static bool PointVisible(Vector2 point)
    {
        bool result = true;

        if (point.x < botLeftCorner().x || point.x > botRightCorner().x)
            result = false;
            
        if (point.y < botLeftCorner().y || point.y > topRightCorner().y)
            result = false;

        return result;
    }

    public void ProcessWave(Wave wave)
    {
        SpawnManager spawnManager = GameObject.FindGameObjectWithTag("Management").GetComponent<SpawnManager>();
        Debug.Log(wave.type);
        switch (wave.type)
        {
            case WaveType.Full:
                StartCoroutine(spawnManager.Fill(getSpawnPoints()));
                break;
            case WaveType.GridEdges:
                StartCoroutine(spawnManager.GridEdges(getSpawnPoints()));
                break;
            case WaveType.ScreenEdges:
                StartCoroutine(spawnManager.ScreenEdges());
                break;
            case WaveType.Corners:
                StartCoroutine(spawnManager.Corners(getSpawnPoints()));
                break;
            case WaveType.Other:
                wave.StartRound();
                break;
            default:
                break;
        }
    }
}

