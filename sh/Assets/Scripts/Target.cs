﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Target : MonoBehaviour
{
    Animator anim;
    TextMeshProUGUI text;

    [SerializeField] Controller.TargetType type;
    [SerializeField] uint hitPoints = 1;
    [SerializeField] int points = 10;
    [SerializeField] float lifeTime = 4f;

    [Header("DEBUG")]
    public bool allowClick = false;

    private void Start()
    {
        //GetComponentInChildren<Explodable>().generateFragments();
        anim = GetComponent<Animator>();
        text = GetComponentInChildren<TextMeshProUGUI>();

        Controller.aliveTargets++; 

        UpdateUI();
    }

    private void Update() {
        lifeTime -= Time.deltaTime;  
        if(lifeTime <= 0)  
            Disappear();
    }
    
    public virtual void Hit() 
    {
        hitPoints--;

        if (hitPoints <= 0)
            Break();

        UpdateUI();
    }

    void Break()
    {
        Controller.addPoints(points);
        Controller.aliveTargets--;
        GetComponentInChildren<Explodable>().explode();
    }

    void Disappear(){
        anim.SetTrigger("Disappear");
    }

    private void UpdateUI()
    {
        text.text = hitPoints.ToString();
    }

    private void OnMouseDown()
    {
        if(allowClick)
            Hit();
    }

    void Destroy()
    {
        Controller.aliveTargets--;
        Destroy(gameObject);
    }
}
