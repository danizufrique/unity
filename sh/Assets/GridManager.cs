﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField] Vector2 cellSize = new Vector2(1, 1);
    private int nCol, nRow;
    private float restCol, restRow;

    public Vector2[][] CalculateSpawners()
    {
        Vector2[][] result;

        nCol = Mathf.RoundToInt((Controller.topRightCorner().x - Controller.topLeftCorner().x) / cellSize.x);
        nRow = Mathf.RoundToInt((Controller.topRightCorner().y - Controller.botRightCorner().y) / cellSize.y);

        restCol = Controller.topRightCorner().x - (Controller.topLeftCorner().x + nCol * cellSize.x);
        restRow = Controller.topRightCorner().y - (Controller.botRightCorner().y + nRow * cellSize.y);

        result = new Vector2[nCol][];
        for (int i = 0; i < result.Length; i++)
            result[i] = new Vector2[nRow];

        for(int i=0; i<nCol; i++)
        {
            for(int j=0; j<nRow; j++)
            {
                Vector2 pos = Controller.botLeftCorner() + new Vector2(restCol/2 + (i + 0.5f) * cellSize.x, restRow / 2 + (j+0.5f) * cellSize.y);
                result[i][j] = pos;
            }
        }

        return result;
    }

    private void OnDrawGizmos()
    {
        nCol = Mathf.RoundToInt((Controller.topRightCorner().x - Controller.topLeftCorner().x) / cellSize.x);
        nRow = Mathf.RoundToInt((Controller.topRightCorner().y - Controller.botRightCorner().y) / cellSize.y);

        restCol = Controller.topRightCorner().x - (Controller.topLeftCorner().x + nCol * cellSize.x);
        restRow = Controller.topRightCorner().y - (Controller.botRightCorner().y + nRow * cellSize.y);

        Vector2 from, to;

        Gizmos.color = Color.gray;

        for(int i=0; i<=nCol; i++)
        {
            from = Controller.botLeftCorner() + new Vector2(restCol/2 + i*cellSize.x,0);
            to = Controller.topLeftCorner() + new Vector2(restCol/2 + i * cellSize.x, 0);

            if (Controller.PointVisible(from))
            {
                Gizmos.DrawWireSphere(from, .2f);
                Gizmos.DrawWireSphere(to, .2f);
                Gizmos.DrawLine(from, to);
            }
        }

        for (int i = 0; i <= nRow; i++)
        {
            from = Controller.botLeftCorner() + new Vector2(0, restRow/2 + i * cellSize.y);
            to = Controller.botRightCorner() + new Vector2(0, restRow/2 + i * cellSize.y);

            if (Controller.PointVisible(from))
            {
                Gizmos.DrawWireSphere(from, .2f);
                Gizmos.DrawWireSphere(to, .2f);
                Gizmos.DrawLine(from, to);
            }
        }

        Gizmos.color = Color.blue;
        Vector2[][] points = CalculateSpawners();

        for (int i = 0; i < points.Length; i++)
        {
            for (int j = 0; j < points[0].Length; j++)
            {
                Gizmos.DrawCube(points[i][j], new Vector3(.2f, .2f, 0));

            }
        }
    }

    public int getNCol()
    {
        return nCol;
    }

    public int getNRow()
    {
        return nRow;
    }
}
