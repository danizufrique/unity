﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundController : MonoBehaviour
{
    private SpawnManager spawnManager;
    private GridManager gridManager;

    void Start()
    {
        spawnManager = GetComponent<SpawnManager>();
        gridManager = GetComponent<GridManager>();

        StartCoroutine(spawnManager.Fill(gridManager.CalculateSpawners(), .1f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
