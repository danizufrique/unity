﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] GameObject prefab;

    public IEnumerator Fill(Vector2[][] spawnPoints, float timeBetweenSpawn=0)
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            for (int j = 0; j < spawnPoints[0].Length; j++)
            {
                Instantiate(prefab, spawnPoints[i][j], Quaternion.identity);
                yield return new WaitForSeconds(timeBetweenSpawn);
            }
        }
    }

    public IEnumerator DiagonalInGrid(Vector2[][] spawnPoints, int nCol, int nRow, float timeBetweenSpawn=0)
    {
        int min = Mathf.Min(nCol, nRow);
        yield break;
    }

    public IEnumerator Corners(Vector2[][] spawnPoints, float timeBetweenSpawn=0)
    {
        int nCol = spawnPoints[0].Length;
        int nRow = spawnPoints.Length;

        Instantiate(prefab, spawnPoints[0][0], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[0][nRow-1], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[nCol-1][0], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[nCol - 1][nRow - 1], Quaternion.identity);
    }

    public IEnumerator GridEdges(Vector2[][] spawnPoints, float timeBetweenSpawn=0)
    {
        int nCol = spawnPoints[0].Length;
        int nRow = spawnPoints.Length;

        Instantiate(prefab, spawnPoints[0][nRow/2], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[nCol-1][nRow/2], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[nCol/2][0], Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, spawnPoints[nCol/2][nRow - 1], Quaternion.identity);
    }

    public IEnumerator ScreenEdges(float timeBetweenSpawn=0)
    {
        float halfY = Controller.botLeftCorner().y + 0.5f * (Controller.topLeftCorner().y - Controller.botLeftCorner().y);
        float halfX = Controller.botLeftCorner().x + 0.5f * (Controller.botRightCorner().x - Controller.botLeftCorner().x);
        float targetRadius = prefab.GetComponent<CircleColliderOptimizer>().getRadius();

        Instantiate(prefab, new Vector2(targetRadius + Controller.botLeftCorner().x, halfY), Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, new Vector2(Controller.botRightCorner().x - targetRadius, halfY), Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, new Vector2(halfX, Controller.botLeftCorner().y + targetRadius), Quaternion.identity);
        yield return new WaitForSeconds(timeBetweenSpawn);
        Instantiate(prefab, new Vector2(halfX, Controller.topLeftCorner().y - targetRadius), Quaternion.identity);
    }


}
