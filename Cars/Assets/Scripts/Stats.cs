﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats:MonoBehaviour
{
    public Stats()
    {
        peso = 0;
        freno = 0;
        manejo = 0;
        salto = 0;
        derrape = 0;
        turbo = 0;
    }
    public float peso;
    public float freno;
    public float manejo;
    public float salto;
    public float derrape;
    public float turbo;


}
