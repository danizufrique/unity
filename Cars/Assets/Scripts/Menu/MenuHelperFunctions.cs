﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class MenuHelperFunctions : MonoBehaviour
{
    [Header("Players")]
    public static bool[] playersReady = new bool[4] { false, false, false, false };
    public static bool[] playersJoined = new bool[4] { false, false, false, false };

    public GameObject[] carPrefabs;
    public GameObject carModel, statsHolder;


    [Header("Lobby Variables")]
    public UnityEvent OnOpenMenu;
    public int playerIndex = 1;
    public GameObject playerPanel;
    public int countdownDuration = 10;

    public string sceneToLoad;

    [Header("Stats")]
    public GameObject carSelect;
    public Stats stats;

    private void Start()
    {
        OnOpenMenu.Invoke();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    // Resetea cuentas atrás y pone a listo al siguiente jugador;
    public void PlayerJoined()
    {
        GameObject.Find("CountdownTimer").GetComponent<Text>().text = "";
        foreach (MenuHelperFunctions menuHelper in GameObject.FindObjectsOfType<MenuHelperFunctions>())
            menuHelper.StopCoroutine("LobbyCountdown");

        playersJoined[playerIndex - 1] = true;
    }

    public void NextCar()
    {
        Debug.Log("next");
    }

    public void PreviousCar()
    {
        Debug.Log("prev");
    }

    // Jugador listo. Si todos lo están inicia cuenta atrás
    public void PlayerReady()
    {
        playersReady[playerIndex - 1] = true;

        bool allPlayersReady = true;

        for (int i = 0; i < playersJoined.Length && allPlayersReady; i++)
        {
            if (playersJoined[i] == true)
                if (playersReady[i] == false)
                    allPlayersReady = false;
        }

        if (allPlayersReady && playersJoined.Length > 0)
            StartCoroutine("LobbyCountdown");
    }


    // Inicia cuentas atrás y carga escena dada
    IEnumerator LobbyCountdown()
    {
        int remainingTime = countdownDuration;

        for (int i = 0; i < countdownDuration; i++)
        {
            GameObject.Find("CountdownTimer").GetComponent<Text>().text = "Start\n" + remainingTime.ToString();
            yield return new WaitForSeconds(1);
            remainingTime--;
        }

        LoadScene(sceneToLoad);
    }

    public void EnableStats(GameObject carModel, GameObject statsHolder)
    {
        carModel.SetActive(true);
        statsHolder.SetActive(false);
    }

    public void OnCancel()
    {
        if (playersReady[playerIndex - 1] == true)  // Estaba listo
        {
            playerPanel.transform.Find("Get Ready Panel").transform.Find("ReadyButton").gameObject.SetActive(true);
            playerPanel.transform.Find("EventSystemPlayer").GetComponent<MultiplayerEventSystem>().SetSelectedGameObject(null);
            playerPanel.transform.Find("EventSystemPlayer").GetComponent<MultiplayerEventSystem>().SetSelectedGameObject(playerPanel.transform.Find("Get Ready Panel").transform.Find("ReadyButton").gameObject);

            playerPanel.GetComponent<Animator>().Play("RotatePlayerModel");
            playersReady[playerIndex - 1] = false;

            GameObject.Find("CountdownTimer").GetComponent<Text>().text = "";

            foreach (MenuHelperFunctions menuHelper in GameObject.FindObjectsOfType<MenuHelperFunctions>())
                menuHelper.StopCoroutine("LobbyCountdown");

            Debug.Log("Cancel Ready");
        }
        else if (playersJoined[playerIndex - 1] == true) // Estaba solo unido
        {
            playerPanel.transform.Find("Get Ready Panel").gameObject.SetActive(false);
            playerPanel.transform.Find("Join Panel").gameObject.SetActive(true);
            playerPanel.transform.Find("EventSystemPlayer").GetComponent<MultiplayerEventSystem>().SetSelectedGameObject(null);
            playerPanel.transform.Find("EventSystemPlayer").GetComponent<MultiplayerEventSystem>().SetSelectedGameObject(playerPanel.transform.Find("Join Panel").transform.Find("JoinButton").gameObject);

            playerPanel.GetComponent<Animator>().Play("RotatePlayerModel");
            playersJoined[playerIndex - 1] = false;

            Debug.Log("Cancel Join" + playerIndex);
        }
        else if (playerIndex == 1)   // Era el único en la sala
            LoadScene("Menu");
    }

}
