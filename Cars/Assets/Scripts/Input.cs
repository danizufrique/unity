// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Input : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Input()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Input"",
    ""maps"": [
        {
            ""name"": ""Drive"",
            ""id"": ""7a2858e3-2cb8-4050-8269-46cf82b49072"",
            ""actions"": [
                {
                    ""name"": ""Frenar"",
                    ""type"": ""Button"",
                    ""id"": ""dd56605e-6bb4-4ef3-922f-0924f7ade13e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Acelerar"",
                    ""type"": ""Button"",
                    ""id"": ""ac175c7e-b465-439f-bc3c-588c78585d53"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Girar"",
                    ""type"": ""Button"",
                    ""id"": ""27608ab5-4a2d-4ffc-832e-1e652433768d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Saltar"",
                    ""type"": ""Button"",
                    ""id"": ""a31d33f1-533d-4bad-a235-cc7bae3163a6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Turbo"",
                    ""type"": ""Button"",
                    ""id"": ""e05f43cb-2823-4a79-9ec4-06b2669c90d7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Derrapar"",
                    ""type"": ""Button"",
                    ""id"": ""49c8c1cc-c6bf-49e8-9e92-c61d3ccb6cc5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c3c9ab94-f455-4a09-a328-af916079320a"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""67367ae8-2522-4da7-9ee8-7aa2c8191311"",
                    ""path"": ""<Keyboard>/capsLock"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""b89a10a3-d366-4cac-884f-4bd81eba6e96"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""2b2b774e-f298-4895-af42-1df28d1ba13a"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""76874edd-ebf3-4d8a-adbc-3cfdfe4c8849"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""W-S"",
                    ""id"": ""69291c9b-5864-4905-8592-182550882a28"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""7fac7b94-6f70-47f7-97ee-0804f0b24717"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""0d2a4303-ea4e-4e26-bc6f-a747e233f3e9"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""LeftJoystick"",
                    ""id"": ""7a290c18-429a-4471-a211-bd2c1973d9ea"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f06d8766-0ab6-4efd-aa6e-1a349bde676f"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""47aae626-3175-494e-a866-b314fd034587"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""A-D"",
                    ""id"": ""45c9ba38-5540-4823-aa53-87f5b3138ec7"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""abe198e7-c3a4-4dc3-ac6e-39ee724051a1"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d89808eb-c44c-4067-950c-c37c6df59d02"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""99c5bcd4-1cd0-482b-a544-5181062ab58e"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Saltar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a1d6768d-fb10-4b57-a3f0-7030f0892f32"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Saltar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bd7af9d7-3ed6-490b-93b6-4ca344266945"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turbo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3fc20898-12fc-46d3-999f-5a320d4ca69d"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turbo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac238692-11b5-4f11-8e10-3a60401f1ef3"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derrapar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""16df180e-d4c5-4d67-875b-cf0d190759f3"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derrapar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Drive1"",
            ""id"": ""1650c3aa-667b-4c72-aa5c-46629062e7ac"",
            ""actions"": [
                {
                    ""name"": ""Frenar"",
                    ""type"": ""Button"",
                    ""id"": ""10d18233-b198-4f1e-9b5c-7cad70bb87ab"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Acelerar"",
                    ""type"": ""Button"",
                    ""id"": ""b3a4e883-391f-422e-b9a7-ee39aed445ad"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Girar"",
                    ""type"": ""Button"",
                    ""id"": ""f590d47a-6924-4a3f-827e-58b19a317e59"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Saltar"",
                    ""type"": ""Button"",
                    ""id"": ""f5402a80-f15a-4714-aff9-6df8c3c56dfa"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Turbo"",
                    ""type"": ""Button"",
                    ""id"": ""d8c261c3-a2b0-48e3-aa93-c0076890ee82"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Derrapar"",
                    ""type"": ""Button"",
                    ""id"": ""26df3295-06b7-4deb-8b18-31364482a0ac"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""870ea83d-3d1b-4c68-9073-ce856d46f707"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""576293fc-de8a-4c2a-9b4f-bb0e7b763fe2"",
                    ""path"": ""<Keyboard>/capsLock"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""8ff4d97c-b8fb-41bb-b319-003d29b86df5"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""804f2bed-d495-4997-997a-7aa460d4fc9a"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9dbe2089-8c58-489f-8462-d23e80508849"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""W-S"",
                    ""id"": ""23228b5e-1290-4ac8-ad3c-12889c04bdd3"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""a5f5734b-044b-45de-bb1b-3de663a11643"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9b4c34f5-7de8-4f97-9736-7b0833b993ec"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""LeftJoystick"",
                    ""id"": ""b3cf94e1-2057-47b1-b484-572bbf6452ba"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""fae2a630-4b9d-42be-9190-91238d39cac0"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""b107f7bc-fc45-48e2-9cd0-38be5419324b"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""A-D"",
                    ""id"": ""35b4b200-1cc1-4619-a236-880d4f4c79c8"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""1da4d19e-0983-4df7-8db3-a4b536a26162"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""05d69848-2f60-4a75-ba05-435f640b6756"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Girar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b08526f2-48bd-4eb8-ae7a-3685ec824a13"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Saltar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98d06d59-04c5-4b4d-b113-e5a1e0f24abb"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Saltar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca4e9c1c-bb1e-4d53-a535-509b9509b761"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turbo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f526cb3c-2197-484d-bd9f-cf58c1eb00fa"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turbo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6e4f5359-b92a-4789-81b6-04f93687b1b4"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derrapar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c9ec1943-fe6c-4f6e-9256-6273068394dc"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derrapar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI"",
            ""id"": ""6455f6ef-84ad-4140-bfbd-550ba796e165"",
            ""actions"": [
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""5feb757b-8f73-473f-a235-4f590f346067"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Submit"",
                    ""type"": ""Button"",
                    ""id"": ""d5e16a49-cc74-494e-a977-d9ff3c6626f1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""ff9eb1ea-9f54-42dd-9820-40a8c3cbf8d9"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e712e043-51d5-4493-a6f6-bf32cef4f810"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Submit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Drive
        m_Drive = asset.FindActionMap("Drive", throwIfNotFound: true);
        m_Drive_Frenar = m_Drive.FindAction("Frenar", throwIfNotFound: true);
        m_Drive_Acelerar = m_Drive.FindAction("Acelerar", throwIfNotFound: true);
        m_Drive_Girar = m_Drive.FindAction("Girar", throwIfNotFound: true);
        m_Drive_Saltar = m_Drive.FindAction("Saltar", throwIfNotFound: true);
        m_Drive_Turbo = m_Drive.FindAction("Turbo", throwIfNotFound: true);
        m_Drive_Derrapar = m_Drive.FindAction("Derrapar", throwIfNotFound: true);
        // Drive1
        m_Drive1 = asset.FindActionMap("Drive1", throwIfNotFound: true);
        m_Drive1_Frenar = m_Drive1.FindAction("Frenar", throwIfNotFound: true);
        m_Drive1_Acelerar = m_Drive1.FindAction("Acelerar", throwIfNotFound: true);
        m_Drive1_Girar = m_Drive1.FindAction("Girar", throwIfNotFound: true);
        m_Drive1_Saltar = m_Drive1.FindAction("Saltar", throwIfNotFound: true);
        m_Drive1_Turbo = m_Drive1.FindAction("Turbo", throwIfNotFound: true);
        m_Drive1_Derrapar = m_Drive1.FindAction("Derrapar", throwIfNotFound: true);
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_Cancel = m_UI.FindAction("Cancel", throwIfNotFound: true);
        m_UI_Submit = m_UI.FindAction("Submit", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Drive
    private readonly InputActionMap m_Drive;
    private IDriveActions m_DriveActionsCallbackInterface;
    private readonly InputAction m_Drive_Frenar;
    private readonly InputAction m_Drive_Acelerar;
    private readonly InputAction m_Drive_Girar;
    private readonly InputAction m_Drive_Saltar;
    private readonly InputAction m_Drive_Turbo;
    private readonly InputAction m_Drive_Derrapar;
    public struct DriveActions
    {
        private @Input m_Wrapper;
        public DriveActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @Frenar => m_Wrapper.m_Drive_Frenar;
        public InputAction @Acelerar => m_Wrapper.m_Drive_Acelerar;
        public InputAction @Girar => m_Wrapper.m_Drive_Girar;
        public InputAction @Saltar => m_Wrapper.m_Drive_Saltar;
        public InputAction @Turbo => m_Wrapper.m_Drive_Turbo;
        public InputAction @Derrapar => m_Wrapper.m_Drive_Derrapar;
        public InputActionMap Get() { return m_Wrapper.m_Drive; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DriveActions set) { return set.Get(); }
        public void SetCallbacks(IDriveActions instance)
        {
            if (m_Wrapper.m_DriveActionsCallbackInterface != null)
            {
                @Frenar.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnFrenar;
                @Frenar.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnFrenar;
                @Frenar.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnFrenar;
                @Acelerar.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnAcelerar;
                @Acelerar.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnAcelerar;
                @Acelerar.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnAcelerar;
                @Girar.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnGirar;
                @Girar.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnGirar;
                @Girar.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnGirar;
                @Saltar.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnSaltar;
                @Saltar.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnSaltar;
                @Saltar.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnSaltar;
                @Turbo.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnTurbo;
                @Turbo.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnTurbo;
                @Turbo.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnTurbo;
                @Derrapar.started -= m_Wrapper.m_DriveActionsCallbackInterface.OnDerrapar;
                @Derrapar.performed -= m_Wrapper.m_DriveActionsCallbackInterface.OnDerrapar;
                @Derrapar.canceled -= m_Wrapper.m_DriveActionsCallbackInterface.OnDerrapar;
            }
            m_Wrapper.m_DriveActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Frenar.started += instance.OnFrenar;
                @Frenar.performed += instance.OnFrenar;
                @Frenar.canceled += instance.OnFrenar;
                @Acelerar.started += instance.OnAcelerar;
                @Acelerar.performed += instance.OnAcelerar;
                @Acelerar.canceled += instance.OnAcelerar;
                @Girar.started += instance.OnGirar;
                @Girar.performed += instance.OnGirar;
                @Girar.canceled += instance.OnGirar;
                @Saltar.started += instance.OnSaltar;
                @Saltar.performed += instance.OnSaltar;
                @Saltar.canceled += instance.OnSaltar;
                @Turbo.started += instance.OnTurbo;
                @Turbo.performed += instance.OnTurbo;
                @Turbo.canceled += instance.OnTurbo;
                @Derrapar.started += instance.OnDerrapar;
                @Derrapar.performed += instance.OnDerrapar;
                @Derrapar.canceled += instance.OnDerrapar;
            }
        }
    }
    public DriveActions @Drive => new DriveActions(this);

    // Drive1
    private readonly InputActionMap m_Drive1;
    private IDrive1Actions m_Drive1ActionsCallbackInterface;
    private readonly InputAction m_Drive1_Frenar;
    private readonly InputAction m_Drive1_Acelerar;
    private readonly InputAction m_Drive1_Girar;
    private readonly InputAction m_Drive1_Saltar;
    private readonly InputAction m_Drive1_Turbo;
    private readonly InputAction m_Drive1_Derrapar;
    public struct Drive1Actions
    {
        private @Input m_Wrapper;
        public Drive1Actions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @Frenar => m_Wrapper.m_Drive1_Frenar;
        public InputAction @Acelerar => m_Wrapper.m_Drive1_Acelerar;
        public InputAction @Girar => m_Wrapper.m_Drive1_Girar;
        public InputAction @Saltar => m_Wrapper.m_Drive1_Saltar;
        public InputAction @Turbo => m_Wrapper.m_Drive1_Turbo;
        public InputAction @Derrapar => m_Wrapper.m_Drive1_Derrapar;
        public InputActionMap Get() { return m_Wrapper.m_Drive1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Drive1Actions set) { return set.Get(); }
        public void SetCallbacks(IDrive1Actions instance)
        {
            if (m_Wrapper.m_Drive1ActionsCallbackInterface != null)
            {
                @Frenar.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnFrenar;
                @Frenar.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnFrenar;
                @Frenar.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnFrenar;
                @Acelerar.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnAcelerar;
                @Acelerar.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnAcelerar;
                @Acelerar.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnAcelerar;
                @Girar.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnGirar;
                @Girar.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnGirar;
                @Girar.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnGirar;
                @Saltar.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnSaltar;
                @Saltar.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnSaltar;
                @Saltar.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnSaltar;
                @Turbo.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnTurbo;
                @Turbo.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnTurbo;
                @Turbo.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnTurbo;
                @Derrapar.started -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnDerrapar;
                @Derrapar.performed -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnDerrapar;
                @Derrapar.canceled -= m_Wrapper.m_Drive1ActionsCallbackInterface.OnDerrapar;
            }
            m_Wrapper.m_Drive1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Frenar.started += instance.OnFrenar;
                @Frenar.performed += instance.OnFrenar;
                @Frenar.canceled += instance.OnFrenar;
                @Acelerar.started += instance.OnAcelerar;
                @Acelerar.performed += instance.OnAcelerar;
                @Acelerar.canceled += instance.OnAcelerar;
                @Girar.started += instance.OnGirar;
                @Girar.performed += instance.OnGirar;
                @Girar.canceled += instance.OnGirar;
                @Saltar.started += instance.OnSaltar;
                @Saltar.performed += instance.OnSaltar;
                @Saltar.canceled += instance.OnSaltar;
                @Turbo.started += instance.OnTurbo;
                @Turbo.performed += instance.OnTurbo;
                @Turbo.canceled += instance.OnTurbo;
                @Derrapar.started += instance.OnDerrapar;
                @Derrapar.performed += instance.OnDerrapar;
                @Derrapar.canceled += instance.OnDerrapar;
            }
        }
    }
    public Drive1Actions @Drive1 => new Drive1Actions(this);

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_Cancel;
    private readonly InputAction m_UI_Submit;
    public struct UIActions
    {
        private @Input m_Wrapper;
        public UIActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @Cancel => m_Wrapper.m_UI_Cancel;
        public InputAction @Submit => m_Wrapper.m_UI_Submit;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @Cancel.started -= m_Wrapper.m_UIActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnCancel;
                @Submit.started -= m_Wrapper.m_UIActionsCallbackInterface.OnSubmit;
                @Submit.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnSubmit;
                @Submit.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnSubmit;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @Submit.started += instance.OnSubmit;
                @Submit.performed += instance.OnSubmit;
                @Submit.canceled += instance.OnSubmit;
            }
        }
    }
    public UIActions @UI => new UIActions(this);
    public interface IDriveActions
    {
        void OnFrenar(InputAction.CallbackContext context);
        void OnAcelerar(InputAction.CallbackContext context);
        void OnGirar(InputAction.CallbackContext context);
        void OnSaltar(InputAction.CallbackContext context);
        void OnTurbo(InputAction.CallbackContext context);
        void OnDerrapar(InputAction.CallbackContext context);
    }
    public interface IDrive1Actions
    {
        void OnFrenar(InputAction.CallbackContext context);
        void OnAcelerar(InputAction.CallbackContext context);
        void OnGirar(InputAction.CallbackContext context);
        void OnSaltar(InputAction.CallbackContext context);
        void OnTurbo(InputAction.CallbackContext context);
        void OnDerrapar(InputAction.CallbackContext context);
    }
    public interface IUIActions
    {
        void OnCancel(InputAction.CallbackContext context);
        void OnSubmit(InputAction.CallbackContext context);
    }
}
