﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;


public class GameController : MonoBehaviour
{
    public static GameController instance;
    public LayerMask obstacle;


    [Header("PLAYERS")]
    [SerializeField] GameObject playerPrefab;
    [SerializeField] uint nPlayers = 1;


    public List<Texture> playerColors;
    public List<Player> players;
    private Transform targetGroup;
    [SerializeField] List<PlayerUI> playerUI;

    [Header("LOBBY")]
    public List<Transform> spawnPoints;
    
    [Header("MAZE")]
    public GameObject Ground;
    public GameObject Wall;
    public int mazeDim = 10;
    int[][] maze;

    private void Awake()
    {
        #region Singleton
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
        #endregion

        players = new List<Player>();
    }

    private void Start()
    {
        CreateMaze();
        FillMaze();
    }

    void Update()
    {
        #region Camera Follow
        if (players.Count > 0 && targetGroup != null)
        {
            float averageX = 0;
            float averageY = 0;
            float averageZ = 0;

            foreach (Player p in players)
            {
                averageX += p.transform.position.x;
                averageY += p.transform.position.y;
                averageZ += p.transform.position.z;
            }

            Vector3 averagePos = new Vector3(averageX, averageY, averageZ) / players.Count;
            targetGroup.position = averagePos;
        }
        #endregion
    }

    /*
    public void OnPlayerJoined(PlayerInput input)
    {
        Debug.Log("Player Joined");

        Player newPlayer = input.gameObject.GetComponent<Player>();
        setPlayer(newPlayer, players.Count + 1);
    }
    */
    
    public void AddPlayer(PlayerInput input)
    {
        Debug.Log("Player Joined");

        Player newPlayer = Instantiate(playerPrefab, spawnPoints[players.Count].position, Quaternion.identity).GetComponent<Player>();
        setPlayer(newPlayer);

        
        /*newPlayer.GetComponent<PlayerInput>().SwitchCurrentControlScheme(input.devices.First<InputDevice>());
        newPlayer.GetComponent<PlayerInput>().actions = input.actions;
        
        newPlayer.GetComponent<PlayerInput>().uiInputModule = input.uiInputModule;
        newPlayer.GetComponent<Movement>().canMove = false;
        //input.enabled = false;*/


    }

    
    private void OnLevelWasLoaded(int level)
    {
        if(level == 1)  // Fight Level
        {
            foreach(Player p in players)
            {

                p.transform.position = findFreeCell();
                p.GetComponent<Rigidbody>().isKinematic = false;
                targetGroup = GameObject.FindGameObjectWithTag("TargetGroup").transform;
            }
        }
    }
    void setPlayer(Player player)
    {
        int id = 1;
        
        for(int i=0; i<players.Count; i++)
        {
            if (players[i] != null)
                id++;
            else
                break;
        }

        player.id = id;
        //player.ui = playerUI[id - 1];

        player.GetComponent<Rigidbody>().isKinematic = true;
        //player.setBodyMaterial(playerColors[id - 1]);

        
        DontDestroyOnLoad(player.gameObject);

        players.Add(player);

    }

    public Vector3 getNormalizedPosition(Vector3 pos)
    {
        return new Vector3(Mathf.RoundToInt(pos.x), pos.y, Mathf.RoundToInt(pos.z));
    }

    public bool freeCell(Vector3 position)
    {
        Vector3 normalizedPos = getNormalizedPosition(position);
        Collider[] hits = Physics.OverlapSphere(normalizedPos, .2f, obstacle);

        if(hits.Length > 0)
        {
            foreach(Collider go in hits)
            {
                if (go.gameObject.GetComponent<Player>() == null)
                    return false;
            }
        }

        return true;
    }
    

    public bool isPlayer(GameObject player)
    {
        return player.GetComponent<Player>() != null;
    }

    void CreateMaze()
    {
        MazeCreator mazeCreator = new MazeCreator(mazeDim);
        maze = mazeCreator.generateMaze();

        
        foreach(int[] l in maze)
        {
            string row = "";
            for (int i = 0; i < mazeDim; i++)
                row += l[i].ToString() + "  ";

        }
    }

    void FillMaze()
    {
        GameObject walls = new GameObject("WALLS");
        walls.transform.parent = transform;
        
        GameObject ground = new GameObject("GROUND");
        ground.transform.parent = transform;

        for (int i=0; i<mazeDim; i++)
        {
            for (int j = 0; j < mazeDim; j++)
            {
                GameObject g = Instantiate(Ground, new Vector3(i, -1, j), Quaternion.identity);
                g.transform.parent = ground.transform;
                g.name = "Ground[" + i.ToString() + "][" + j.ToString() + "]";

                if (maze[i][j] == 0)
                {
                    GameObject w = Instantiate(Wall, new Vector3(i, 0, j), Quaternion.identity);
                    w.transform.parent = walls.transform;
                    w.name = "Wall[" + i.ToString() + "][" + j.ToString() + "]";
                }
            }
        }
    }

    Vector3 findFreeCell()
    {
        int x = 0;
        int y = 0;
        do
        {
            x = Random.Range(0, maze.Length);
            y = Random.Range(0, maze.Length);


        } while (maze[x][y] == 0 && !freeCell(new Vector3(x,0,y)));

        return new Vector3(x, 0, y);
    }
}
