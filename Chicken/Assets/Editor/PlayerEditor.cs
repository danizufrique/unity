﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEditor;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor
{
    protected static bool showInfo = true;
    protected static bool stats = true;
    protected static bool layers = true;
    protected static bool powers = true;
    protected static bool effects = true;

    public override void OnInspectorGUI()
    {
        Player myTarget = (Player)target;
        LayerMask tempMask;


        powers = EditorGUILayout.Foldout(powers, "POWER-UPS");
        if (powers)
        {
            serializedObject.FindProperty("canKick").boolValue = EditorGUILayout.Toggle("KICK", serializedObject.FindProperty("canKick").boolValue);
        }

        showInfo = EditorGUILayout.Foldout(showInfo, "INFO");
        if (showInfo) 
        { 
            serializedObject.FindProperty("id").intValue = EditorGUILayout.IntField("ID", serializedObject.FindProperty("id").intValue);
            serializedObject.FindProperty("name").stringValue = EditorGUILayout.TextField("Name", serializedObject.FindProperty("name").stringValue);
            serializedObject.FindProperty("points").intValue = EditorGUILayout.IntField("Points", serializedObject.FindProperty("points").intValue);
            serializedObject.FindProperty("bombPrefab").objectReferenceValue = EditorGUILayout.ObjectField("Bomb Prefab", serializedObject.FindProperty("bombPrefab").objectReferenceValue, typeof(Bomb), false);
        }

        stats = EditorGUILayout.Foldout(stats, "STATS");
        if (stats)
        {
            serializedObject.FindProperty("pushDistance").intValue = EditorGUILayout.IntField("Push Distance", serializedObject.FindProperty("pushDistance").intValue);
        }

        layers = EditorGUILayout.Foldout(showInfo, "LAYERS");
        if (layers)
        {
            tempMask = EditorGUILayout.MaskField("Bomb Layer", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(myTarget.isBomb), InternalEditorUtility.layers);
            myTarget.isBomb = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(tempMask);
        }

        effects = EditorGUILayout.Foldout(showInfo, "EFFECTS");
        if (effects)
        {

            serializedObject.FindProperty("layBombEffect").objectReferenceValue = EditorGUILayout.ObjectField("Lay Bomb", serializedObject.FindProperty("layBombEffect").objectReferenceValue, typeof(GameObject), false);
            serializedObject.FindProperty("playerUI").objectReferenceValue = EditorGUILayout.ObjectField("UI", serializedObject.FindProperty("playerUI").objectReferenceValue, typeof(GameObject), false);
        }

        serializedObject.ApplyModifiedProperties();
    }
}