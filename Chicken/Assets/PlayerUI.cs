﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Slider slider;
    public List<Image> images;

    float maxHealth;
    List<PowerUp> currentItems;

    void Start()
    {
        currentItems = new List<PowerUp>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setMaxHealth(float health)
    {
        maxHealth = health;
    }

    public void setName(string name)
    {
        text.text = name;
    }

    public void setHealth(float newHealth)
    {
        slider.value = newHealth/maxHealth;
    }

    public void addItem(PowerUp item)
    {
        if (currentItems.Contains(item))
            return;

        images[currentItems.Count].sprite = item.icon;
        currentItems.Add(item);
    }


}
