﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Damageable
{
    [SerializeField] float time = 5f;
    private Player player = null;

    [Header("Kind")]
    public bool kickBomb;

    public Sprite icon;

    private void Start()
    {
        Destroy(gameObject, time);
    }

    private void OnTriggerEnter(Collider other)
    {
        player = other.gameObject.GetComponent<Player>();

        if (player == null)
        {
            return;
        }
        else
        {
            Action();
            Destroy(gameObject);
        }
    }

    public void Action()
    {

    }

    public override void Die()
    {
        Destroy(gameObject);
    }
}
