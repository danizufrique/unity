using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : Damageable
{
    private GameController gameController;

    public int id = -1;
    [SerializeField] private new string name = "";
    [SerializeField] private int points = 0;
    [SerializeField] Bomb bombPrefab;
    public PlayerUI ui;

    private List<Bomb> instBombs = new List<Bomb>();

    [Header("-------STATS---------")]
    [SerializeField] int pushDistance = 2;
    [SerializeField] float pushRadius = 1f;

    [Header("-------LAYERS--------")]
    public LayerMask isBomb;

    [Header("POWER-UPS")]
    [SerializeField] bool canKick = false;

    [Header("EFFECTS")]
    [SerializeField] GameObject layBombEffect;
    public GameObject playerUI;

    private void Start()
    {
        gameController = GameController.instance;

        /*
        ui.setMaxHealth(hp);
        ui.setName(name);
        */

        GameObject.FindGameObjectWithTag("playerUI").GetComponentInChildren<TextMeshProUGUI>().text = name;

    }

    public override void Die()
    {
        Destroy(gameObject);
    }

    private void OnLayBomb()
    {
        Vector3 normalizedPos = gameController.getNormalizedPosition(transform.position);
        if (gameController.freeCell(normalizedPos))
        {
            Instantiate(layBombEffect, normalizedPos, Quaternion.identity);
            Bomb instBomb = Instantiate(bombPrefab, normalizedPos, Quaternion.identity).GetComponent<Bomb>();
            instBomb.owner = this;
            instBombs.Add(instBomb);
        }
    }

    private void OnKick()
    {
        Bomb nearest = FindNearestBomb();
        if (nearest != null)
        {
            Vector3 dir = gameController.getNormalizedPosition(transform.position + transform.forward) - gameController.getNormalizedPosition(transform.position);
            StartCoroutine(nearest.PushBomb(dir, pushDistance));
        }
    }

    private void OnExplode()
    {
        if(instBombs.Count > 0)
            instBombs[0].Explode();
    }


    public void setBodyMaterial(Texture texture)
    {
        GetComponentInChildren<Renderer>().material.mainTexture = texture;
    }

    public void setEggMaterial(Texture texture)
    {
        bombPrefab.GetComponentInChildren<Renderer>().material.mainTexture = texture;
    }


    public void eraseInstBomb(Bomb bomb)
    {
        instBombs.Remove(bomb);
    }

    Bomb FindNearestBomb()
    {
        if (instBombs.Count > 0)
        {
            List<Bomb> copy = instBombs.OrderBy(bomb => Vector3.Distance(transform.position, bomb.transform.position)).ToList();

            return copy.First();
        }

        return null;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Bomb nearBomb = FindNearestBomb();

        if (nearBomb != null)
        {
            if(Vector3.Distance(transform.position, nearBomb.transform.position) < pushRadius)
                Gizmos.DrawLine(transform.position, nearBomb.transform.position);
        }

        //Gizmos.color = Color.cyan;
        //Gizmos.DrawWireSphere(transform.position, pushRadius);
    }

    public override void receiveDamage(int damage)
    {
        base.receiveDamage(damage);
        ui.setHealth(hp);
    }

}