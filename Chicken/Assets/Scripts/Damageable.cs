using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Damageable : MonoBehaviour
{
    public int hp;

    public virtual void receiveDamage(int damage){
        hp -= damage;
        Debug.Log("DAMAGE: " + gameObject.name);
        if(hp <= 0)
            Die();
    }

    public abstract void Die();
}