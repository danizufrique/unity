using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : Damageable
{
    private enum BlockType{
        Grass, Ice, Lava
    }

    private void Start()
    {
        transform.Rotate(0.0f, Random.Range(0,4)*90f, 0.0f, Space.Self);
    }

    public override void receiveDamage(int damage)
    {
        base.receiveDamage(damage);
        StartCoroutine(gameObject.GetComponent<CubeExplosion>().SplitMesh(false));
    }

    public override void Die(){
        StartCoroutine(gameObject.GetComponent<CubeExplosion>().SplitMesh(true));
        //Destroy(gameObject);
    }
}