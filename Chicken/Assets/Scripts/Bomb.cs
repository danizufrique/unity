using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public Player owner;
    GameController gameController;

    [SerializeField] GameObject[] explosionParticles;
    private enum Direction{
        North, South, East, West
    }

    [SerializeField] float time = 2f;
    [SerializeField] int range = 1;
    [SerializeField] int damage = 1;
    [SerializeField] float pushSpeed = 1;

    bool exploded = false;

    private void Start() {
        gameController = GameController.instance;
        Invoke("Explode", time);
    }

    public void Explode(){
        if (!exploded)
        {
            exploded = true;
            GetComponentInChildren<MeshRenderer>().enabled = false;
            //transform.Find("Collider").gameObject.SetActive(false);

            StartCoroutine("Path", Direction.North);
            StartCoroutine("Path", Direction.South);
            StartCoroutine("Path", Direction.East);
            StartCoroutine("Path", Direction.West);

            gameObject.AddComponent<TriangleExplosion>();
            StartCoroutine(gameObject.GetComponent<TriangleExplosion>().SplitMesh(true));
            Destroy(gameObject, .5f);
        }
    }

    public IEnumerator PushBomb(Vector3 direction, int blocks=1)
    {
        bool stop = false;

        Debug.Log("EMPIEZO: " + direction);
        for (int i = 0; i < blocks && !stop; i++)   // En cada casilla comprueba si en la siguiente hay obstaculo
        {
            RaycastHit hit;
            Physics.Raycast(transform.position + new Vector3(0, .05f, 0), direction, out hit, 1, gameController.obstacle);

            Vector3 destiny = transform.position + direction;

            if (!hit.collider)  // Si no hay obstaculo mover a siguiente
            {
                Debug.Log("NO OBSTACULOS");
                while ((destiny - transform.position).magnitude > .001f)
                {
                    Debug.Log("MOVIENDOSE");
                    transform.position = Vector3.MoveTowards(transform.position, destiny, pushSpeed * .05f);
                    yield return null;
                }
            }
            else    // Si lo hay parar
            {
                Debug.Log("OBSTACULOS");
                stop = true;
            }
            
        }

        // Cuadrar posicion
        transform.position = gameController.getNormalizedPosition(transform.position);

        Debug.Log("ACABO");
        yield return null;
    }
    IEnumerator Path(Direction path){
        Vector3 dir;

        switch(path)
        {
            case Direction.North:
                dir = Vector3.forward;
                break;
            case Direction.South:
                dir = Vector3.back;
                break;
            case Direction.East:
                dir = Vector3.right;
                break;
            case Direction.West:
                dir = Vector3.left;
                break;
            default:
                dir = Vector3.zero;
                break;
        }
        
        for(int i=1; i<=range; i++){
            RaycastHit hit; 
            Physics.Raycast(transform.position + new Vector3(0, .05f, 0), dir, out hit, i, gameController.obstacle);

            if (!hit.collider)
            {
                Instantiate(explosionParticles[0], transform.position + (i * dir) + new Vector3(0,transform.localScale.y/2,0), Quaternion.identity);
            }
            else{

                Bomb hitBomb = hit.transform.gameObject.GetComponent<Bomb>();
                if (hitBomb != null)
                    hitBomb.Explode();

                else if (hit.transform.gameObject.GetComponent<Damageable>() != null)
                {
                    hit.transform.gameObject.GetComponent<Damageable>().receiveDamage(damage);
                    break;
                }
            }

            yield return new WaitForSeconds(.05f);
        }
    }

    private void OnDestroy()
    {
        owner.eraseInstBomb(this);
    }
}