﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeCreator
{
    private Stack<Node> stack;
    private int[][] maze;
    private int dimension;

    private void Start()
    {
        
    }

    public MazeCreator(int dim)
    {
        maze = new int[dim][];
        for(int i=0; i<dim; i++)
            maze[i] = new int[dim];
        
        dimension = dim;
    }

    public int[][] generateMaze()
    {
        stack = new Stack<Node>();
        stack.Push(new Node(0, 0));
        while (stack.Count > 0)
        {
            Node next = stack.Pop();
            if (validNextNode(next))
            {
                maze[next.y][next.x] = 1;
                List<Node> neighbors = findNeighbors(next);
                randomlyAddNodesToStack(neighbors);
            }
        }

        return maze;
    }

    public String getSymbolicMaze()
    {
        string res = "";
        for (int i = 0; i < dimension; i++)
        {
            for (int j = 0; j < dimension; j++)
            {
                res += maze[i][j] == 1 ? "*" : " ";
                res += " ";
            }
            res += "\n";
        }
        return res;
    }

    private bool validNextNode(Node node)
    {
        int numNeighboringOnes = 0;
        for (int y = node.y - 1; y < node.y + 2; y++)
        {
            for (int x = node.x - 1; x < node.x + 2; x++)
            {
                if (pointOnGrid(x, y) && pointNotNode(node, x, y) && maze[y][x] == 1)
                {
                    numNeighboringOnes++;
                }
            }
        }
        return (numNeighboringOnes < 3) && maze[node.y][node.x] != 1;
    }

    private void randomlyAddNodesToStack(List<Node> nodes)
    {
        int targetIndex;
        while (nodes.Count > 0)
        {
            
            targetIndex = UnityEngine.Random.Range(0, nodes.Count);
            stack.Push(nodes[targetIndex]);
            nodes.RemoveAt(targetIndex);
        }
    }

    private List<Node> findNeighbors(Node node)
    {
        List<Node> neighbors = new List<Node>();
        for (int y = node.y - 1; y < node.y + 2; y++)
        {
            for (int x = node.x - 1; x < node.x + 2; x++)
            {
                if (pointOnGrid(x, y) && pointNotCorner(node, x, y)
                    && pointNotNode(node, x, y))
                {
                    neighbors.Add(new Node(x, y));
                }
            }
        }
        return neighbors;
    }

    private bool pointOnGrid(int x, int y)
    {
        return x >= 0 && y >= 0 && x < dimension && y < dimension;
    }

    private bool pointNotCorner(Node node, int x, int y)
    {
        return (x == node.x || y == node.y);
    }

    private bool pointNotNode(Node node, int x, int y)
    {
        return !(x == node.x && y == node.y);
    }
}

