﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeExplosion : MonoBehaviour
{
    public int size = 10;
    public Vector2 force;

    public IEnumerator SplitMesh(bool destroy)
    {
        MeshRenderer renderer = GetComponentInChildren<MeshRenderer>();
        Mesh myMesh = GetComponentInChildren<MeshFilter>().mesh;

        Material material = renderer.material;

        Vector3 startPos = transform.position - gameObject.GetComponent<BoxCollider>().size * 0.5f;
        float cubeSize = gameObject.GetComponent<BoxCollider>().size.x;
        Vector3 spawnPos = startPos;

        for (int i=0; i<size; i++)
        {
            spawnPos.x = startPos.x;
            for (int j = 0; j < size; j++)
            {
                for (int k = 0; k < size; k++)
                {
                    GameObject cube = new GameObject("cube");
                    cube.transform.position = spawnPos + new Vector3(0, 0, k * cubeSize / size);
                    cube.transform.rotation = transform.rotation;
                    cube.AddComponent<MeshRenderer>().material = material;
                    cube.AddComponent<MeshFilter>().mesh = myMesh;
                    cube.transform.localScale /= size;

                    Vector3 explosionPos = new Vector3(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(0f, 0.5f), transform.position.z + Random.Range(-0.5f, 0.5f));
                    cube.AddComponent<Rigidbody>().AddExplosionForce(Random.Range(force.x, force.y), explosionPos, 5);
                    Destroy(cube, 2f);
                }

                spawnPos += new Vector3(cubeSize / size, 0, 0);
            }

            spawnPos += new Vector3(0, cubeSize / size, 0);
        }


        

        yield return new WaitForSeconds(0.2f);
        if (destroy == true)
        {
            if (GetComponent<Collider>())
                GetComponent<Collider>().enabled = false;
            GetComponentInChildren<Renderer>().enabled = false;
            Destroy(gameObject);
        }

    }

    /*
    IEnumerator Reduce(GameObject mesh)
    {
        while (mesh.transform.localScale.magnitude > .05f)
        {
            float x = mesh.transform.localScale.x;
            float y = mesh.transform.localScale.y;
            float z = mesh.transform.localScale.z;

            Mathf.Lerp(x, 0, 10);
            Mathf.Lerp(y, 0, 10);
            Mathf.Lerp(z, 0, 10);

            mesh.transform.localScale = new Vector3(x,y,z);
            yield return null;
        }
    }
    */
}
