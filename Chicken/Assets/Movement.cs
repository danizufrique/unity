﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : MonoBehaviour
{

    
    [Header("STATS")]
    [SerializeField] float speed = 5f;
    [SerializeField] public float jumpForce = 2f;
    [SerializeField] public float dashForce = 2f;
    [SerializeField] public LayerMask groundLayer;

    // COMPONENTS
    private Rigidbody rb;
    private Animator anim;

    // MOVEMENT
    private Vector3 dir = Vector3.zero;
    private bool grounded = true;

    private Vector2 axis;

    public bool canMove = true;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        /*
        if (canMove)
        {
            grounded = Physics.CheckSphere(transform.position, 0.2f, groundLayer, QueryTriggerInteraction.Ignore);

            //camera forward and right vectors:
            Vector3 forward = Camera.main.transform.forward;
            Vector3 right = Camera.main.transform.right;

            //project forward and right vectors on the horizontal plane (y = 0)
            forward.y = 0f;
            right.y = 0f;
            forward.Normalize();
            right.Normalize();

            dir = forward * axis.y + right * axis.x;

            if (dir != Vector3.zero)
            {
                transform.forward = dir;
                anim.SetBool("Run", true);
            }
            else
            {
                anim.SetBool("Run", false);
            }
        }

        /*
        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.AddForce(Vector3.up * Mathf.Sqrt(jumpForce * -2f * Physics.gravity.y), ForceMode.VelocityChange);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Vector3 dashVelocity = Vector3.Scale(transform.forward, dashForce * new Vector3((Mathf.Log(1f / (Time.deltaTime * rb.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * rb.drag + 1)) / -Time.deltaTime)));
            rb.AddForce(dashVelocity, ForceMode.VelocityChange);
        }
        */
    }

    private void OnMovement(InputValue value)
    {
        Vector2 input = value.Get<Vector2>();
        axis = new Vector2(input.x, input.y);
        goal
    }

    void FixedUpdate()
    {
        rb.velocity = Vector2.SmoothDamp(rb.velocity, goalVelocity, ref currentVelocityRef, accelerationDuration, float.MaxValue, Time.deltaTime);
    }
}
