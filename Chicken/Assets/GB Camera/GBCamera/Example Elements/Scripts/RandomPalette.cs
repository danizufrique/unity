﻿using UnityEngine;
using System.Collections;

namespace RogueNoodleAndTicoliro
{
namespace GBCamera
{

public class RandomPalette : MonoBehaviour {

	public Texture[] _palettes;
	public Material[] _materials;
	private int _previousPaletteIndex = 0;
	public int maxArray;

	private void Start() 
	{
		maxArray = maxArray -1;
	}

	public void SelectRandomPalette ()
	{
		if (_palettes.Length == 0 || _materials.Length == 0)
			return;
		
		int randomPaletteIndex = _previousPaletteIndex;
		while (randomPaletteIndex == _previousPaletteIndex)
		{
			randomPaletteIndex = Random.Range (0, _palettes.Length);
		}	
		
		for (int i = 0; i < _materials.Length; i++)
		{
			_materials[i].SetTexture ("_Palette", _palettes[randomPaletteIndex]);
		}
		
		_previousPaletteIndex = randomPaletteIndex;
	}

	public void NextPalette()
	{
		if (_palettes.Length == 0 || _materials.Length == 0)
			return;
		int randomPaletteIndex = _previousPaletteIndex;
		while (randomPaletteIndex == _previousPaletteIndex)
		{
			randomPaletteIndex = randomPaletteIndex +1;
		}
		for (int i = 0; i < _materials.Length; i++)
		{
			_materials[i].SetTexture ("_Palette", _palettes[randomPaletteIndex]);
		}
		_previousPaletteIndex = randomPaletteIndex;

		if (randomPaletteIndex >= maxArray)
		{
			_previousPaletteIndex = 0;
			randomPaletteIndex = -1;
		}

	}

	public void PreviousPalette()
	{
		if (_palettes.Length == 0 || _materials.Length == 0)
			return;
		int randomPaletteIndex = _previousPaletteIndex;
		while (randomPaletteIndex == _previousPaletteIndex)
		{
			randomPaletteIndex = randomPaletteIndex -1;
		}
		for (int i = 0; i < _materials.Length; i++)
		{
			_materials[i].SetTexture ("_Palette", _palettes[randomPaletteIndex]);
		}
		_previousPaletteIndex = randomPaletteIndex;

		if (randomPaletteIndex <= 0)
		{

			_previousPaletteIndex = 10;
			randomPaletteIndex = 10;
		}
	}
}

} // GBCamera
} // RogueNoodleAndTicoliro
