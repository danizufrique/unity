﻿using UnityEngine;
using System.Collections;

namespace RogueNoodleAndTicoliro
{
namespace GBCamera
{

public class RandomPaletteButton : MonoBehaviour
{
	
	public FadeMaterials _fadeMaterials;
	public RandomPalette _randomPalette;
	private bool _switchingPalette = false;
	
	public void OnChooseRandomPalette ()
	{
		if (!_switchingPalette)
		{
			StartCoroutine (ChooseRandomPalette ());
		}
			
	}

	public void OnChooseNextPalette ()
	{
		if (!_switchingPalette)
		{
			StartCoroutine (ChooseNextPalette ());
		}
			
	}
	
	public void OnChoosePreviousPalette ()
	{
		if (!_switchingPalette)
		{
			StartCoroutine (ChoosePreviousPalette ());
		}
			
	}

	public IEnumerator ChooseRandomPalette ()
	{
		_switchingPalette = true;
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeOut ());
		_randomPalette.SelectRandomPalette ();
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeIn());
		_switchingPalette = false;
	}

	public IEnumerator ChooseNextPalette ()
	{
		_switchingPalette = true;
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeOut ());
		_randomPalette.NextPalette ();
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeIn());
		_switchingPalette = false;
	}

	public IEnumerator ChoosePreviousPalette ()
	{
		_switchingPalette = true;
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeOut ());
		_randomPalette.PreviousPalette ();
		yield return _fadeMaterials.StartCoroutine (_fadeMaterials.FadeIn());
		_switchingPalette = false;
	}
	

}


}
}
