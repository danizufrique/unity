﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Pickable
{
    // Start is called before the first frame update
    public override void Start()
    {
        type = Type.Coin;
    }

    public override void Effect(Player p)
    {
        base.Effect(p);
        GameController.instance.AddPoints(1);
        GameController.instance.EatCoin();
    }
}
