﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Portal otherPortal;
    public Node node;
    public bool canBeUsed;

    void Start()
    {
        canBeUsed = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8 && canBeUsed)
        {
            Player p = other.gameObject.GetComponent<Player>();
            otherPortal.canBeUsed = false;
            p.Teleport(otherPortal.node);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            canBeUsed = true;
        }
    }
}
