﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

public class BlueGhost : Ghost
{
    public override void Start()
    {
        type = Type.Blue;
        base.Start();
    }

    public override bool canExitHouse()
    {
        return currentLevel.eatedCoins >= 30;
    }

    protected override Node CalculateChaseNode()
    {
        Vector3Int dir = Vector3Int.zero;

        switch (player.direction)
        {
            case Player.Direction.Up:
                dir = new Vector3Int(-1, 0, 1);
                break;
            case Player.Direction.Left:
                dir = new Vector3Int(-1, 0, 0);
                break;
            case Player.Direction.Down:
                dir = new Vector3Int(0, 0, -1);
                break;
            case Player.Direction.Right:
                dir = new Vector3Int(1, 0, 0);
                break;
            default:
                dir = Vector3Int.zero;
                break;
        }

        dir *= 2;
        Vector2Int pacmanAhead = currentLevel.nodes[player.currentNode.coordinates.x + dir.x][player.currentNode.coordinates.y + dir.y].coordinates;
        Vector2Int redNode = currentLevel.ghosts.OfType<RedGhost>().First().currentNode.coordinates;
        Vector2Int finalCoordinates = (pacmanAhead + (pacmanAhead - redNode));
        finalCoordinates = new Vector2Int(Mathf.Abs(finalCoordinates.x) % (currentLevel.dim.x-1), Mathf.Abs(finalCoordinates.y) % (currentLevel.dim.y-1));

        return currentLevel.nodes[finalCoordinates.x][finalCoordinates.y];
    }

    public override void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        base.OnDrawGizmos();
    }
}
