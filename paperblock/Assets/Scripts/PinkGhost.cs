﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PinkGhost : Ghost
{
    public override void Start()
    {
        type = Type.Pink;
        base.Start();
    }

    public override bool canExitHouse()
    {
        return true;
    }

    protected override Node CalculateChaseNode()
    {
        Vector3Int dir = Vector3Int.zero;

        switch (player.direction)
        {
            case Player.Direction.Up:
                dir = new Vector3Int(-1, 0, 1);
                break;
            case Player.Direction.Left:
                dir = new Vector3Int(-1, 0, 0);
                break;
            case Player.Direction.Down:
                dir = new Vector3Int(0, 0, -1);
                break;
            case Player.Direction.Right:
                dir = new Vector3Int(1, 0, 0);
                break;
            default:
                dir = Vector3Int.zero;
                break;
        }

        dir *= 4;
        Node result = currentLevel.nodes[player.currentNode.coordinates.x + dir.x][player.currentNode.coordinates.y + dir.y];

        return result;
    }

    public override void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        base.OnDrawGizmos();
    }
}
