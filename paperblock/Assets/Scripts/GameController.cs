﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Cinemachine;
using TMPro;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    

    public static GameController instance;
    public List<Level> levels;
    Level currentLevel;
    Player currentPlayer;
    
    public GameObject playerPrefab;
    public GameObject redPrefab, pinkPrefab, orangePrefab, bluePrefab;
    public GameObject portalPrefab;
    public GameObject eatPrefab;

    //public Hashtable ghosts = new Hashtable();
    public List<GameObject> ghostsPrefabs;

    public CinemachineVirtualCamera cam;
    public CinemachineTargetGroup targetGroup;

    [Header("UI")]
    public GameObject InfoPanel;
    public TextMeshProUGUI pointsGUI;
    int playerPoints;
    public GameObject menu;
    public  TextMeshProUGUI livesGUI;
    int playerLives;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        ghostsPrefabs.Add(redPrefab);
        ghostsPrefabs.Add(pinkPrefab);
        ghostsPrefabs.Add(orangePrefab);
        ghostsPrefabs.Add(bluePrefab);

        currentLevel = levels.First();
        currentPlayer = currentLevel.SpawnLevel();

        UpdateTargetGroup();

        playerPoints = 0;
        pointsGUI.text = "Points: " + playerPoints.ToString();
        playerLives = 3;
        livesGUI.text = "Lives: " + playerLives.ToString();
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return))
        {
            PauseGame();
        }

    }

    public void PauseGame()
    {
        if(menu.active == false)
        {
            menu.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            menu.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartFrightened()
    {
        foreach(Ghost g in currentLevel.ghosts)
        {
            g.StartFrightened();
        }
    }

    
    public void AddPoints(int p)
    {
        playerPoints += p;
        pointsGUI.text = "Points: " + playerPoints.ToString();
    }

    public void AddLives(int l)
    {
        playerLives += l;
        livesGUI.text = "Lives: " + playerLives.ToString();
    }

    public void EatCoin()
    {
        currentLevel.eatedCoins++;
        if (currentLevel.eatedCoins == currentLevel.totalCoins)
            StartCoroutine(LevelCompleted());

    }

    public void PlayerDie()
    {
        AddLives(-1);
        if (playerLives<= 0)
        {
            GameOver();
        }
        else
        {
            currentLevel.player.transform.position = currentLevel.RandomSpawnNode().position;
        }
    }

    void GameOver()
    {
        InfoPanel.GetComponentInChildren<TextMeshProUGUI>().text = "GAME OVER!";
        InfoPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    void UpdateTargetGroup()
    {
        for (int i = 0; i < targetGroup.m_Targets.Length; i++)
        {
            targetGroup.RemoveMember(targetGroup.m_Targets[i].target);
        }
            
        targetGroup.AddMember(currentLevel.player.transform, 1, 1);

        foreach (Ghost g in currentLevel.ghosts)
            targetGroup.AddMember(g.transform, 0.25f, 1);
    }
    IEnumerator LevelCompleted()
    {
        InfoPanel.GetComponentInChildren<TextMeshProUGUI>().text = "Level Completed!";
        InfoPanel.SetActive(true);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.5f);

        Level nextLevel = levels[0];
        for(int i=0; i<levels.Count; i++)
        {
            if (levels[i] == currentLevel && i < levels.Count - 1)
                nextLevel = levels[i + 1];
        }

        currentLevel.Dispawn();
        currentLevel = nextLevel;
        InfoPanel.SetActive(false);
        currentLevel.SpawnLevel(currentPlayer);

        

    }

    private void OnDrawGizmos()
    {
        /*
        Gizmos.color = Color.yellow;
        if (currentLevel != null)
        {
            for (int i = 0; i < currentLevel.dim.x; i++)
            {
                for (int j = 0; j < currentLevel.dim.y; j++)
                {
                    if (currentLevel.nodes[i][j].isIntersection())
                        Gizmos.DrawSphere(currentLevel.nodes[i][j].position, .2f);
                }
            }
        }
        */
    }
    
}
