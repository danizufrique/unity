using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System.Linq;
using System.Collections;
using System.Linq.Expressions;
using System;

public class Ghost : MonoBehaviour{
    
    public Level currentLevel;
    public enum Type { Red, Blue, Orange, Pink}
    public enum State{Scatter, Chase, Frightened, House}
    public enum Direction{Up, Left, Down, Right, None}

    public Type type;
    public State state;
    public Direction currentDirection;

    Rigidbody rb;
    public Player player;
    public Node targetNode, currentNode;
    Material baseMaterial;
    public Material frightenedMaterial;

    [Range(7,11)]
    public float speed = 3;
    private Wave currentWave;
    Coroutine waveCounter, frightenedRoutine;

    public virtual void Start() {
        rb = GetComponent<Rigidbody>();
        baseMaterial = GetComponent<MeshRenderer>().material;
        
        StartCoroutine(StartFromHouse());
    }

    IEnumerator StartFromHouse()
    {
        Node randomNode = currentLevel.RandomHouseNode();

        // Aparecer en nodo aleatorio de House
        currentDirection = Direction.None;
        transform.position = randomNode.position;
        currentNode = randomNode;

        if (frightenedRoutine != null)
            StopCoroutine(frightenedRoutine);

        state = State.House;
        GetComponent<MeshRenderer>().material = baseMaterial;
        rb.angularVelocity = Vector3.zero;
        transform.rotation = Quaternion.Euler(0, 0, 0);

        // Esperar a poder salir
        while (!canExitHouse())
            yield return new WaitForSeconds(1);

        // Salir
        StartCoroutine(ExitHouse());
    }

    public virtual bool canExitHouse() { return false; }

    IEnumerator ExitHouse()
    {
        state = State.House;

        yield return new WaitForSeconds(1f);

        // Go to Exit
        Node exit = currentLevel.RandomExitHouseNode();
        while (Vector3.Distance(transform.position, exit.position) > .001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, exit.position, speed * Time.deltaTime);
        }

        transform.position = exit.position;
        currentNode = exit;

        // Find Way Out
        targetNode = CalculateTargetNode();
        UpdateDirection();

        state = State.Scatter;

        if(waveCounter != null)
            StopCoroutine(waveCounter);

        waveCounter = StartCoroutine(CountWaves());

        yield return null;
    }

    public void SetSpeed(float s)
    {
        speed = s;
    }

    IEnumerator CountWaves()
    {
        int index = 0;

        while (index < currentLevel.GetNWaves())
        {
            currentWave = currentLevel.GetWave(index);

            state = Ghost.State.Scatter;
            yield return new WaitForSeconds(currentWave.scatterTime);

            state = Ghost.State.Chase;
            yield return new WaitForSeconds(currentWave.chaseTime);

            currentWave.finished = true;
            index++;
        }

        state = Ghost.State.Chase;
    }

    void Update()
    {
        targetNode = CalculateTargetNode();
        UpdateCurrentNode();
    }

    private void FixedUpdate()
    {
        Vector3 dir = Vector3.zero;

        switch (currentDirection)
        {
            case Direction.Up:
                dir = new Vector3(0, 0, 1);
                break;
            case Direction.Left:
                dir = new Vector3(-1, 0, 0);
                break;
            case Direction.Down:
                dir = new Vector3(0, 0, -1);
                break;
            case Direction.Right:
                dir = new Vector3(1, 0, 0);
                break;
            case Direction.None:
                dir = Vector3.zero;
                break;
            default:
                break;
        }

        rb.velocity = dir * speed;
    }

    Direction ProcessDirection(Node n)
    {
        Direction d = Direction.None;

        Vector2Int coor = n.coordinates;
    
        if (currentNode.coordinates.x < coor.x && currentDirection != Direction.Left)
            d = Direction.Right;
        else if (currentNode.coordinates.x > coor.x && currentDirection != Direction.Right)
            d = Direction.Left;
        else if (currentNode.coordinates.y < coor.y && currentDirection != Direction.Down)
            d = Direction.Up;
        else if (currentNode.coordinates.y > coor.y && currentDirection != Direction.Up)
            d = Direction.Down;

        return d;
    }

    Node CalculateTargetNode()
    {
        if (canExitHouse() && currentNode.type == Node.Type.HouseExit)   // Ha salido
        {
            state = State.Scatter;
        }

        switch (state)
        {
            case State.House:
                return null;

            case State.Chase:
                return CalculateChaseNode();

            case State.Frightened:
                return currentLevel.RandomNode();

            default:    // Scatter
                return (Node)currentLevel.scatterNodes[type.ToString()];
        }
    }

    protected virtual Node CalculateChaseNode()
    {
        return null;
    }

    void UpdateDirection()
    {
        List<Node> orderedAdyacentNodes = currentNode.adyacentNodes.OrderBy(o => Vector3.Distance(o.position, targetNode.position)).ToList();

        bool finish = false;
        for(int i=0; i<orderedAdyacentNodes.Count && !finish; i++)
        {
            if (orderedAdyacentNodes[i].isTransitable())
            {
                currentDirection = ProcessDirection(orderedAdyacentNodes[i]);
                finish = (currentDirection == Direction.None) ? false : true;
            }
        }

    }

    void UpdateCurrentNode()
    {
        Node newNode = currentNode;

        if (Vector3.Distance(transform.position, currentNode.position) > .95f)
        {
            List<Node> orderedAdyacentNodes = currentNode.adyacentNodes.OrderBy(o => Vector3.Distance(o.position, transform.position)).ToList();

            bool finish = false;
            for (int i = 0; i < orderedAdyacentNodes.Count && !finish; i++)
            {
                if (orderedAdyacentNodes[i].isTransitable())
                {
                    newNode = orderedAdyacentNodes[i];
                    finish = true;
                }
            }
        }
        
        if(currentNode != newNode)
        {
            currentNode = newNode;

            if(currentNode.isIntersection())
                UpdateDirection();
        }
    }

    public void StartFrightened()
    {
        if (state != State.House)
        {
            if (state == State.Frightened)
                StopCoroutine(frightenedRoutine);

            frightenedRoutine = StartCoroutine(Frightened());
        }
    }

    IEnumerator Frightened()
    {
        StopCoroutine(waveCounter);
        state = State.Frightened;

        GetComponent<MeshRenderer>().material = frightenedMaterial;
        ReverseDirection();

        yield return new WaitForSeconds(currentLevel.frightenedTime);

        GetComponent<MeshRenderer>().material = baseMaterial;
        waveCounter = StartCoroutine(CountWaves());
    }

    void ReverseDirection()
    {
        switch (currentDirection)
        {
            case Direction.Left:
                currentDirection = Direction.Right;
                break;
            case Direction.Right:
                currentDirection = Direction.Left;
                break;
            case Direction.Up:
                currentDirection = Direction.Down;
                break;
            case Direction.Down:
                currentDirection = Direction.Up;
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 8)
        {
            if (state == State.Frightened)
                Die();
            else
                GameController.instance.PlayerDie();
        }
    }


    void Die()
    {
        GameController.instance.AddPoints(50);
        StartCoroutine(StartFromHouse());
    }

    public virtual void OnDrawGizmos()
    {
        if (targetNode != null)
        {
            Gizmos.DrawLine(transform.position, targetNode.position);
            Gizmos.DrawCube(targetNode.position, new Vector3(.2f, .2f, .2f));
            Gizmos.DrawSphere(currentNode.position, .5f);
        }
    }
}