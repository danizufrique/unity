﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class OrangeGhost : Ghost
{
    public override void Start()
    {
        type = Type.Orange;
        base.Start();
    }

    public override bool canExitHouse()
    {
        return (float)currentLevel.eatedCoins / currentLevel.totalCoins >= 0.3f;
    }

    protected override Node CalculateChaseNode()
    {
        float distance = Vector3.Distance(currentNode.position, player.currentNode.position);

        if(distance <= 8)
        {
            return (Node)currentLevel.scatterNodes[type.ToString()];
        }
        else {
            return player.currentNode;
        }
    }

    public override void OnDrawGizmos()
    {
        Gizmos.color = new Color(255,165,0);
        base.OnDrawGizmos();
    }
}
