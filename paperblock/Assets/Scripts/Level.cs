﻿using Array2DEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

[System.Serializable]
public class ColorMapping
{
    public Color color;
    public Node.Type type;
}

[System.Serializable]
public class Wave
{
    public float scatterTime;
    public float chaseTime;
    public bool finished = false;
}

[System.Serializable]
public class Level : MonoBehaviour
{
    private GameController controller;
    private GameObject wallsParent, coinsParent;
    public Array2DString layout;
    public Texture2D map;
    public float frightenedTime;
    public Wave[] waves;
    public ColorMapping[] colorMappings;
    public List<Ghost> ghosts;
    public List<Portal> portals;
    public Player player;
    public Vector2Int dim;
    public Node[][] nodes;
    public GameObject wallPrefab, coinPrefab;

    List<Node> playerSpawnNodes;
    List<Node> eatNodes;
    List<Node> houseNodes;
    List<Node> exitHouseNodes;
    public Hashtable scatterNodes = new Hashtable();
    List<Node> portals1;
    List<Node> portals2;
    List<Node> portals3;
    List<Node> coinNodes;

    public int totalCoins, eatedCoins;

    void ProcessInfo()
    {
        controller = GameController.instance;
        playerSpawnNodes = new List<Node>();
        houseNodes = new List<Node>();
        exitHouseNodes = new List<Node>();
        portals1 = new List<Node>();
        portals2 = new List<Node>();
        portals3 = new List<Node>();
        eatNodes = new List<Node>();
        coinNodes = new List<Node>();

        dim = new Vector2Int(map.width, map.height);

        Debug.Log(dim);

        #region Reserva Memoria
        nodes = new Node[dim.x][];

        for (int i = 0; i < dim.x; i++)
        {
            nodes[i] = new Node[dim.y];
        }
        #endregion

        #region Crear Nodos
        for (int i = 0; i < map.width; i++)
        {
            for (int j = 0; j < map.height; j++) { 
                ProcessPixel(i, j);
            }
        }
        #endregion

        #region Calcular Adyacentes
        for (int i = 0; i < dim.x; i++)
        {
            for (int j = 0; j < dim.y; j++)
            {
                nodes[i][j].adyacentNodes = CalculateAdjacentNodes(new Vector2Int(i, j));

                /*
                Debug.Log("======================================");
                Debug.Log("ADYACENTES DEL NODO " + nodes[i][j].coordinates + ":");
                foreach(Node n in nodes[i][j].adyacentNodes)
                {
                    if (n == null)
                        Debug.Log(null);
                    else
                        Debug.Log(n.coordinates);
                }
                */
                    
            }
        }
        #endregion
    }

    void ProcessPixel(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);
        bool found = false;

        for(int i=0; i<colorMappings.Length && !found; i++)
        {
            if (colorMappings[i].color == pixelColor)
            {
                Node.Type nodeType = colorMappings[i].type;
                nodes[x][y] = new Node(new Vector2Int(x, y), nodeType);

                if (nodeType == Node.Type.House)
                    houseNodes.Add(nodes[x][y]);
                else if (nodeType == Node.Type.HouseExit)
                    exitHouseNodes.Add(nodes[x][y]);

                else if (nodeType == Node.Type.Red)
                    scatterNodes.Add("Red", nodes[x][y]);
                else if (nodeType == Node.Type.Blue)
                    scatterNodes.Add("Blue", nodes[x][y]);
                else if (nodeType == Node.Type.Orange)
                    scatterNodes.Add("Orange", nodes[x][y]);
                else if (nodeType == Node.Type.Pink)
                    scatterNodes.Add("Pink", nodes[x][y]);

                else if (nodeType == Node.Type.Spawn)
                    playerSpawnNodes.Add(nodes[x][y]);

                else if (nodeType == Node.Type.Portal1)
                {
                    portals1.Add(nodes[x][y]);
                }
                else if (nodeType == Node.Type.Portal2)
                {
                    portals2.Add(nodes[x][y]);
                }
                else if (nodeType == Node.Type.Portal3)
                {
                    portals3.Add(nodes[x][y]);
                }

                else if (nodeType == Node.Type.Eat)
                    eatNodes.Add(nodes[x][y]);

                else if (nodeType == Node.Type.Coin)
                    coinNodes.Add(nodes[x][y]);

                found = true;
            }

        }

        if (nodes[x][y] == null)
            Debug.Log("Fallo en " + x.ToString() + ", " + y.ToString() + ". Color: " + pixelColor.r * 256);
    }

    List<Node> CalculateAdjacentNodes(Vector2Int coor)
    {
        List<Node> res = new List<Node>();

        if (coor.y < dim.y - 1)
            res.Add(nodes[coor.x][coor.y + 1]);
        else
            res.Add(null);

        if (coor.x > 0)
            res.Add(nodes[coor.x - 1][coor.y]);
        else
            res.Add(null);

        if (coor.y > 0)
            res.Add(nodes[coor.x][coor.y - 1]);
        else
            res.Add(null);

        if (coor.x < dim.x - 1)
            res.Add(nodes[coor.x + 1][coor.y]);
        else
            res.Add(null);

        return res;
    }

    public void Dispawn()
    {
        Destroy(wallsParent);
        Destroy(coinsParent);
        foreach (Ghost g in ghosts)
            Destroy(g.gameObject);

        foreach (Portal p in portals)
            Destroy(p.gameObject);

        scatterNodes.Clear();
    }

    public Player SpawnLevel(Player p = null)
    {
        ProcessInfo();
        wallsParent = new GameObject("WALLS");
        coinsParent = new GameObject("COINS");

        #region WALLS
        for (int i = 0; i < dim.x; i++)
        {
            for (int j = 0; j < dim.y; j++)
            {
                Node currentNode = nodes[i][j];

                if (currentNode.type == Node.Type.Wall)
                {
                    GameObject wall = Instantiate(wallPrefab, currentNode.position, Quaternion.identity);
                    wall.transform.parent = wallsParent.transform;
                    wall.name = "[" + currentNode.coordinates.x.ToString() + "][" + currentNode.coordinates.y.ToString() + "]";
                    GameObject extraWall = null;

                    //GameObject w = Instantiate(wallPrefab, currentNode.position, Quaternion.identity);
                    
                    List<Node> adyacentNodes = currentNode.adyacentNodes;
                    int nAdy = 0;
                    bool upAdy = false, leftAdy=false, downAdy=false, rightAdy=false;

                    for (int k=0; k<adyacentNodes.Count; k++)
                    {
                        if (adyacentNodes[k] != null)
                        {
                            
                            if (adyacentNodes[k].type == Node.Type.Wall)
                            {
                                nAdy++;

                                if (k == 0)
                                    upAdy = adyacentNodes[0].coordinates.y > currentNode.coordinates.y;
                                else if (k == 1)
                                    leftAdy = adyacentNodes[1].coordinates.x < currentNode.coordinates.x;
                                else if (k == 2)
                                    downAdy = adyacentNodes[2].coordinates.y < currentNode.coordinates.y;
                                else if (k == 3)
                                    rightAdy = adyacentNodes[3].coordinates.x > currentNode.coordinates.x;

                            }
                        }
                        
                    }

                    if (nAdy == 1)
                    {
                        // Nada?
                    }
                    else if (nAdy == 2)
                    {
                        if (upAdy)
                        {
                            if (leftAdy)
                            {   // .:
                                wall.transform.position = wall.transform.position + new Vector3(0, 0, 0.25f);
                                wall.transform.localScale = wall.transform.localScale + new Vector3(0, 0, -0.5f);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(-0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                            else if (downAdy)
                            { // :
                            }
                            else if (rightAdy)
                            { // :.
                                wall.transform.position = wall.transform.position + new Vector3(0, 0, 0.25f);
                                wall.transform.localScale = wall.transform.localScale + new Vector3(0, 0, -0.5f);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                        }
                        else if (downAdy)
                        {
                            if (rightAdy)
                            {  // :·
                                wall.transform.position = wall.transform.position + new Vector3(0, 0, -0.25f);
                                wall.transform.localScale = wall.transform.localScale + new Vector3(0, 0, -0.5f);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                            else if (leftAdy)
                            {   // ·:
                                wall.transform.position = wall.transform.position + new Vector3(0, 0, -0.25f);
                                wall.transform.localScale = wall.transform.localScale + new Vector3(0, 0, -0.5f);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(-0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                        }
                        else if (rightAdy)
                        {
                            if (leftAdy)
                            {   // --
                                wall.transform.rotation = Quaternion.Euler(0, 90, 0);
                            }
                        }
                    }
                    else if (nAdy == 3)
                    {
                        if (upAdy && downAdy)
                        {
                            if (leftAdy)
                            {   // -|
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(-0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                            else
                            {   // |- 
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.Euler(0, 90, 0));
                                extraWall.transform.position = extraWall.transform.position + new Vector3(0.25f, 0, 0);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                        }
                        else
                        {
                            if (upAdy)
                            {     // .:.
                                wall.transform.rotation = Quaternion.Euler(0, 90, 0);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.identity);
                                extraWall.transform.position = extraWall.transform.position + new Vector3(0, 0, 0.25f);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                            else
                            {   // ·:·
                                wall.transform.rotation = Quaternion.Euler(0, 90, 0);
                                extraWall = Instantiate(wallPrefab, currentNode.position, Quaternion.identity);
                                extraWall.transform.position = extraWall.transform.position + new Vector3(0, 0, -0.25f);
                                extraWall.transform.localScale = extraWall.transform.localScale + new Vector3(0, 0, -0.5f);
                            }
                        }
                    }
                    else if (nAdy == 4)
                    { // +
                      // Nada
                    }

                    if (extraWall != null)
                    {
                        extraWall.name = "AUX[" + currentNode.coordinates.x.ToString() + "][" + currentNode.coordinates.y.ToString() + "]";
                        extraWall.transform.parent = wallsParent.transform;
                    }

                }
            }
        }
        #endregion

        #region PLAYER
        Node playerSpawn = RandomSpawnNode();

        if (p == null) 
            player = Instantiate(GameController.instance.playerPrefab,
                    playerSpawn.position,
                    Quaternion.identity).GetComponent<Player>();
        else
            player = p;

        player.transform.position = playerSpawn.position;
        player.currentNode = playerSpawn;
        player.currentLevel = this;
        #endregion

        #region GHOSTS
        ghosts = new List<Ghost>();

        for (int i=0; i<4; i++)
        {
            Node randomNode = RandomHouseNode();
            Ghost instGhost = Instantiate(controller.ghostsPrefabs[i], randomNode.position, Quaternion.identity).GetComponent<Ghost>();
            instGhost.currentLevel = this;
            instGhost.currentNode = randomNode;
            instGhost.player = player;
            player.IncreaseSpeed();
            instGhost.SetSpeed(player.speed * 0.7f);
            ghosts.Add(instGhost);
        }
        #endregion

        #region PORTALS
        portals = new List<Portal>();
        if(portals1.Count == 2)
        {
            Portal p1 = Instantiate(controller.portalPrefab, portals1[0].position, Quaternion.identity).GetComponent<Portal>();
            p1.node = portals1[0];
            Portal p2 = Instantiate(controller.portalPrefab, portals1[1].position, Quaternion.identity).GetComponent<Portal>();
            p2.node = portals1[1];

            p1.otherPortal = p2;
            p2.otherPortal = p1;
            portals.Add(p1);
            portals.Add(p2);
        }
        if(portals2.Count == 2)
        {
            Portal p1 = Instantiate(controller.portalPrefab, portals2[0].position, Quaternion.identity).GetComponent<Portal>();
            p1.node = portals2[0];
            Portal p2 = Instantiate(controller.portalPrefab, portals2[1].position, Quaternion.identity).GetComponent<Portal>();
            p2.node = portals2[1];

            p1.otherPortal = p2;
            p2.otherPortal = p1;
            portals.Add(p1);
            portals.Add(p2);
        }
        if(portals3.Count == 2)
        {
            Portal p1 = Instantiate(controller.portalPrefab, portals3[0].position, Quaternion.identity).GetComponent<Portal>();
            p1.node = portals3[0];
            Portal p2 = Instantiate(controller.portalPrefab, portals3[1].position, Quaternion.identity).GetComponent<Portal>();
            p2.node = portals3[1];

            p1.otherPortal = p2;
            p2.otherPortal = p1;
            portals.Add(p1);
            portals.Add(p2);
        }
        #endregion

        #region COINS & EAT

        totalCoins = 0;
        eatedCoins = 0;

        for(int i=0; i<dim.x; i++)
        {
            for (int j = 0; j < dim.y; j++)
            {
                Node n = nodes[i][j];
                GameObject c = null;

                if (eatNodes.Contains(n))
                {
                    c = Instantiate(controller.eatPrefab, nodes[i][j].position, Quaternion.identity);
                    totalCoins++;
                }
                else if (coinNodes.Contains(n))
                {
                    c = Instantiate(coinPrefab, nodes[i][j].position, Quaternion.identity);
                    totalCoins++;
                }

                if (c != null)
                    c.transform.parent = coinsParent.transform;


            }
        }
        #endregion

        Time.timeScale = 1;

        return player;
    }

    public Node RandomNode()
    {
        int x = Random.Range(0, nodes.Length);
        int y = Random.Range(0, nodes[0].Length);

        return nodes[x][y];
    }

    
    public Node RandomSpawnNode()
    {
        return playerSpawnNodes[Random.Range(0, playerSpawnNodes.Count)];
    }
    public Node RandomHouseNode()
    {
        return houseNodes[Random.Range(0, houseNodes.Count)];
    }

    public Node RandomExitHouseNode()
    {
        return exitHouseNodes[Random.Range(0, exitHouseNodes.Count)];
    }

    public Wave GetWave(int i)
    {
        return waves[i];
    }

    public int GetNWaves() { return waves.Length; }
    
}
