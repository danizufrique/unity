﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum Direction { Up, Left, Down, Right}

    Rigidbody rb;

    [Range(7, 11)]
    public float speed;

    public KeyCode upKey, leftKey, downKey, rightKey;
    public Direction direction;
    private Vector3 inputAxis;
    public Node currentNode;
    public Level currentLevel;

    public int lives = 3;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    private void Update()
    {
        Move();
        UpdateCurrentNode();
    }

    public void Move()
    {
        if (Input.GetKeyDown(upKey)) {
            inputAxis = new Vector3(0, 0, 1);
            direction = Direction.Up;
        }

        else if (Input.GetKeyDown(leftKey)) { 
            inputAxis = new Vector3(-1, 0, 0);
            direction = Direction.Left;
        }

        else if (Input.GetKeyDown(downKey)) { 
            inputAxis = new Vector3(0, 0, -1);
            direction = Direction.Down;
        }

        else if (Input.GetKeyDown(rightKey)) { 
            inputAxis = new Vector3(1, 0, 0);
            direction = Direction.Right;
        }

        rb.velocity = inputAxis * speed;
    }

    public void IncreaseSpeed()
    {
        speed += 0.25f;
    }

    public void Teleport(Node node)
    {
        Debug.Log("Ss");
        transform.position = node.position;
        currentNode = node;
    }

    void UpdateCurrentNode()
    {
        if (Vector3.Distance(transform.position, currentNode.position) > .95f && currentNode!= null)
        {
            List<Node> orderedAdyacentNodes = currentNode.adyacentNodes.OrderBy(o => Vector3.Distance(o.position, transform.position)).ToList();

            bool finish = false;
            for (int i = 0; i < orderedAdyacentNodes.Count && !finish; i++)
            {
                if (orderedAdyacentNodes[i].isTransitable())
                {
                    currentNode = orderedAdyacentNodes[i];
                    finish = true;
                }
            }
        }
    }

    

    
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(currentNode.position, .5f);
    }
}
