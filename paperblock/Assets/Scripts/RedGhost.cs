﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class RedGhost : Ghost
{
    public override void Start()
    {
        type = Type.Red;
        base.Start();
    }

    public override bool canExitHouse()
    {
        return true;
    }

    protected override Node CalculateChaseNode()
    {
        return player.currentNode;
    }

    public override void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        base.OnDrawGizmos();
    }
}
