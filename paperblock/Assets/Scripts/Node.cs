﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class Node
{
    public enum Type { Wall, House, HouseExit, Red, Blue, Pink, Orange, Coin, Portal1, Portal2, Portal3, Fruit, Eat, Spawn,  Empty};
    public Vector2Int coordinates;
    public Vector3Int position;
    public Type type;
    public List<Node> adyacentNodes;    // Up, Left, Down, RIght
    
    public Node(Vector2Int coor, Type t)
    {
        adyacentNodes = new List<Node>();

        coordinates = coor;
        position = Vector3Int.zero + new Vector3Int(coordinates.x, 0, coordinates.y);
        type = t;
    }

    public bool isTransitable()
    {
        return type == Type.Coin || type == Type.Portal1 || type == Type.Portal2 || type == Type.Portal3 || type == Type.Fruit || type == Type.Eat || type == Type.Spawn || type == Type.Empty;
    }

    public bool isIntersection()
    {
        List<Node> transitables = new List<Node>();

        if (type == Type.Wall)
            return false;

        foreach(Node n in adyacentNodes)
        {
            if (n != null)
                if (n.isTransitable() || n.type == Type.HouseExit)
                    transitables.Add(n);
        }

        if (transitables.Count == 0)
            return false;
        else if (transitables.Count == 1) //Callejón
            return true;
        else if(transitables.Count == 2)  //Línea recta ó Giro
        {
            if (transitables[0].coordinates.x == transitables[1].coordinates.x)
                return false;
            if (transitables[0].coordinates.y == transitables[1].coordinates.y)
                return false;
        }

        return true;
    }

}
