﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : Pickable
{
    // Start is called before the first frame update
    void Start()
    {
        type = Type.Eat;
    }

    public override void Effect(Player p)
    {
        base.Effect(p);
        GameController.instance.AddPoints(10);
        GameController.instance.EatCoin();
        GameController.instance.StartFrightened();
    }
}
