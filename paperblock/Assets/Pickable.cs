﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    public enum Type{Coin, Fruit, Eat }

    public Type type;

    public virtual void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            Player p = other.gameObject.GetComponent<Player>();
            Effect(p);
            Destroy(this.gameObject);
        }
    }

    public virtual void Effect(Player p)
    {
        Debug.Log("You picked " + gameObject.name);
    }
}
