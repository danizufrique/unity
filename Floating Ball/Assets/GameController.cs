﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject enemy;
    public float spawnRate = 1f;
    float timer = 0;


    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.vSyncCount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > spawnRate)  {
            SpawnRandom();
            timer = 0;
        }
        else
            timer += Time.deltaTime;

        if(Input.GetKeyDown("p")){
            if(Time.timeScale == 1)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
        else if(Input.GetKeyDown("q"))
            Application.Quit();
    }

    public void SpawnRandom()
    {
         Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0,Screen.width), Screen.height, Camera.main.farClipPlane/2));
         GameObject instance = Instantiate(enemy,screenPosition,Quaternion.identity);
         instance.transform.parent = transform;
    }

    public void clear(){
        foreach(Transform child in transform){
            if(child != this.transform)
                Destroy(child.gameObject);
        }
    }

}
