﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb;
    CircleCollider2D col;
    GameController gameController;

    public float impulse = 12.5f;
    public  float velocity = 15f;
    public Text score;
    public Text record;
    public  Text deathText;

    float timer = 0, seconds = 0, minutes = 0;
    float record_sec, record_min;
    int deaths;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<CircleCollider2D>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        if(PlayerPrefs.HasKey("record_sec") )
            record_sec = PlayerPrefs.GetFloat("record_sec");
        else
            record_sec = 0;

        if(PlayerPrefs.HasKey("record_min") )
            record_min = PlayerPrefs.GetFloat("record_min");
        else
            record_min = 0;

            if(PlayerPrefs.HasKey("deaths") )
                deaths = PlayerPrefs.GetInt("deaths");
            else
                deaths = 0;


        string minutesS = record_min.ToString("00");
        string secondsS = record_sec.ToString("00");
        string deathS=deaths.ToString("0");

        record.text = string.Format("{0}:{1}", minutesS, secondsS);
        deathText.text = string.Format("{0}{1}", "x", deathS);
    }

    void Update()
    {
        Timer();
        PlayerInput();
    }

    void OnBecameInvisible(){
        Die();
    }

    void OnCollisionEnter2D(Collision2D col){
        Die();
    }

    void Timer(){
        timer += Time.deltaTime;

        minutes = Mathf.Floor(timer / 60);
        string minutesS = minutes.ToString("00");
        seconds = Mathf.Floor(timer % 60);
        string secondsS = seconds.ToString("00");

        score.text = string.Format("{0}:{1}", minutesS, secondsS);
    }

    void PlayerInput(){
        if(Input.GetKeyDown("space")){
            rb.velocity =  Vector2.zero;
            rb.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
        }

        float dir = Input.GetAxis("Horizontal") * velocity;
        rb.velocity = new Vector2(dir, rb.velocity.y);
    }

    void Die(){
        if(minutes >= record_min && seconds > record_sec){
            PlayerPrefs.SetFloat("record_sec", seconds);
            PlayerPrefs.SetFloat("record_min", minutes);
            record.text = string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
        }

        timer = 0;
        transform.position = Vector2.zero;
        deaths++;
        deathText.text = string.Format("{0}{1}", "x",  deaths.ToString("0"));
        PlayerPrefs.SetInt("deaths", deaths);
        rb.velocity = Vector2.zero;
        gameController.clear();
    }

}
