﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    PlayerCollision col;

    [Header("Stats")]
    [SerializeField] float speed = 10;
    [SerializeField] float jumpForce = 50;
    [SerializeField] float wallJumpLerp = 10;

    [Header("Booleans")]
    public bool canMove = true;
    public bool grounded = false;
    private bool wallJumped = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<PlayerCollision>();
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");

        #region Walk
        Vector2 walkDir = new Vector2(x, y);
        Walk(walkDir);
        #endregion

        #region Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(col.grounded)
                Jump(Vector2.up);
            else if (col.onWall)
                WallJump();                
        }
        #endregion

        if (col.grounded)
        {
            wallJumped = false;
        }

    }

    private void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        Vector2 velocity = Vector2.zero;

        if (!wallJumped)
            velocity = new Vector2(dir.x * speed, rb.velocity.y);
        else
        {
            Vector2 direction = new Vector2(dir.x * speed, rb.velocity.y);
            velocity = Vector2.Lerp(rb.velocity, direction, wallJumpLerp * Time.deltaTime);
        }

        rb.velocity = velocity;
    }

    private void Jump(Vector2 dir)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
    }

    private void WallJump()
    {
        wallJumped = true;
        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        rb.velocity = Vector2.zero;

        Vector2 wallDir = col.onRightWall ? Vector2.left : Vector2.right;
        Vector2 dir = (Vector2.up / 1.5f) + (wallDir / 1.5f);
        Jump(dir);
    }

    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
}
