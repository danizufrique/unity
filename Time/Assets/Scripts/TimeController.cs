﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    List<Rewindable> rewindableObjects;
    [SerializeField] float recordTime = 5f;
    void Start()
    {
        rewindableObjects = new List<Rewindable>();
        findRewindableObjects();
        setRecordTime(recordTime);
    }

    void Update()
    {
        
    }

    public void findRewindableObjects()
    {
        rewindableObjects = FindObjectsOfType<Rewindable>().ToList();
    }

    public void setRecordTime(float newTime)
    {
        foreach(Rewindable ro in rewindableObjects)
        {
            ro.recordTime = newTime;
        }
    }
}
