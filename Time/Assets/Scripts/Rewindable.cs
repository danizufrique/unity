﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class Rewindable : MonoBehaviour
{
    bool isRewindwing = false;
    public float recordTime = 5f;
    public float slowmoGravity = -0.2f;
    public float slowmoMass = 0.1f;
    bool rewindTask = false;

    List<TimeData> timeData;
    Rigidbody2D rb;

    void Start()
    {
        timeData = new List<TimeData>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            if(timeData.Count > 60)
                StartRewind();
        }
        else
        {
            StopRewind();
        }

        if (Input.GetMouseButton(0))
        {
            StartSlowmo();
        }
        else
        {
            StopSlowmo();
        }
    }

    void FixedUpdate()
    {
        if (isRewindwing)
        {
            if(rewindTask == false)
            {
                StartCoroutine(Rewind());
            }
        }
        else
        {
            Record();
        }
            
    }

    IEnumerator Rewind()
    {
        while (timeData.Count > 0 && isRewindwing)
        {
            TimeData pointInTime = timeData[0];
            transform.position = pointInTime.position;
            transform.rotation = pointInTime.rotation;
            timeData.RemoveAt(0);
            yield return StartCoroutine(WaitFrames(30));
        }
        
        StopRewind();
        
    }

    private IEnumerator WaitFrames(int frameCount)
    {
        while (frameCount > 0)
        {
            frameCount--;
            yield return null;
        }
    }

    void Record()
    {
        if (timeData.Count > Mathf.Round(recordTime / Time.fixedDeltaTime))
        {
            timeData.RemoveAt(timeData.Count - 1);
        }

        timeData.Insert(0, new TimeData(transform.position, transform.rotation));
    }

    void StartSlowmo()
    {
        rb.velocity = Vector2.zero;
        rb.mass = slowmoMass;
        rb.gravityScale = slowmoGravity;
    }

    void StopSlowmo()
    {
        rb.gravityScale = 1;
        rb.mass = 1;
    }

    void StartRewind()
    {
        isRewindwing = true;
        rb.isKinematic = true;
    }

    void StopRewind()
    {
        isRewindwing = false;
        rb.isKinematic = false;
    }
}
