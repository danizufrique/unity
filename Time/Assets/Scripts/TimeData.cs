﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeData
{
    public Vector2 position;
    public Quaternion rotation;

    public TimeData(Vector2 pos, Quaternion rot)
    {
        position = pos;
        rotation = rot;
    }
}
