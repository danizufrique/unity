﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField] GameObject bulletPrebaf;
    [SerializeField] Vector2 spawnOffset;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Vector2 spawnPos = new Vector2(transform.position.x + transform.localScale.x * spawnOffset.x,
                                            transform.position.y + spawnOffset.y);

            //Get the Screen positions of the object
            Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

            //Get the Screen position of the mouse
            Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

            //Get the angle between the points
            float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);


            GameObject bullet = Instantiate(bulletPrebaf, spawnPos, Quaternion.Euler(new Vector3(0f, 0f, angle)));

        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawWireCube((Vector2)transform.position + spawnOffset, new Vector3(.25f, .25f, 0));
    }
}
