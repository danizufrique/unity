﻿using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] LayerMask groundLayer;

    public bool grounded;
    public bool onWall;
    public bool onRightWall;
    public bool onLeftWall;

    [SerializeField] float colRadius = .25f;
    [SerializeField] Vector2 feetOffset;
    [SerializeField] Vector2 rightOffset;
    [SerializeField] Vector2 leftOffset;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = (Vector2)transform.position;

        grounded = Physics2D.OverlapCircle(pos + feetOffset, colRadius, groundLayer);
        
        onRightWall = Physics2D.OverlapCircle(pos + rightOffset, colRadius, groundLayer);
        onLeftWall = Physics2D.OverlapCircle(pos + leftOffset, colRadius, groundLayer);
        onWall = onRightWall || onLeftWall;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector2 pos = (Vector2)transform.position;

        Gizmos.DrawWireSphere(pos + feetOffset, colRadius);
        Gizmos.DrawWireSphere(pos + rightOffset, colRadius);
        Gizmos.DrawWireSphere(pos + leftOffset, colRadius);
    }
}
