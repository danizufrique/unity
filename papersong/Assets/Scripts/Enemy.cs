using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    

    protected Player player;
    protected IEnumerator idleCoroutine;
    protected IEnumerator attackCoroutine;

    public float idleTime;
    public float speed;
    public GameObject proyectile;

    private GameController.State currentState;
    private float timer;

    

    public virtual void Start() {
        idleCoroutine = IdleState();
        attackCoroutine = AttackState();

        StartCoroutine(idleCoroutine);
    }

    void Update(){
        if(currentState == GameController.State.Idle){
            timer += Time.deltaTime;
            
            if(timer >= idleTime){
                timer = 0;
                StopCoroutine(IdleState());
                StartCoroutine(AttackState());
            }
        }
    }
    public virtual void Shoot()
    {

    }

    protected virtual IEnumerator IdleState(){
        timer = 0;
        currentState = GameController.State.Idle;
        yield return null;

    }

    protected virtual IEnumerator AttackState(){
        currentState = GameController.State.Attack;
        yield return null;
    }

}
