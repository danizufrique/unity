﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{

    [Header("Layers")]
    public LayerMask groundLayer;

    [Header("Transforms")]
    Vector3 rightHand = Vector3.zero;
    Vector3 leftHand = Vector3.zero;
    Vector3 feet = Vector3.zero;
    BoxCollider2D col;
    public float collisionRadius = .25f;

    [Header("States")]
    public bool onGround;
    public bool onWall;
    public bool onWallL;
    public bool onWallR;

    private void Start()
    {
        col = GetComponent<BoxCollider2D>(); 
    }

    private void Update()
    {
        rightHand = transform.position + new Vector3(col.size.x/2, 0, 0);
        leftHand = transform.position + new Vector3(-col.size.x/2, 0, 0);
        feet = transform.position + new Vector3(0, -col.size.y/2, 0);

        onGround = Physics2D.OverlapCircle(feet, collisionRadius, groundLayer);
        onWall = Physics2D.OverlapCircle(rightHand, collisionRadius, groundLayer) ||
            Physics2D.OverlapCircle(leftHand, collisionRadius, groundLayer);

        onWallL = Physics2D.OverlapCircle(leftHand, collisionRadius, groundLayer);
        onWallR = Physics2D.OverlapCircle(rightHand, collisionRadius, groundLayer);


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(rightHand, collisionRadius);
        Gizmos.DrawWireSphere(leftHand, collisionRadius);
        Gizmos.DrawWireSphere(feet, collisionRadius);
    }
}
