﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 5;
    public float lifeTime = 10;
    public Enemy owner;

    public virtual void Start()
    {
        GetComponent<SpriteRenderer>().color = owner.GetComponent<SpriteRenderer>().color;
        Destroy(gameObject, lifeTime);
    }

    public virtual void Update()
    {
    }
}
