﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator anim;

    [Space]
    [Header("MOVEMENT")]
    private Vector2 currentVelocityRef;
    private Vector2 inputAxis;

    public float speed = 10;
    public float accelerationDuration = 0.5f;
    public float dashSpeed = 20;
    public float timeBtwDash = 0.5f;

    [Header("States")]
    public bool canDash = true;

    [Header("VFX")]
    public TrailRenderer trailRenderer;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void OnMovement(InputValue input)
    {
        Vector2 value = input.Get<Vector2>();
        inputAxis = new Vector2(value.x, value.y).normalized;
    }

    void OnParry()
    {
        Debug.Log("PARRY");
    }

    void OnDash()
    {
        if (canDash)
        {
            rb.velocity = inputAxis.normalized * dashSpeed;
            StartCoroutine(DashWait());
        }
    }

    IEnumerator DashWait()
    {
        canDash = false;
        trailRenderer.emitting = true;

        yield return new WaitForSeconds(timeBtwDash);

        canDash = true;
        trailRenderer.emitting = false;
    }

    

    private void Update()
    {
        rb.velocity = Vector2.SmoothDamp(rb.velocity, inputAxis * speed, ref currentVelocityRef, accelerationDuration, float.MaxValue, Time.deltaTime);
    }

}
