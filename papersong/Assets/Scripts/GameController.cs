using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public enum EnemyType{Square};
    public enum State { Idle, Dead, Attack };

    public static GameController instance;
    private Vector3[] screenCorners;
    Camera camera;

    void Awake(){
        if(instance == null){
            instance = this;
        }
    }

    void Start(){
        camera = Camera.main;

        screenCorners = new Vector3[4];
        screenCorners[0] = camera.ViewportToWorldPoint(new Vector3(0,0,camera.nearClipPlane));
        screenCorners[1] = camera.ViewportToWorldPoint(new Vector3(0,1,camera.nearClipPlane));
        screenCorners[2] = camera.ViewportToWorldPoint(new Vector3(1,0,camera.nearClipPlane));
        screenCorners[3] = camera.ViewportToWorldPoint(new Vector3(1,1,camera.nearClipPlane));
    }

    public Vector3 randomPoint(float offset=0){
        float x = Random.Range(screenCorners[0].x + offset, screenCorners[1].x - offset);
        float y = Random.Range(screenCorners[0].y + offset, screenCorners[2].y - offset);
        return new Vector3(x,y,0);
    }

}
