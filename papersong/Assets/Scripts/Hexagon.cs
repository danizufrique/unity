﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hexagon : Enemy
{
    PolygonCollider2D col;

    public override void Start()
    {
        col = GetComponent<PolygonCollider2D>();
        Invoke("Shoot", 5);
        Invoke("Shoot", 7);
        Invoke("Shoot", 9);
        Invoke("Shoot", 11);
    }

    public override void Shoot()
    {
        for (int i = 0; i < 6; i++) {
            Vector2 spawnPos = (Vector2)transform.position + col.points[i];
            FixedProjectile p = Instantiate(proyectile, spawnPos, Quaternion.identity).GetComponent<FixedProjectile>();
            p.owner = this;
            p.dir = spawnPos - (Vector2)transform.position;
        }
    }
}
