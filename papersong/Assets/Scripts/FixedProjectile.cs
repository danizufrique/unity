﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedProjectile : Projectile
{
    public Vector2 dir;

    // Update is called once per frame
    public override void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, (Vector2)transform.position + dir, speed * Time.deltaTime);
    }
}
