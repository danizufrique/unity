# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2019-07-13
### Changed
- Mark package to support Unity 2019.3.0a10 onwards.

## [2.0.0] - 2019-06-19
### Changed.
- Stable Version.

## [1.0.0-preview.3] - 2019-06-14
### Fixed
- Fix undo on select point.

## [1.0.0-preview.1] - 2019-04-22
### Added
- Initial Version.