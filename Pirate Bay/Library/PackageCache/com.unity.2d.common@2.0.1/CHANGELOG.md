# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2019-07-13
### Changed
- Mark package to support Unity 2019.3.0a10 onwards.

## [2.0.0] - 2019-6-17
### Added
- Drop preview tag.
- Remove experimental namespace

## [1.2.0-preview.2] - 2019-6-4
### Added
- Remove Image Packer Debug Window
- Move tests out of package

## [1.2.0-preview.1] - 2019-2-20
### Added
- Update for Unity 2019.2 support.

## [1.1.0-preview.2] - 2019-3-18
### Added
- Remove deprecated call to Unity internal API

## [1.1.0-preview.1] - 2019-1-25
### Added
- Added versioning for CI.