﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJump : MonoBehaviour
{
    public bool jumping;
    public float jumpForce;

    EnemyController ec;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        ec = GetComponent<EnemyController>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (jumping)
        {
            if (ec.grounded)
            {
                jumping = false;
                ec.anim.SetBool("Jumping", jumping);
            }
        }
    }

    public void Jump(float dir)
    {
        Debug.Log("JUMPPPPP");
        jumping = true;
        ec.anim.SetBool("Jumping", jumping);
        rb.velocity = Vector3.zero;
        rb.AddForce(new Vector2(dir * jumpForce, jumpForce));
    }
}
