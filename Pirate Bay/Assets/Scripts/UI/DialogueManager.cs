﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public Animator anim;

    public Text nameText;
    public Text dialogueText;
    public Image characterPhoto;

    private Queue<string> sentences;
    private GameObject player;

    InputHandler inputHandler;
    BombDamage bd;
    PlayerController pc;

    void Start()
    {
        sentences = new Queue<string>();
        player = GameObject.FindGameObjectWithTag("Player");

        inputHandler = player.GetComponent<InputHandler>();
        bd = player.GetComponent<BombDamage>();
        pc = player.GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("A"))
            DisplayNext();
        if (Input.GetButtonDown("B"))
            EndDialogue();
    }

    public void BeginDialogue(Dialogue dialogue)
    {
        anim.SetBool("Open", true);
        inputHandler.enabled = false;
        pc.hRaw = 0;
        bd.enabled = false;

        nameText.text = dialogue.name;
        characterPhoto.sprite = dialogue.characterSprite;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNext();
    }

    public void DisplayNext()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine("Type",sentence);
    }

    IEnumerator Type(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        anim.SetBool("Open", false);
        inputHandler.enabled = true;
        bd.enabled = true;
    }
}
