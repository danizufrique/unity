﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{

    public int currentHealth;
    public int maxHealth = 3;
    public GameObject[] heartPrefab;

    public void ChangeHealth(int newHealth)
    {
        for (int i = 0; i < newHealth; i++)
            heartPrefab[i].SetActive(true);
        for (int i = newHealth; i < maxHealth; i++)
            heartPrefab[i].SetActive(false);
    }
}
