﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParallaxEffect : MonoBehaviour
{
    public RawImage[] images;
    public float[] speeds;

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].uvRect = new Rect(images[i].uvRect.x + speeds[i] * Time.deltaTime,1,1,1);
        }
    }
}
