﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleTrajectory : MonoBehaviour
{
    PlayerController pc;

    Vector2 gravity;
    Vector2 launchVelocity;
    Vector2 initialPos;

    public float nPoints = 50;
    public float timeToCover = 4;
    public GameObject dotPrefab;

    float forceMultiplier;
    float maxLaunchForce;

    List<Vector2> positions;
    List<GameObject> instDots;
    

    private void Start() {
        gravity = Physics2D.gravity; //(0, -9.8)
        pc = gameObject.GetComponent<PlayerController>();

        maxLaunchForce = pc.maxLaunchForce; //250

        positions = new List<Vector2>();
        instDots = new List<GameObject>();       
    }

    private void Update()
    {
        if (Input.GetButton("X")) //Aiming
        {           
            forceMultiplier = pc.forceMultiplier;

            //Calculates all needed values
            if (forceMultiplier < 1)
            {
                ErasePoints();
                launchVelocity = CalculateVelocity();
                initialPos = transform.position;

                //Draw new trajectory
                SaveTrajectory();
                DrawTrajectory();
            }           
        }
        else //Not aiming
            ErasePoints();
    }

    Vector2 CalculatePoint(float t)
    {
        return gravity * t * t * 0.5f + launchVelocity/53 * t + initialPos; //That 53 is a very random value
    }

    Vector2 CalculateVelocity()
    {
        Vector2 result;

        float realForce = forceMultiplier * maxLaunchForce;
        result = new Vector2(1, 1) * realForce * pc.model.transform.localScale;

        return result;
    }

    void SaveTrajectory()
    {
        for (int i = 0; i < nPoints; i++)
        {
            float t = 4 / nPoints * i;
            Vector2 point = CalculatePoint(t);
            positions.Add(point);
        }
    }

    void DrawTrajectory()
    {
    
        for (int i = 0; i < positions.Count; i++)
        {
            GameObject dot = Instantiate(dotPrefab, positions[i], Quaternion.identity);
            instDots.Add(dot);
        }
    }

    void ErasePoints()
    {
        positions.Clear();
        ClearDots();
    }

    void ClearDots()
    {
        foreach (GameObject dot in instDots)
        {
            Destroy(dot);
        }

        instDots.Clear();
    }
}
