﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject interrogation;
    public Transform iconPosition;

    GameObject player;
    GameObject instIcon;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        interrogation.SetActive(true);

        instIcon.transform.parent = gameObject.transform;
        instIcon.transform.localScale = iconPosition.transform.localScale;
    }

    private void Update()
    {
        Vector2 distance = transform.position - player.transform.position;
        float module = distance.magnitude;

        if (module < 2f && Input.GetButtonDown("Y"))
        {
            /*
            if (transform.position.x - player.transform.position.x < 0)
                transform.localScale = Vector3.one;
            else
                transform.localScale = new Vector3(-1, 1, 1);
            */

            TriggerDialog();
        }

    }

    public void TriggerDialog()
    {
        FindObjectOfType<DialogueManager>().BeginDialogue(dialogue);
    }
}
