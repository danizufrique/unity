﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HandleHablityButtons : MonoBehaviour
{
    public void BombDamage(HandleUpgrades upgrades) { upgrades.cantBombDamaged = !upgrades.cantBombDamaged; }
    public void ChargeBomb(HandleUpgrades upgrades) { upgrades.canChargeBomb = !upgrades.canChargeBomb; }
    public void BombBar(HandleUpgrades upgrades) { upgrades.canShowBombBar = !upgrades.canShowBombBar; }
    public void Detonate(HandleUpgrades upgrades) { upgrades.canDetonate = !upgrades.canDetonate; }
    public void DetonateAll(HandleUpgrades upgrades) { upgrades.canDetonateALL = !upgrades.canDetonateALL; }
    public void Trajectory(HandleUpgrades upgrades) { upgrades.canSeeTrajectory = !upgrades.canSeeTrajectory; }
    public void KickBomb(HandleUpgrades upgrades) { upgrades.canKickBombs = !upgrades.canKickBombs; }
    public void BombPush(HandleUpgrades upgrades) { upgrades.cantBombPushed = !upgrades.cantBombPushed; }
    public void BombImpulse(HandleUpgrades upgrades) { upgrades.canBombJump = !upgrades.canBombJump; }   
    
    public void SetVolume(Slider slider)
    {
        AudioSource audio = GameObject.FindGameObjectWithTag("AudioSource").GetComponent<AudioSource>();

        audio.volume = slider.value;
    }

    public void LoadScene(int index)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        SaveFunctionality save;

        if (player != null)
        {
            save = player.GetComponent<SaveFunctionality>();
            save.SavePlayer();
        }

        Destroy(player);
        SceneManager.LoadScene(index);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Continue()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        SceneManager.LoadScene(data.sceneIndex);
    }
}
