﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleUpgrades : MonoBehaviour
{
    PlayerController controller;
    HandleTrajectory traject;
    BombDamage bd;
    Bomb bomb;

    [Tooltip("Determines if Player can be damaged by bombs.")]
    public bool cantBombDamaged;
    [Tooltip("Determines if Player can throw bombs.")]
    public bool canChargeBomb;
    [Tooltip("Determines if Player can know the time left for the bomb to explode.")]
    public bool canShowBombBar;
    [Tooltip("Determines if Player can detonate bombs when wanted.")]
    public bool canDetonate;
    [Tooltip("Determines if Player can detonate ALL bombs when wanted.")]
    public bool canDetonateALL;
    [Tooltip("Determines if Player can see bomb trajectory when throws bomb.")]
    public bool canSeeTrajectory;
    [Tooltip("Determines if Player can kick bombs.")]
    public bool canKickBombs;
    [Tooltip("Determines if Bomb pushes Player when it explodes.")]
    public bool cantBombPushed;
    [Tooltip("Determines if Player can use Bombs to jump higher.")]
    public bool canBombJump;
    [Tooltip("Determines if Player can Smash Down.")]
    public bool canSmash;

    void Start()
    {
        controller = gameObject.GetComponent<PlayerController>();
        traject = gameObject.GetComponent<HandleTrajectory>();
        bd = gameObject.GetComponent<BombDamage>();
        bomb = FindObjectOfType<Bomb>(); //doesn't work that way
    }

    private void Update()
    {
        if (cantBombDamaged)
            bd.damageable = false;
        else
            bd.damageable = true;

        if (canChargeBomb)
            controller.canChargeBomb = true;
        else
            controller.canChargeBomb = false;
        
        if (canShowBombBar)
            controller.canShowBombBar = true;
        else
            controller.canShowBombBar = false;

        if (canDetonate)
            controller.canDetonateBombs = true;
        else
            controller.canDetonateBombs = false;

        if (canDetonateALL)
            controller.canDetonateALLBombs = true;
        else
            controller.canDetonateALLBombs = false;

        if (canSeeTrajectory)
            traject.enabled = true;
        else
            traject.enabled = false;

        if (canKickBombs)
            controller.canKickBombs = true;
        else
            controller.canKickBombs = false;

        if (cantBombPushed)
            controller.canPushPlayer = false;
        else
            controller.canPushPlayer = true;

        if (canBombJump)
        {
            controller.canBombJump = true;
            controller.canDetonateBombs = true;
            controller.canPushPlayer = false;
            bd.damageable = false;
        }
        else
        {
            controller.canBombJump = false;

            if(!canDetonate)
                controller.canDetonateBombs = false;
            if(!cantBombPushed)
                controller.canPushPlayer = true;
            if (!cantBombDamaged)
                bd.damageable = true;
        }

        if (canSmash)
            controller.canSmash = true;
        else
            controller.canSmash = false;
    }

    public void EnableAll()
    {
        cantBombDamaged = true;
        canChargeBomb = true;
        canShowBombBar = true;
        canDetonate = true;
        canDetonateALL = true;
        canSeeTrajectory = true;
        canKickBombs = true;
        cantBombPushed = true;
        canBombJump = true;
        canSmash = true;
    }

    public void DisableAll()
    {
        cantBombDamaged = false;
        canChargeBomb = false;
        canShowBombBar = false;
        canDetonate = false;
        canDetonateALL = false;
        canSeeTrajectory = false;
        canKickBombs = false;
        cantBombPushed = false;
        canBombJump = false;
        canSmash = false;
    }
}
