﻿using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    #region Input
    [HideInInspector]
    public bool a_input;
    [HideInInspector]
    public bool a_inputDown;
    [HideInInspector]
    public bool x_input;
    [HideInInspector]
    public bool y_inputUp;
    [HideInInspector]
    public bool rb_inputUp;
    [HideInInspector]
    public bool b_inputDown;

    [HideInInspector]
    public float h, v;
    [HideInInspector]
    public float hRaw, vRaw;
    #endregion
    #region Components
    [HideInInspector]
    public Rigidbody2D rb;
    [HideInInspector]
    public Animator anim;
    public GameObject model;
    CapsuleCollider2D capsuleCol;
    private Vector2 capsuleInitialSize;
    #endregion
    #region Movement
    public float startMaxSpeed = 9;
    public float windSpeedModifier;
    float maxSpeed;
    public float runSpeed;
    public float jumpForce;
    public float fallMultiplier;
    public float lowJumpMultiplier;
    public float smashForce;
    public bool canMove = true;

    float maxJumpPressedTime = 0.3f;
    float jumpPressedTime = 0;
    #endregion
    #region States
    public bool grounded;
    public bool inWater;
    public bool inWind = false;
    public bool inAir = false;
    #endregion
    #region Habilities
    //Habilities   
    [HideInInspector]
    public bool damaged = false;
    [HideInInspector]
    public bool canGetBombDamage = false;
    [HideInInspector]
    public bool canChargeBomb = false;
    [HideInInspector]
    public bool canShowBombBar = false;
    [HideInInspector]
    public bool canDetonateBombs = false;
    [HideInInspector]
    public bool canDetonateALLBombs = false;
    [HideInInspector]
    public bool canKickBombs = false;
    [HideInInspector]
    public bool canPushPlayer = false;
    [HideInInspector]
    public bool canBombJump = false;
    [HideInInspector]
    public bool canSmash = false;
    #endregion
    #region Action
    [Header("Action")]
    public GameObject bombPrefab;
    public float coolDown;
    public float kickForceMultiplier;
    public float bombJumpStun;
    public Slider bombBar;
    float bombCountdown;
    float bombPressedTime = 0;
    [HideInInspector]
    public float forceMultiplier;
    List<GameObject> instBombs;
    public float maxLaunchForce;
    public float maxBombTimePressed;

    GameObject currentBomb = null;
    

    [HideInInspector]
    public Bomb nearestBomb;
    #endregion
    #region UI
    [Header("UI")]
    public GameObject hitBombEffect;
    #endregion
    #region Transforms
    [Header("Transforms")]
    public Transform backPos;
    public Transform feetPos;
    #endregion
    #region VFX
    [Header("VFX")]
    public GameObject jumpParticles;
    public GameObject landPaticles;
    #endregion

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        capsuleCol = GetComponent<CapsuleCollider2D>();
        capsuleInitialSize = capsuleCol.size;
        bombCountdown = coolDown;
        maxSpeed = startMaxSpeed;
        instBombs = new List<GameObject>();
    }

    private void Update()
    {
        if (canMove)
        {
            Movement();
            Action();
        }

        anim.SetFloat("Horizontal", Mathf.Abs(rb.velocity.x));
        anim.SetBool("Grounded", grounded);
    }

    void Movement()
    {
        #region Run
        if (inWind)
            maxSpeed = startMaxSpeed / windSpeedModifier;
        else
            maxSpeed = startMaxSpeed;

        if (Mathf.Abs(hRaw) > 0 || Mathf.Abs(vRaw) > 0)
        {
            Vector2 force = new Vector2(hRaw * runSpeed, 0);

            if (Mathf.Abs(rb.velocity.x) < maxSpeed)
                rb.AddForce(force);
                

            if (rb.velocity.x >= 0)
            {
                model.transform.localScale = Vector3.one;
                capsuleCol.offset = Vector2.zero;
            }
                
            else if (rb.velocity.x < 0)
            {
                model.transform.localScale = new Vector3(-1, 1, 1);
                capsuleCol.offset = new Vector2(-0.23f, 0);
            }              
        }

        else if (!inWind)
            rb.velocity = new Vector2(0, rb.velocity.y);
        #endregion

        #region Jump
        if (grounded && a_input)
        {
            Instantiate(jumpParticles, feetPos.transform.position, Quaternion.identity);
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.velocity += Vector2.up * jumpForce;
            inAir = true;
        }

        if (!grounded)
        {
            jumpPressedTime += Time.deltaTime;

            if (a_inputDown && jumpPressedTime > .3f && canSmash)
            {
                rb.AddForce(Vector2.down * smashForce);
                jumpPressedTime = 0;
            }
            
        }
        else
            jumpPressedTime = 0;
        #endregion
    }

    void Action()
    {
        GameObject bomb = null;

        //Instantiate bomb
        if (!canChargeBomb)
        {
            bombCountdown -= Time.deltaTime;

            if (x_input && bombCountdown <= 0)
            {
                bomb = Instantiate(bombPrefab, backPos.transform.position, transform.rotation);
                bombCountdown = coolDown;
            }
        }
        else
        {
            if (x_input)
            {
                bombPressedTime += Time.deltaTime;
                forceMultiplier = Mathf.Clamp01(bombPressedTime / maxBombTimePressed);
                bombBar.value = forceMultiplier;

                if (forceMultiplier > .1)
                {
                    //Enables bar
                    bombBar.transform.parent.gameObject.SetActive(true);
                    bombBar.transform.localScale = Vector2.one;
                }
            }

            else if (bombPressedTime > 0)
            {
                bomb = Instantiate(bombPrefab, backPos.transform.position, transform.rotation);

                ThrowForce(bomb);

                bombPressedTime = 0;
                bombBar.transform.parent.gameObject.SetActive(false);
                bombBar.value = 0;               
            }
        }

        //BombBar and Add Bombs to List
        if (bomb != null)
        {
            if (canShowBombBar)
                bomb.GetComponent<Bomb>().canShowSlider = true;
            else
                bomb.GetComponent<Bomb>().canShowSlider = false;

            if (canDetonateBombs || canDetonateALLBombs)
                instBombs.Add(bomb);
        }

        //Detonate Bombs (ESTÁ UN POCO FEO)     
        if (instBombs.Count > 0)
        {
            currentBomb = instBombs[0];

            if (y_inputUp)
            {
                int index = instBombs.Count - 1;
               
                if (currentBomb != null)
                    currentBomb.GetComponent<Bomb>().Explode();

                RemovePos(ref instBombs, index);
            }

            else if (rb_inputUp && canDetonateALLBombs)
            {
                foreach (GameObject obj in instBombs)
                {
                    if (obj != null)
                        obj.GetComponent<Bomb>().Explode();
                }

                instBombs.Clear();
            }

            //Checks if a bomb has already exploded and erases it from instbomb
            for (int i = 0; i < instBombs.Count; i++)
                if (instBombs[i] == null)
                    RemovePos(ref instBombs, i);
        }

        //KickBomb
        if(b_inputDown && nearestBomb != null && canKickBombs)
        {
            forceMultiplier = kickForceMultiplier;
            ThrowForce(nearestBomb.gameObject);
            Instantiate(hitBombEffect, nearestBomb.transform.position, Quaternion.identity);
            nearestBomb = null;
        }
    }

    void RemovePos(ref List<GameObject> list, int index)
    {
        //Copiar
        List<GameObject> copy = new List<GameObject>();
        GameObject element;

        for (int i = 0; i < list.Count; i++)
        {
            element = list[i];
            if(element != null)
                copy.Add(element);
        }

        //Liberar
        list.Clear();

        //Reasignar
        for (int i = 0; i < copy.Count; i++)
            list.Add(copy[i]);
    }

    public void ThrowForce(GameObject go)
    {
        Rigidbody2D rb = go.GetComponent<Rigidbody2D>();

        float realForce = forceMultiplier * maxLaunchForce;
        Vector2 forceDir = new Vector2(1, 1) * realForce * model.transform.localScale;

        forceMultiplier = 0; //Por que era?

        rb.AddForce(forceDir, ForceMode2D.Force);
    }

    public IEnumerator WaitToMove(float stun)
    {
        yield return new WaitForSeconds(stun);
        damaged = false;
        canMove = true;
        anim.SetBool("Hit", false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8 )//Ground
        {
            Instantiate(landPaticles, feetPos.transform.position, Quaternion.identity);
            inAir = false;
        }
    }  

}
