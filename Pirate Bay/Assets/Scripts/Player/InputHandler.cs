﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [Header("AXIS")]
    public float vertical;
    public float verticalRaw;
    public float horizontal;
    public float horizontalRaw;

    [Header("BUTTONS")]
    public bool b_input;
    public bool a_input;
    public bool a_inputDown;
    public bool x_input;
    public bool y_input;
    bool b_inputDown;
    bool y_inputUp;
    

    [Header("BUMPERS")]
    public bool rb_input;
    public bool lb_input;
    

    [Header("TRIGGERS")]
    public float rt_axis;
    public bool rt_input;
    public float lt_axis;
    public bool lt_input;
    bool rb_inputUp;

    [Header("JOYSTICK BUTTONS")]
    public bool leftAxis_down;
    public bool rightAxis_down;

    PlayerController controller;

    void Awake()
    {       
        controller = gameObject.GetComponent<PlayerController>();
    }

    void Update()
    {
        UpdateStates();
        GetInput();
    }

    void GetInput()
    {
        //Axis
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
        verticalRaw = Input.GetAxisRaw("Vertical");
        horizontalRaw = Input.GetAxisRaw("Horizontal");

        //Buttons
        a_input = Input.GetButton("A");
        a_inputDown = Input.GetButtonDown("A");
        b_input = Input.GetButton("B");
        x_input = Input.GetButton("X");      
        y_input = Input.GetButton("Y");
        b_inputDown = Input.GetButtonDown("B");
        y_inputUp = Input.GetButtonUp("Y");

        //Triggers
        rt_input = Input.GetButton("RT");
        rt_axis = Input.GetAxis("RT");
        if (rt_axis != 0)
            rt_input = true;

        lt_input = Input.GetButton("LT");
        lt_axis = Input.GetAxis("LT");
        if (lt_axis != 0)
            lt_input = true;

        //Bumpers
        rb_input = Input.GetButton("RB");      
        lb_input = Input.GetButton("LB");
        rb_inputUp = Input.GetButtonUp("RB");


        //Joystick Buttons
        leftAxis_down = Input.GetButton("L3");
        rightAxis_down = Input.GetButton("R3");


    }

    void UpdateStates()
    {
        controller.h = horizontal;
        controller.v = vertical;
        controller.hRaw = horizontalRaw;
        controller.vRaw = verticalRaw;

        controller.a_input = a_input;
        controller.a_inputDown = a_inputDown;
        controller.x_input = x_input;
        controller.b_inputDown = b_inputDown;
        controller.y_inputUp = y_inputUp;

        controller.rb_inputUp = rb_inputUp;
    }

    
}
