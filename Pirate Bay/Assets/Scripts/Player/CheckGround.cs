﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;
    public bool grounded;

    PlayerController pc;
    EnemyController ec;

    void Awake()
    {
        pc = gameObject.GetComponent<PlayerController>();
        ec = gameObject.GetComponent<EnemyController>();
    }

    private void Update()
    {
        grounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        if (pc != null)
            pc.grounded = grounded;
        else if (ec != null)
            ec.grounded = grounded;

    }
}
