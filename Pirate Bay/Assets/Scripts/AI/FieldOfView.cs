﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    EnemyController ec;

    public float viewRadius;
    public float scapeRadius;

    [HideInInspector]
    public float startViewAngle;
    [Range(0, 360)]
    public float viewAngle;
    public float delay = .2f;

    public LayerMask target;
    public LayerMask obstacle;

    public GameObject exclamation;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();

    Collider2D[] targetsRunAway;

    //[HideInInspector]
    public bool playerDetected = false;

    void Start()
    {
        ec = GetComponent<EnemyController>();
        startViewAngle = viewAngle;
        StartCoroutine(FindTargetsWithDelay(delay));
    }

    private void Update()
    {
        if (visibleTargets.Count > 0)
            exclamation.SetActive(true);
        
        targetsRunAway = Physics2D.OverlapCircleAll(transform.position, scapeRadius, target);
        if (targetsRunAway.Length == 0)
        {           
            ec.OnPlayerScape();
        }

        
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            if(!playerDetected && !ec.attacking)
                FindVisibleTargets();
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider2D[] targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, target);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            float angle = Vector3.Angle(transform.right, dirToTarget);

            if ((transform.localScale.x == 1 && angle < viewAngle / 2) || (transform.localScale.x == -1 && angle > (viewAngle + 180) / 2))
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics2D.Raycast(transform.position, dirToTarget, distanceToTarget, obstacle))
                {
                    visibleTargets.Add(target);
                    ec.OnSeePlayer(target.transform);
                    playerDetected = true;
                }               
            }

        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool isGlobal)
    {
        if (!isGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0);
    }
}
