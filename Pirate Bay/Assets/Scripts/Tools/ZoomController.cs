﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Cinemachine;

public class ZoomController : MonoBehaviour
{
    public float zoom;
    public float zoomSpeed = 0.1f;
    public bool reversible;

    CinemachineVirtualCamera cam;

    private void Start()
    {
        cam = GameObject.FindGameObjectWithTag("vCam").GetComponent<CinemachineVirtualCamera>();
    }

    void OnTriggerEnter2D(Collider2D col){
        if(col.gameObject.layer == 9)
            if (zoom != 0 && Mathf.Abs(zoom- cam.m_Lens.OrthographicSize)>0.3)
                StartCoroutine(Zoom(zoom));
    }

    void OnDrawGizmos(){
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position, new Vector3(transform.localScale.x, transform.localScale.y, 0));
    }

    
    IEnumerator Zoom(float newZoom)
    {
        float currentZoom = cam.m_Lens.OrthographicSize;

        if(reversible)
            zoom = currentZoom;

        if (currentZoom < newZoom)
        {
            while(currentZoom < newZoom)
            {
                float dZoom = currentZoom + zoomSpeed;

                cam.m_Lens.OrthographicSize += zoomSpeed;
                currentZoom = cam.m_Lens.OrthographicSize;
                yield return null;
            }
        }
        else if(currentZoom > newZoom)
        {
            while (currentZoom > newZoom)
            {
                float dZoom = currentZoom - zoomSpeed;
                cam.m_Lens.OrthographicSize = dZoom;
                currentZoom = cam.m_Lens.OrthographicSize;
                yield return null;
            }
        }       
    }
    

}
