﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilityEnabler : MonoBehaviour
{
    HandleUpgrades upgrades;

    public bool cantBombDamaged;
    public bool canChargeBomb;
    public bool canShowBombBar;
    public bool canDetonate;
    public bool canDetonateALL;
    public bool canSeeTrajectory;
    public bool canKickBombs;
    public bool cantBombPushed;
    public bool canBombJump;
    public bool canSmash;

    // Start is called before the first frame update
    void Start()
    {
        upgrades = GameObject.FindGameObjectWithTag("Player").GetComponent<HandleUpgrades>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 9)
        {
            if (cantBombDamaged)
                upgrades.cantBombDamaged = true;
            else
                upgrades.cantBombDamaged = false;

            if (canChargeBomb)
                upgrades.canChargeBomb = true;
            else
                upgrades.canChargeBomb = false;

            if (canShowBombBar)
                upgrades.canShowBombBar = true;
            else
                upgrades.canShowBombBar = false;

            if (canDetonate)
                upgrades.canDetonate = true;
            else
                upgrades.canDetonate = false;

            if (canDetonateALL)
                upgrades.canDetonateALL = true;
            else
                upgrades.canDetonateALL = false;

            if (canSeeTrajectory)
                upgrades.canSeeTrajectory = true;
            else
                upgrades.canSeeTrajectory = false;

            if (canKickBombs)
                upgrades.canKickBombs = true;
            else
                upgrades.canKickBombs = false;

            if (cantBombPushed)
                upgrades.cantBombPushed = true;
            else
                upgrades.cantBombPushed = false;

            if (canBombJump)
                upgrades.canBombJump = true;
            else
                upgrades.canBombJump = false;

            if (canSmash)
                upgrades.canSmash = true;
            else
                upgrades.canSmash = false;

        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(transform.localScale.x, transform.localScale.y, 0));
    }
}
