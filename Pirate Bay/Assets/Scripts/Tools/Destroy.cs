﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public bool activate = false;
    public bool wait = false;
    public float time = 0;
    float timer = 0;

    private void Update()
    {
        if (wait)
        {
            timer += Time.deltaTime;
            if (timer >= time)
                SelfDestroy();
        }
    }

    public void SelfDestroy()
    {
        if(activate)
            Destroy(gameObject);
    }

}
