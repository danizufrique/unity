﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(PlayerController)), CanEditMultipleObjects]
public class PlayerEditor : Editor
{
    bool comp_FoldOut = false;
    bool mov_FoldOut = false;
    bool states_FoldOut = false;
    bool action_FoldOut = false;
    bool ui_FoldOut = false;
    bool transforms_FoldOut = false;
    bool vfx_FoldOut = false;

    public override void OnInspectorGUI()
    {
        PlayerController controller = (PlayerController)target;


        EditorGUILayout.ObjectField(MonoScript.FromMonoBehaviour((PlayerController)target), typeof(PlayerController), false);

        //COMPONENTS
        comp_FoldOut = EditorGUILayout.Foldout(comp_FoldOut, "Components");
		
        if(comp_FoldOut)
		    controller.model = (GameObject)EditorGUILayout.ObjectField("Model", controller.model, typeof(GameObject), true);		
        
        //MOVEMENT
        mov_FoldOut = EditorGUILayout.Foldout (mov_FoldOut, "Movement");
        if (mov_FoldOut) {
            controller.canMove = EditorGUILayout.Toggle("Can Move", controller.canMove);

            controller.runSpeed = EditorGUILayout.FloatField("Run Speed", controller.runSpeed);
            controller.startMaxSpeed = EditorGUILayout.FloatField("Max Speed", controller.startMaxSpeed);
            controller.windSpeedModifier = EditorGUILayout.FloatField("Wind Speed Modifier", controller.windSpeedModifier);
            controller.jumpForce = EditorGUILayout.FloatField("Jump Force", controller.jumpForce);
            controller.bombJumpStun = EditorGUILayout.FloatField("Bomb Jump Stun", controller.bombJumpStun);
            controller.fallMultiplier = EditorGUILayout.FloatField("Fall Multiplier", controller.fallMultiplier);
            controller.lowJumpMultiplier = EditorGUILayout.FloatField("Low Jump Multiplier", controller.lowJumpMultiplier);
            controller.smashForce = EditorGUILayout.FloatField("Smash Force", controller.smashForce);
            
        }

        //STATES
        states_FoldOut = EditorGUILayout.Foldout(states_FoldOut, "States");
        if(states_FoldOut){
        	controller.grounded=EditorGUILayout.Toggle("Grounded", controller.grounded);
        	controller.inAir= EditorGUILayout.Toggle("In Air", controller.inAir);
        	controller.inWater= EditorGUILayout.Toggle("In Water", controller.inWater);
        	controller.inWind= EditorGUILayout.Toggle("In Wind", controller.inWind);
        }

        //ACTION
        action_FoldOut = EditorGUILayout.Foldout(action_FoldOut, "Action");
        if (action_FoldOut)
        {
            controller.bombPrefab = (GameObject)EditorGUILayout.ObjectField("Bomb Prefab", controller.bombPrefab, typeof(GameObject), true);
            controller.coolDown = EditorGUILayout.FloatField("Cool Down", controller.coolDown);
            controller.kickForceMultiplier = EditorGUILayout.FloatField("Kick Multiplier", controller.kickForceMultiplier);
            controller.maxLaunchForce = EditorGUILayout.FloatField("Launch Force", controller.maxLaunchForce);
            controller.maxBombTimePressed = EditorGUILayout.FloatField("Max Time Pressed", controller.maxBombTimePressed);
        }

        //UI
        ui_FoldOut = EditorGUILayout.Foldout(ui_FoldOut, "UI");
        if (ui_FoldOut)
        {
            controller.bombBar = (Slider)EditorGUILayout.ObjectField("Bomb Bar", controller.bombBar, typeof(Slider), true);
            controller.hitBombEffect = (GameObject)EditorGUILayout.ObjectField("Hit Bomb Effect", controller.hitBombEffect, typeof(GameObject), true);
        }

        //TRANSFORMS
        transforms_FoldOut = EditorGUILayout.Foldout(transforms_FoldOut, "Transforms");
        if (transforms_FoldOut)
        {
            controller.backPos = (Transform)EditorGUILayout.ObjectField("Back Position", controller.backPos, typeof(Transform), true);
            controller.feetPos = (Transform)EditorGUILayout.ObjectField("Feet Position", controller.feetPos, typeof(Transform), true);
        }

        //VFX
        vfx_FoldOut = EditorGUILayout.Foldout(states_FoldOut, "VFX");
        if (vfx_FoldOut)
        {
            controller.jumpParticles = (GameObject)EditorGUILayout.ObjectField("Jump Particles", controller.jumpParticles, typeof(GameObject), true);
            controller.landPaticles = (GameObject)EditorGUILayout.ObjectField("Land Particles", controller.landPaticles, typeof(GameObject), true);
        }
    }
}
    