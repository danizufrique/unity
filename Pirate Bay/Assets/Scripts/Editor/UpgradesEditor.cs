﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HandleUpgrades))]
public class UpgradesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        HandleUpgrades upgrades = (HandleUpgrades)target;

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Enable All"))
            upgrades.EnableAll();
        if (GUILayout.Button("Disable All"))
            upgrades.DisableAll();
        GUILayout.EndHorizontal();
    }
}
