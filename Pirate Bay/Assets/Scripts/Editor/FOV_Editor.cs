﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FieldOfView))]
public class FOV_Editor : Editor
{
    private void OnSceneGUI()
    {        
        FieldOfView fov = (FieldOfView)target;
        float sign = Mathf.Sign(fov.gameObject.transform.localScale.x);

        Handles.color = Color.cyan;
        Handles.DrawWireArc(fov.transform.position, Vector3.forward, Vector3.up, 360, fov.viewRadius);
        Vector3 viewAngleA = fov.DirFromAngle(-fov.viewAngle / 2, false);
        Vector3 viewAngleB = fov.DirFromAngle(fov.viewAngle / 2, false);

        Handles.color = Color.grey;
        Handles.DrawWireArc(fov.transform.position, Vector3.forward, Vector3.up, 360, fov.scapeRadius);

        Handles.color = Color.blue;
        Handles.DrawLine(fov.transform.position, fov.transform.position + sign * viewAngleA * fov.viewRadius);
        Handles.DrawLine(fov.transform.position, fov.transform.position + sign * viewAngleB * fov.viewRadius);

        Handles.color = Color.red;
        foreach (Transform visibleTarget in fov.visibleTargets)
            Handles.DrawLine(fov.transform.position, visibleTarget.position);
    }
}
