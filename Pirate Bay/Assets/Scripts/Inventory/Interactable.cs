﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius;
    public CircleCollider2D col;

    private void Start()
    {
        col.radius = radius/transform.localScale.x;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 && Input.GetButtonDown("Y"))
            Interact();
    }
    public virtual void Interact()
    {
        Debug.Log("Interacting with " + gameObject.name);
    }

     
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
