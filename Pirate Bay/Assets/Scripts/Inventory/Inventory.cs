﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;

    private void Awake()
    {
        if (instance != null)
            Debug.LogWarning("Mora than one inventory instance found");

        instance = this;
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 10;

    public List<Item> items = new List<Item>();

    public bool Add(Item item)
    {
        if (!item.isDefaultItem)
        {
            if(items.Count >= space)
            {
                Debug.Log("Not enough room");
                return false;
            }
            items.Add(item);

            if(onItemChangedCallback !=null)
                onItemChangedCallback.Invoke();
        }

        return true;    
            
    }

    public void Remove(Item item)
    {
        items.Remove(item);
        Debug.Log("Removing");

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

    public virtual void Use()
    {

    }
}
