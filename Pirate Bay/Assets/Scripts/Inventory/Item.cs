﻿
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName ="Inventory Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;
    public bool removable;

     
    
    public virtual bool Use()
    {
        bool success = false;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        HealthController healthController = GameObject.FindGameObjectWithTag("Player").GetComponent<HealthController>();

        if (name == "Heart")
        {
            if (healthController.health < healthController.maxHealth)
            {
                healthController.GetDamage(-1);
                success = true;
            }
        }
        else if(name == "Golden Skull")
        {
            
            bool isActive = player.transform.Find("Lintern").gameObject.activeSelf;
            Debug.Log(isActive);
            player.transform.Find("Lintern").gameObject.SetActive(!isActive);
        }
        return success;
    }
}
