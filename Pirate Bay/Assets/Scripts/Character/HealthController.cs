﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public int health;
    HealthBar healthBar;
    public int maxHealth = 3;
    Transform checkPoint;

    PlayerController pc;
    EnemyController ec;

    public Animator anim;

    private void Start()
    {
        
        pc = gameObject.GetComponent<PlayerController>();
        ec = gameObject.GetComponent<EnemyController>();

        if (pc != null) {
            healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<HealthBar>();
            checkPoint = GameObject.FindGameObjectWithTag("Checkpoint").transform;

            if (healthBar != null)
            {
                maxHealth = healthBar.maxHealth;
                healthBar.ChangeHealth(health);
            }
        }



    }

    public void GetDamage(int damage)
    {

        health -= damage;

        if (pc != null)
        {
            if (healthBar != null)
                healthBar.ChangeHealth(health);
        }
        else if(ec != null)
        {
            ec.healthBar.value = health;
        }

        if (health <= 0)
            StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        anim.SetBool("Dead", true);
        float length = anim.GetCurrentAnimatorClipInfo(0).Length;

        yield return new WaitForSeconds(length);

        if (checkPoint != null)
        {
            gameObject.transform.position = checkPoint.transform.position;
            pc.hRaw = 0;
            health = maxHealth;

            if (healthBar != null)
                healthBar.ChangeHealth(3);
        }
        else
            Destroy(gameObject);

        anim.SetBool("Dead", false);

        
    }
}
