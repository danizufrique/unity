﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class CannonController : MonoBehaviour
{

    Animator anim;

    public GameObject particles;
    public GameObject light;
    public GameObject bulletPrefab;
    public Transform spawn;
    public float shootForce;

    public float step;
    public float cadency;

    float timer = 0;
    float initialIntensity;
    bool cooled = true;

    private void Start()
    {
        anim = GetComponent<Animator>();
        initialIntensity = light.GetComponent<Light2D>().intensity;
    }

    private void Update()
    {
        if(cooled)
            timer += Time.deltaTime;

        if(timer >= cadency)
        {
            timer = 0;
            anim.SetBool("Shooting", true);
        }
    }

    public void Shoot()
    {
        Vector2 direction = (spawn.transform.position- transform.position).normalized;
        cooled = false;

        GameObject bullet = Instantiate(bulletPrefab, spawn.transform.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().AddForce(direction * shootForce);
        bullet.GetComponent<CircleCollider2D>().enabled = true;

        particles.SetActive(true);
        light.SetActive(true);
        
        StartCoroutine(CoolDown(step));
    }

    IEnumerator CoolDown(float step)
    {
        while (light.GetComponent<Light2D>().intensity > 0 && !cooled) {

            light.GetComponent<Light2D>().intensity -= step;

            if (light.GetComponent<Light2D>().intensity <= 0) {
                particles.SetActive(false);
                light.SetActive(false);
                light.GetComponent<Light2D>().intensity = initialIntensity;
                cooled = true;
                anim.SetBool("Shooting", false);
            }

            yield return null;
        }
        
    }
}
