﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class BombDamage : DAMAGE
{
    
    
    [HideInInspector]
    public Vector3 bombPos;

    
    [Header("BOMB DAMAGE CLASS")]
    public float bombJumpModifier;
    public float force;


    Rigidbody2D rb;

    private void Start()
    {
        if (damage <= 0 && gameObject.layer != 13) //Decoration
            Debug.LogWarning("La variable Damage de BombDamage.cs es " + damage + " en el objeto " + gameObject.name + ".");

        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    public void Explode()
    {
        Damage(gameObject, damage, damageable);
    }

    public override void Damage(GameObject go, int damage, bool canGetDamage)
    {
        
        if(canGetDamage)
            base.Damage(go, damage, damageable);

        if (pc != null)
        {
            if (pc.canPushPlayer || (pc.canBombJump && pc.y_inputUp && pc.inAir))
                AddExplosionForce(rb, force, transform.position);
        }
        else
            AddExplosionForce(rb, force, transform.position);
    }

    void AddExplosionForce(Rigidbody2D rb, float force, Vector2 targetPos)
    {
        
        Vector2 forceDir;
        Vector2 forceNor;

        forceDir = targetPos - new Vector2(bombPos.x, bombPos.y);
        forceNor = forceDir.normalized;
        rb.velocity = Vector2.zero;


        if (pc != null)
        {
            if (pc.canBombJump && rb.gameObject.layer == 9)
            { 
                pc.canMove = false;
                pc.StartCoroutine("WaitToMove", pc.bombJumpStun);
                rb.AddForce(forceNor * force / bombJumpModifier, ForceMode2D.Impulse);
            }
        }
        else
            rb.AddForce(forceNor * force, ForceMode2D.Force);
    }




}
