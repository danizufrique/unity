﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    Animator anim;
    Rigidbody2D rb;
    CircleCollider2D col;

    public int cannonDamage;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rb2d;
        BombDamage bd;

        //DAMAGE
        //.....   
        col.enabled = false;
        rb.velocity = Vector2.zero;
        anim.SetBool("Explode", true);
        rb.isKinematic = true;

        if(collision.gameObject.layer == 9 || collision.gameObject.layer == 10)
        {
            rb2d = collision.gameObject.GetComponent<Rigidbody2D>();
            bd = collision.gameObject.GetComponent<BombDamage>();

            if (rb2d != null && bd != null)
            {
                if (bd.enabled) //FALTA AÑADIR EL EMPUJE AUNQUE ALOMEJOR NO HACE FALTA
                {
                    bd.Damage(bd.gameObject, cannonDamage, true);                    
                }
            }
        }
    }


    public void Destroy()
    {
        Destroy(gameObject);
    }
}
