﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DAMAGE : MonoBehaviour
{
    [Header("DAMAGE CLASS")]
    HealthController health;
    public bool damageable;
    [HideInInspector]
    public PlayerController pc;
    public EnemyController ec;
    public int damage;
    public float stun;

    public virtual void Damage(GameObject go,int damage, bool canGetDamage)
    {
        health = go.GetComponent<HealthController>();

        pc = go.GetComponent<PlayerController>();
        ec = go.GetComponent<EnemyController>();

        if (pc != null)
        {
            if (canGetDamage)
            {
                
                StopAllCoroutines();
                pc.StartCoroutine(pc.WaitToMove(stun));
                pc.damaged = true;
                pc.canMove = false;
                pc.rb.velocity = Vector2.zero;
                pc.anim.SetBool("Hit", true);
                


            }
        }
        else if(ec != null)
        {
            StopAllCoroutines();
            ec.StartCoroutine(ec.WaitToMove(stun));
            ec.hit = true;
            ec.anim.SetBool("Hit", true);
        }

        if (health != null)        
            health.GetDamage(damage);
    }
}
