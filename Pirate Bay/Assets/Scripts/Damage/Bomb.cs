﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bomb : MonoBehaviour
{
    public float delay;
    public float radius;
    public Transform bombPos;

    [Header("Bar")]
    public Slider slider;
    public bool canShowSlider;

    float countdown;
    bool exploded = false;

    [Header("Habilities")]
    public bool canPushPlayer;
    public float bombJumpModifier;

    Animator anim;
    CircleCollider2D col;
    PlayerController pc;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        col = gameObject.GetComponent<CircleCollider2D>();
        pc = GameObject.FindObjectOfType<PlayerController>();
        
        countdown = delay;      
    }

    void Update()
    {
        if (canShowSlider && !exploded)
        {
            slider.transform.parent.gameObject.SetActive(true);
            slider.value = countdown / delay;
        }
        else
            slider.transform.parent.gameObject.SetActive(false);            

        countdown -= Time.deltaTime;

        if(countdown <= 0 && !exploded)
        {
            //effector.enabled = true;
            Explode();         
            
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }


    public void Explode()
    {
        exploded = true;

        slider.transform.parent.gameObject.SetActive(false);
        anim.SetBool("Exploded", exploded);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(gameObject.transform.position, radius);

        foreach(Collider2D nearbyObject in colliders)
        {
            Rigidbody2D rb = nearbyObject.GetComponent<Rigidbody2D>();
            BombDamage bd = nearbyObject.GetComponent<BombDamage>();
            DAMAGE damageClass = nearbyObject.GetComponent<DAMAGE>();

            

            if(rb != null && bd != null)
            {
                if (bd.enabled)
                {
                    Debug.Log(nearbyObject.name);
                    bd.bombPos = transform.position;
                    bd.bombJumpModifier = bombJumpModifier;
                    bd.Explode();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
            pc.nearestBomb = this;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
            pc.nearestBomb = null;
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
