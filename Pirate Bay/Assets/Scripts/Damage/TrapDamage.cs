﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDamage : DAMAGE
{
    //EnemyController enemyController;

    
    Rigidbody2D rb;
    Animator anim;

    [Header("TRAP CLASS")]
    public float jumpForce;

    private void Start()
    {
       
        rb = GetComponent<Rigidbody2D>();
        if(pc != null)
            anim = pc.anim;
              
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {        
        if(collision.gameObject.layer == 20)
            Damage(gameObject, damage, damageable);            
    }

    public override void Damage(GameObject go, int damage, bool canGetDamage)
    {
        float sign = Mathf.Sign(rb.velocity.x);
        base.Damage(go, damage, canGetDamage);

        
        Vector2 force = new Vector2(-sign * 1, 1).normalized * jumpForce;
        rb.AddForce(force, ForceMode2D.Impulse);
    }
}
