﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveFunctionality : MonoBehaviour
{
    
    //AsyncOperation operation = null;
    Vector3 position;

    public void SavePlayer()
    {
        
        SaveSystem.SavePlayer(this.gameObject);
    }

    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        PlayerController pc = GetComponent<PlayerController>();
        HandleUpgrades upgrades = GetComponent<HandleUpgrades>();

        //Update
        int sceneIndex = data.sceneIndex;
       
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        //Return      

        //operation = SceneManager.LoadSceneAsync(sceneIndex);

        gameObject.transform.position = position;

        upgrades.cantBombDamaged = data.cantBombDamaged;
        upgrades.canChargeBomb = data.canChargeBomb;
        upgrades.canShowBombBar = data.canShowBombBar;
        upgrades.canDetonate = data.canDetonate;
        upgrades.canDetonateALL = data.canDetonateALL;
        upgrades.canSeeTrajectory = data.canSeeTrajectory;
        upgrades.canKickBombs = data.canKickBombs;
        upgrades.cantBombPushed = data.cantBombPushed;
        upgrades.canBombJump = data.canBombJump;


    }

    /*
    private void Update()
    {
        if (operation != null)
        {
            if (operation.isDone)
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");

                Debug.Log("POS");
                gameObject.transform.position = position;
                operation = null;
            }
        }

    }
    
    private IEnumerator WaitUntilLoaded(AsyncOperation operation)
    {

        while (!operation.isDone)
        {
            yield return null;
        }
    }
    */
}
