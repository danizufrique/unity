﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PlayerData
{
    public int sceneIndex;
    public float[] position;

    public bool cantBombDamaged;
    public bool canChargeBomb;
    public bool canShowBombBar;
    public bool canDetonate;
    public bool canDetonateALL;
    public bool canSeeTrajectory;
    public bool canKickBombs;
    public bool cantBombPushed;
    public bool canBombJump;


    public PlayerData(GameObject player)
    {
        PlayerController pc = player.GetComponent<PlayerController>();
        HandleUpgrades upgrades = player.GetComponent<HandleUpgrades>();
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();


        position = new float[3];

        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;

        cantBombDamaged = upgrades.cantBombDamaged;
        canChargeBomb = upgrades.canChargeBomb;
        canShowBombBar = upgrades.canShowBombBar;
        canDetonate = upgrades.canDetonate;
        canDetonateALL = upgrades.canDetonateALL;
        canSeeTrajectory = upgrades.canSeeTrajectory;
        canKickBombs = upgrades.canKickBombs;
        cantBombPushed = upgrades.cantBombPushed;
        canBombJump = upgrades.canBombJump;

    }
}
