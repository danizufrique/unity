﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SetSaveButtons : MonoBehaviour
{
    public Button button;
    public bool save;
    GameObject player = null;
    SaveFunctionality saveSystem;


    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
            {
                button.onClick.AddListener(OnClick);
                saveSystem = player.GetComponent<SaveFunctionality>();
            }
        }
    }

    void OnClick()
    {
        if (save)
            saveSystem.SavePlayer();
        else
            saveSystem.LoadPlayer();
    }
}
