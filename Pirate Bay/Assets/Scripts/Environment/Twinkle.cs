﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twinkle : MonoBehaviour
{
    public UnityEngine.Experimental.Rendering.Universal.Light2D light;
    public float minIntensity;
    public float maxIntensity;
    public float speedMod;
    private float random;

    // Start is called before the first frame update
    void Start()
    {
        random = Random.Range(0.0f, 65535.0f);
    }

    // Update is called once per frame
    void Update()
    {
        float noise = Mathf.PerlinNoise(random, Time.time * speedMod);
        light.intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);

    }
}
