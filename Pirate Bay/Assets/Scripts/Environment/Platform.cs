﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Platform : MonoBehaviour
{
    Transform[] trajectoryArray;
    List<Vector3> trajectoryList;
    Rigidbody2D rb;
    Vector3 startPos;
    Quaternion startRot;

    public bool movable;
    public bool fallable;
    bool falling = false;

    public float shakeSpeed;
    public float shakeAmount;

    public float timeBeforeFall = 1;
    public float timeToReset = 1.5f;
    public float timer = 0;

    Vector3 nextPoint;
    int pointIndex = 0;
    bool forward = true;

    public float speed = 3;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPos = transform.position;
        startRot = transform.rotation;


        trajectoryList = new List<Vector3>();

        trajectoryArray = gameObject.GetComponentsInChildren<Transform>();
        trajectoryList.Add(gameObject.transform.position);

        for (int i = 1; i < trajectoryArray.Length; i++)
            trajectoryList.Add(trajectoryArray[i].transform.position);

        foreach (Transform point in trajectoryArray)
            point.transform.parent = null;

        if (trajectoryList.Count > 1)
            nextPoint = trajectoryList[pointIndex];
        
    }

    void FixedUpdate()
    {
        if (movable)
        {
            if (trajectoryList.Count > 0)
            {
                float step = speed * Time.fixedDeltaTime; // calculate distance to move
                transform.position = Vector3.MoveTowards(transform.position, nextPoint, step);
            }
            if (transform.position == nextPoint)
            {
                if (pointIndex == trajectoryList.Count - 1 && forward)
                    forward = false;
                else if (pointIndex == 0 && !forward)
                    forward = true;

                pointIndex = (forward) ? pointIndex + 1 : pointIndex - 1;

                nextPoint = trajectoryList[pointIndex];
            }
        }
        if (fallable && falling)
            Fall();
    }

    
    private void OnDrawGizmos()
    {
        if (movable)
        {
            if (Application.isPlaying && trajectoryList != null)
            {
                Gizmos.color = Color.magenta;
                foreach (Vector3 point in trajectoryList)
                    Gizmos.DrawSphere(point, .1f);

                Gizmos.color = Color.blue;
                for (int i = 0; i < trajectoryList.Count - 1; i++)
                    Gizmos.DrawLine(trajectoryList[i], trajectoryList[i + 1]);
            }
            else
            {
                trajectoryArray = gameObject.GetComponentsInChildren<Transform>();

                Gizmos.color = Color.magenta;
                foreach (Transform point in trajectoryArray)
                    Gizmos.DrawSphere(point.transform.position, .1f);

                Gizmos.color = Color.blue;
                for (int i = 0; i < trajectoryArray.Length - 1; i++)
                    Gizmos.DrawLine(trajectoryArray[i].transform.position, trajectoryArray[i + 1].transform.position);
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.gameObject.transform.parent = transform;

        if (collision.gameObject.layer == 9 && fallable)
            falling = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.gameObject.transform.parent = null;
    }

    void Fall()
    {
        Debug.Log("fef");
        timer += Time.fixedDeltaTime;

        if (timer >= timeBeforeFall)
        {
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.constraints = RigidbodyConstraints2D.None;
            gameObject.layer = 17;
        }
        else
        {
            float x = Random.Range(-shakeAmount, shakeAmount) * shakeSpeed * Time.deltaTime;
            float y = Random.Range(-shakeAmount, shakeAmount) * shakeSpeed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z);
        }

        if (timer >= timeToReset + timeBeforeFall)
        {
            falling = false;
            timer = 0;
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            gameObject.layer = 16;
            gameObject.transform.position = startPos;
            gameObject.transform.rotation = startRot;
            rb.velocity = Vector3.zero;
        }
        



    }

}
