﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterController : MonoBehaviour
{
    PlayerController pc;
    public GameObject splashEffect;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Instantiate(splashEffect, collision.transform.position, Quaternion.identity);

    }

}
