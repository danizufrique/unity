﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMobile : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed = 3;
    public Transform origin;
    public Transform target;
    Transform bubble;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

        origin.transform.parent = null;
        target.transform.parent = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target != null)
        {
            float step = speed * Time.fixedDeltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        }
        if (transform.position == target.position)
        {
            bubble = origin;
            origin = target;
            target = bubble;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(origin.transform.position, target.transform.position);
        Gizmos.DrawSphere(origin.transform.position, .1f);
        Gizmos.DrawSphere(target.transform.position, .1f);
    }
}
