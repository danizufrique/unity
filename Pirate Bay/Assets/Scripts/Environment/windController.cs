﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windController : MonoBehaviour
{
    public ParticleSystem effect;
    AreaEffector2D effector;
    public BoxCollider2D collider;

    public float effectLifeTime;
    public float windForce;

    public bool active = true;

    public bool burst = false;
    public float burstDuration;
    public float timeBtBurst;

    float startForce;
    float startLife;

    float burstTimer = 0;
    bool fading = false;

    private void Start()
    {
        effector = GetComponent<AreaEffector2D>();
        collider = GetComponent<BoxCollider2D>();

        startForce = windForce;
        startLife = effectLifeTime;
        effect.startLifetime = effectLifeTime;
        effector.forceMagnitude = windForce;
    }

    private void Update()
    {
        

        if (burst)
        {
            burstTimer += Time.deltaTime;

            if(burstTimer >= burstDuration && !fading)
            {
                burstTimer = 0;
                fading = true;
                
                
                StartCoroutine(FadeWindOut());
            }

            else if (!active)
            {
                burstTimer += Time.deltaTime;

                if(burstTimer >= timeBtBurst && !fading)
                {
                    burstTimer = 0;
                    fading = true;
                    StartCoroutine(FadeWindIn());
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 && active)
        {
            if(active)
                collision.gameObject.GetComponent<PlayerController>().inWind = true;
            else
                collision.gameObject.GetComponent<PlayerController>().inWind = false;
        }
            
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
            collision.gameObject.GetComponent<PlayerController>().inWind = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position, collider.size);
    }

    IEnumerator FadeWindOut()
    {
        float startLife = effectLifeTime;
        float startAlpha = effect.startColor.a;
        

        while (windForce > 0)
        {
            
            float newWindForce = windForce - 0.5f;
            float magnitude = newWindForce / startForce;
            float newAlpha = magnitude * startAlpha;

            windForce = startForce * magnitude;
            
            //effectLifeTime = startLife * magnitude;
            //effect.startLifetime = effectLifeTime;

            effector.forceMagnitude = windForce;
            Color newColor = new Color(1, 1, 1, newAlpha);

            effect.startColor = newColor;

            yield return null;
        }

        active = false;



        fading = false;
        

    }

    IEnumerator FadeWindIn()
    {
        
        windForce = .1f;
        effect.startColor = Color.white;

        while (windForce < startForce)
        {
            
            float newWindForce = windForce + 0.5f;
            float magnitude = newWindForce / startForce;

            Debug.Log(windForce);

            windForce = startForce * magnitude;

            //effectLifeTime = startLife * magnitude;
            //effect.startLifetime = effectLifeTime;

            effector.forceMagnitude = windForce;

            yield return null;
        }

        fading = false;
        active = true;

    }

}
