﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class DoorController : MonoBehaviour
{
    public bool isEntrance;
    public GameObject playerPrefab;

    CinemachineVirtualCamera vcam;
    GameObject model;
    GameObject player;
    PlayerController pc;
    SaveFunctionality saveSystem;

    Animator playerAnim;
    Animator anim;
    public int sceneIndex;

    private void Awake()
    {
        if (isEntrance)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
                Destroy(player);

            player = Instantiate(playerPrefab, transform.position, Quaternion.identity);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!isEntrance)
            player = GameObject.FindGameObjectWithTag("Player");

        anim = GetComponent<Animator>();
        vcam = GameObject.FindGameObjectWithTag("vCam").GetComponent<CinemachineVirtualCamera>();

        model = GameObject.FindGameObjectWithTag("Model");
        pc = player.GetComponent<PlayerController>();
        saveSystem = player.GetComponent<SaveFunctionality>();
        playerAnim = model.GetComponent<Animator>();

        if (isEntrance)
        {
            saveSystem.LoadPlayer();
            player.transform.position = this.transform.position;

            playerAnim.SetBool("ExitDoor", true);
            vcam.Follow = player.transform;
            pc.canMove = true;
        }     
               
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 9 && Input.GetButtonDown("Y")) {
            if (pc.canMove && !pc.inAir && !isEntrance)
            {
                saveSystem.SavePlayer();
                anim.SetBool("Open", true);
                pc.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pc.canMove = false;
            }
        } 
    }

    public void Opened()
    {
        playerAnim.SetBool("EnterDoor", true);
        float duration = playerAnim.GetCurrentAnimatorClipInfo(0).Length;
        StartCoroutine(LoadScene(duration));
    }

    IEnumerator LoadScene(float time)
    {
        saveSystem.SavePlayer();
        yield return new WaitForSeconds(time);
        Destroy(player);
        SceneManager.LoadScene(sceneIndex);
    }

}
