﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaldyAttack : DAMAGE
{
    LayerMask target;
    FieldOfView fov;
    ChaseBehaviour chase;
    EnemyController ec;
    float distanceToAttack;
    int damage = 1;

    private void Start()
    {
        fov = GetComponent<FieldOfView>();
        chase = GetComponent<ChaseBehaviour>();
        ec = GetComponent<EnemyController>();
        target = fov.target;
        distanceToAttack = chase.distanceToAttack;
    }

    public void Attack()
    {
        Collider2D[] nearby = Physics2D.OverlapCircleAll(transform.position, distanceToAttack, target);

        if(nearby.Length == 1)
        {
            Damage(nearby[0].gameObject, damage, true);
        }
    }

    public void EndAttack()
    {
        ec.attacking = false;
        ec.anim.SetBool("Attacking", false);
    }
}
