﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    #region Components
    Rigidbody2D rb;
    Animator anim;

    EnemyController ec;
    EnemyJump jump;

    public Transform checkStep;
    [Tooltip("[0]: left, [1]: right")]
    public Transform[] groundDetection;
    #endregion

    #region Movement
    public bool canPatrol = true;
    public bool canJump;
    public float speed;

    public float checkDistance;
    public float rayDistance;

    [HideInInspector]
    public bool movingRight = true; 
    bool secureStep;

    int currentPoint = 1;
    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        ec = GetComponent<EnemyController>();
        jump = GetComponent<EnemyJump>();

        for (int i = 0; i < groundDetection.Length; i++)
            groundDetection[i].parent = null;
    }

    void Update()
    {
        anim.SetFloat("Velocity",Mathf.Abs(rb.velocity.x));

        if (canPatrol && !ec.hit)
        {
            secureStep = Physics2D.Raycast(checkStep.position, Vector2.down, checkDistance);

            if (secureStep)
            {
                if (groundDetection.Length == 1)
                {
                    rb.velocity = new Vector2(speed, rb.velocity.y);
                    RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection[0].position, Vector2.down, rayDistance);

                    if (groundInfo.collider == false)
                    {
                        if (movingRight)
                        {
                            transform.localScale = new Vector3(-1, 1, 1);
                            movingRight = false;
                            speed *= -1;
                        }
                        else
                        {
                            transform.localScale = Vector3.one;
                            movingRight = true;
                            speed *= -1;
                        }
                    }
                }
                else if (groundDetection.Length == 2)
                {
                    rb.velocity = new Vector2(speed, rb.velocity.y);

                    if (movingRight)
                    {
                        if (transform.position.x >= groundDetection[currentPoint].position.x)
                        {
                            currentPoint = 0;
                            movingRight = false;
                            speed *= -1;
                        }
                    }
                    else if (transform.position.x <= groundDetection[currentPoint].position.x)
                    {
                        currentPoint = 1;
                        movingRight = true;
                        speed *= -1;
                    }



                }
                else { Debug.LogWarning("Wrong number of Ground Detection points in " + gameObject.name); }
            }
            else if (canJump && !jump.jumping) {
                float direction = Mathf.Sign(transform.localScale.x);
                jump.Jump(direction);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(checkStep.position, checkStep.position + new Vector3(0, -checkDistance, 0));
    }
}
