﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseBehaviour : MonoBehaviour
{
    #region Transforms
    public Transform checkStep;
    public Transform feetPos;
    public Transform target;
    #endregion

    #region Parameters
    public bool canJump;
    public float chaseSpeed;
    public float checkDistance;
    public float maxDistanceToJump;
    public float distanceToAttack;
    public LayerMask whatIsGround;      
    bool secureStep;
    #endregion

    #region Components
    EnemyController ec;
    Rigidbody2D rb;
    EnemyJump jump;
    #endregion

    void Start()
    {
        ec = GetComponent<EnemyController>();
        rb = GetComponent<Rigidbody2D>();
        jump = GetComponent<EnemyJump>();
    }

    void Update()
    {
        secureStep = Physics2D.Raycast(checkStep.position, Vector2.down, checkDistance);

        float sign = Mathf.Sign(target.transform.position.x - transform.position.x);

        if (ec.grounded)
        {
            if (target != null && Mathf.Abs(transform.position.x - target.transform.position.x) < distanceToAttack)
            {
                rb.velocity = Vector2.zero;
                ec.attacking = true;
                ec.anim.SetBool("Attacking", true);
            }

            else if (!ec.attacking)
            {
                if (secureStep && !ec.hit && !jump.jumping)
                    rb.velocity = new Vector2(sign * chaseSpeed, rb.velocity.y);

                else if (canJump)
                {
                    Ray2D downRay = new Ray2D(transform.position, Vector3.down);
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.down);

                    if (hit.distance <= maxDistanceToJump && !jump.jumping)
                    {
                        float dir = Mathf.Sign(transform.localScale.x);
                        jump.Jump(dir);
                    }
                }
                else
                    ec.OnPlayerScape();
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(checkStep.position, checkStep.position + new Vector3(0, -maxDistanceToJump, 0));
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(checkStep.position, checkStep.position + new Vector3(0, -checkDistance, 0));
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, distanceToAttack);
    }
}
