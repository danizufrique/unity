﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;

public class EnemyController : MonoBehaviour
{
    public Slider healthBar;
    public bool hit;
    HealthController healthController;
    [HideInInspector]
    public Animator anim;

    AIPath aiPath;
    AIDestinationSetter aiDestination;
    Patrol patrolScript;
    ChaseBehaviour chase;

    Rigidbody2D rb;
    FieldOfView fov;

    public bool grounded;

    public bool attacking = false;
    

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        aiPath = GetComponent<AIPath>();
        aiDestination = GetComponent<AIDestinationSetter>();
        patrolScript = GetComponent<Patrol>();
        chase = GetComponent<ChaseBehaviour>();
        fov = GetComponent<FieldOfView>();
    }

    public void OnSeePlayer(Transform player)
    {
        patrolScript.enabled = false;
        chase.enabled = true;
        chase.target = player;

        anim.speed = anim.speed * Mathf.Abs(chase.chaseSpeed / patrolScript.speed);
    }

    public void OnPlayerScape()
    {

        chase.target = null;
        chase.enabled = false;
        patrolScript.enabled = true;
        fov.exclamation.SetActive(false);

        fov.playerDetected = false;
        anim.speed = 1;

    }

    // Start is called before the first frame update
    void Start()
    {
        healthController = GetComponent<HealthController>();
        anim = GetComponent<Animator>();

        healthBar.maxValue = healthController.maxHealth;
        healthBar.value = healthController.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

        if(rb.velocity.x < -.1f)
            transform.localScale = new Vector3(-1, 1, 1);
        else if(rb.velocity.x > .1f)
            transform.localScale = Vector3.one;

    }

    
    public IEnumerator WaitToMove(float stun)
    {
        yield return new WaitForSeconds(stun);
        hit = false;
        anim.SetBool("Hit", false);
    }
    
}
