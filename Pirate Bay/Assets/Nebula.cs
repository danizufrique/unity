﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nebula : BombDamage
{
    [Header("NEBULA CLASS")]
    public float time;
    float timer = 0;
    Animator anim;

    public Twinkle twinkle;
    float initialMinIntensity, initialMaxIntensity;
    float initialAnimSpeed;

    private void Start()
    {
        anim = GetComponent<Animator>();

        initialMinIntensity = twinkle.minIntensity;
        initialMaxIntensity = twinkle.maxIntensity;
        initialAnimSpeed = anim.speed;
    }

    public override void Damage(GameObject go, int damage, bool canGetDamage)
    {
        timer = 0;

        twinkle.minIntensity = initialMinIntensity;
        twinkle.maxIntensity = initialMaxIntensity;
        anim.speed = initialAnimSpeed;
    }

    private void Update()
    {
        if (timer <= time)
        {
            timer += Time.deltaTime;
            float multiplier = (time - timer) / time;

            twinkle.minIntensity = multiplier * initialMinIntensity;
            twinkle.maxIntensity = multiplier * initialMaxIntensity;
            anim.speed = multiplier * initialAnimSpeed;
        }
    }
}
