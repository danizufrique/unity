using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card_Movement : Card
{
    public int blocks = 2;
    public float jumps = 10f;
    public float speed = 20f;
    public float maxAimAngle = 40f;

    protected override void Start()
    {
        base.Start();
        type = Type.Attack;

        effectSlots[0].text = blocks.ToString();
        effectSlots[1].text = jumps.ToString();
        effectSlots[2].text = speed.ToString();
        effectSlots[3].gameObject.SetActive(false);
    }

    public override void Play(PlayerSettings p)
    {
        p.EnterAimMode();

        p.shooting.maxForce = maxForce;
        p.shooting.maxAimAngle = maxAimAngle;
        p.shooting.chargeSpeed = chargeSpeed;
        p.shooting.nShots = nShots;
    }
}
