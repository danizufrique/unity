using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public GameObject cardPrefab;
    private Controller controller;
    public bool newCard;
    private List<GameObject> cards;
    public float spaceBetween = .2f;
    public float angleBetween = 4f;
    private int nCards;

    // Start is called before the first frame update
    void Start()
    {

        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<Controller>();
        newCard = false;
        nCards = 0;
        cards = new List<GameObject>();
        AddCard();
        AddCard();
        AddCard();
        AddCard();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AddCard()
    {
        GameObject c = Instantiate(cardPrefab, transform.position, transform.rotation, transform);
        c.GetComponent<Card>().deck = this;
        c.name = "Card" + (++nCards).ToString();
        cards.Add(c);
        DisposeCards();
        

    }

    public void PlayCard(Card c)
    {
        if (cards.Contains(c.gameObject))
        {
            cards.Remove(c.gameObject);
            nCards--;
            controller.PlayCard(c);
            Destroy(c.gameObject);
            DisposeCards();
        }
    }

    void DisposeCards()
    {
        for (int i = 0; i < nCards; i++)
        {
            float position = -((((float)nCards - 1) / 2) * spaceBetween) + (i * spaceBetween);
            cards[i].transform.localPosition = transform.position + new Vector3(position, 0, 0);
        }
    }

    private void OnValidate()
    {
        if (newCard)
        {
            AddCard();
            newCard = false;
        }
    }
}
