using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSettings:MonoBehaviour
{
    public KeyCode up, down, left, right;
    public KeyCode jump;
    public KeyCode shoot;
    public  Movement movement;
    public  Shooting shooting;
    public Material color;
    public CinemachineVirtualCamera camera;

    private void Start()
    {
        movement = GetComponent<Movement>();
        shooting = GetComponent<Shooting>();

        movement.enabled = false;
        shooting.enabled = false;
        GetComponentInChildren<SkinnedMeshRenderer>().material = color;
    }

    public virtual void PlayCard(Card card)
    {
        card.Play(this);
    }

    public void EnterAimMode()
    {
        camera.enabled = true;
        shooting.enabled = true;
        shooting.EnablePoints(true);
    }

    public void ExitAimMode()
    {
        camera.enabled = false;
        shooting.enabled = false;
        shooting.EnablePoints(false);
    }
}
