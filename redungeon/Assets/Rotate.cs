using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [Range(0.5f, 30f)]
    public float acceleration = 1;
    public float maxSpeed = 10;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(Vector3.up * maxSpeed);
        /*
        Quaternion deltaRotation = Quaternion.Euler(new Vector3(0,speed,0) * Time.deltaTime);
        rb.MoveRotation(rb.rotation * deltaRotation);
        //transform.Rotate(Vector3.up, 1 * speed);
        */
    }
}
