using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    private PlayerSettings playerSettings;
    public GameObject arrow;
    public GameObject point;
    public GameObject bow;
    public Transform spawn;


    public float maxForce = 10f;
    public float maxAimAngle = 40f;
    public float chargeSpeed = 20f;
    public int nShots = 2;

    public Vector2 aimDeadZone;
    private float force = 0f;
    public int nTrayectoryPoints;
    public float spaceBtwPoints;
    private GameObject[] trayectoryPoints;
    private GameObject aimPointsParent;

    void Start()
    {
        playerSettings = GetComponent<PlayerSettings>();
        force = 0;
        trayectoryPoints = new GameObject[nTrayectoryPoints];
        aimPointsParent = new GameObject("Aim Points");
        aimPointsParent.transform.parent = this.transform;

        for(int i=0; i<nTrayectoryPoints; i++)
        {
            trayectoryPoints[i] = Instantiate(point, spawn.position, Quaternion.identity);
            trayectoryPoints[i].transform.parent = aimPointsParent.transform;
            trayectoryPoints[i].SetActive(false);
        }

        UpdateMaterial(bow.GetComponentInChildren<MeshRenderer>(), 1);
    }

    void UpdateMaterial(MeshRenderer renderer, int index)
    {
        var mats = renderer.materials;
        mats[index] = playerSettings.color;
        renderer.materials = mats;
    }

    public void EnablePoints(bool active)
    {
        for (int i = 0; i < nTrayectoryPoints; i++)
            trayectoryPoints[i].SetActive(active);
    }
    
    void Update()
    {
        Vector3 input = NormalisedMouseInput();

        if (Input.GetMouseButton(0))
        {
            if(force<maxForce)
                force += Time.deltaTime * chargeSpeed;

            for(int i=0; i<nTrayectoryPoints; i++)
                trayectoryPoints[i].transform.position = pos(i * spaceBtwPoints);

            bow.transform.localRotation = Quaternion.Euler(-input.y * maxAimAngle,0,0);

            if(Math.Abs(input.x) > aimDeadZone.x)
                transform.rotation = transform.rotation * Quaternion.Euler(0, input.x, 0);
        }
        if (Input.GetMouseButtonUp(0))
        {
            Shoot();
            force = 0;
        }
    }

    

    void Shoot()
    {
        GameObject spawnArrow = Instantiate(arrow, spawn.position, spawn.rotation);
        MeshRenderer renderer = spawnArrow.GetComponentInChildren<MeshRenderer>();
        UpdateMaterial(renderer, 2);
        spawnArrow.GetComponent<Rigidbody>().velocity = spawn.transform.forward * force;
        nShots--;
        if(nShots <= 0)
        {
            playerSettings.ExitAimMode();
        }
    }

    Vector2 NormalisedMouseInput()
    {
        return new Vector2(
            Mathf.Clamp((Input.mousePosition.x / Screen.width) * 2 - 1, -1.0F, 1.0F),
            Mathf.Clamp((Input.mousePosition.y / Screen.height) * 2 - 1, -1.0F, 1.0F)
            );
    }

    Vector3 pos(float t)
    {
        Vector3 pos = spawn.position + (spawn.transform.forward * force * t) + 0.5f * Physics.gravity * (t*t);
        return pos;
    }
}
