using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    private PlayerSettings playerSettings;
    private Rigidbody _rb;
    private GameObject _mainCamera;
    private Animator _anim;
    private CapsuleCollider _collider;
    private Task waitToMove;

    private bool _isGrounded;

    public LayerMask groundLayer;
    public float gravity = 9.8f;
    public float speed = 10f;
    public float jumpForce = 10f;
    public float bounceForce = 200f;
    public float knockBackTime = 0.5f;

    public bool canMove;
    public int maxBlocks = 5;
    private GameObject lastBlock;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _anim = GetComponent<Animator>();
        _collider = GetComponentInParent<CapsuleCollider>();
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        
        lastBlock = null;
        playerSettings = GetComponent<PlayerSettings>();
        canMove = true;
    }

    void Update()
    {
        _isGrounded = Physics.CheckSphere(transform.position, .25f, groundLayer, QueryTriggerInteraction.Ignore);
        if (_isGrounded)

        if (canMove)
        {
            Jump();
        }

        UpdateAnimator();
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            Walk();
        }
    }

    private Vector3 ReadInput()
    {
        Vector3 input = new Vector3();
        if (Input.GetKey(playerSettings.left))
            input.x = -1;
        else if (Input.GetKey(playerSettings.right))
            input.x = 1;
        else input.x = 0;
        
        if (Input.GetKey(playerSettings.up))
            input.z = 1;
        else if (Input.GetKey(playerSettings.down))
            input.z = -1;
        else input.z = 0;

        var forward = _mainCamera.transform.forward;
        var right = _mainCamera.transform.right;

        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        //this is the direction in the world space we want to move:
        var desiredMoveDirection = forward * input.z + right * input.x;

        return desiredMoveDirection;
    }
    
    private void Walk()
    {
        Vector3 move = ReadInput().normalized;
        
        if (move != Vector3.zero)
            transform.forward = move;

        _rb.velocity = new Vector3(move.x * speed, _rb.velocity.y, move.z*speed);
    }

    private void Jump()
    {
        if (Input.GetKeyDown(playerSettings.jump) && _isGrounded)
            _rb.AddForce(0, jumpForce, 0, ForceMode.Impulse);
    }

    void UpdateAnimator()
    {
        Vector2 horizontalVelocity = new Vector2(_rb.velocity.x, _rb.velocity.z);
        _anim.SetFloat("Speed", horizontalVelocity.magnitude);
        _anim.SetBool("Grounded", _isGrounded);
    }

    void KnockBack(Vector3 dir)
    {
        Debug.Log(dir);
        _rb.velocity = Vector3.zero;
        _rb.AddForce(dir * bounceForce, ForceMode.Impulse);
    }

    IEnumerator WaitToMove(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }

    void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            if (lastBlock == null || hit.gameObject != lastBlock)
            {
                lastBlock = hit.gameObject;
                maxBlocks--;
            }
        }
        else if(hit.gameObject.layer == LayerMask.NameToLayer("Bounce"))
        {
            Debug.Log("aa");
            Vector3 dir = Vector3.zero;
            if (hit.contactCount > 0)
            {
                
                dir = (hit.contacts[0].normal).normalized;
                float force = hit.gameObject.GetComponent<Rotate>().maxSpeed;
                /*
                Debug.Log(hit.gameObject.GetComponent<Rigidbody>().inertiaTensor.magnitude);
                dir *= hit.gameObject.GetComponent<Rigidbody>().inertiaTensor.magnitude;
                */

                KnockBack(dir * force);

                StopCoroutine("WaitToMove");
                StartCoroutine(WaitToMove(knockBackTime));
            }
        }
    }
}

