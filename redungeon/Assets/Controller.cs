using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public PlayerSettings[] players;
    public int turn;

    private void Start()
    {
        GameObject[] p = GameObject.FindGameObjectsWithTag("Player");
        players = new PlayerSettings[p.Length];

        for(int i=0; i<p.Length; i++)
        {
            players[i] = p[i].GetComponent<PlayerSettings>();
        }

        turn = 0;
    }

    public void PlayCard(Card c)
    {
        players[turn].PlayCard(c);
    }
}
