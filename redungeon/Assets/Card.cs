using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;


public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector3 originalSize;
    public float mouseOverAmplify = 1.1f;
    private bool mouseOver;
    private Vector3 position;
    private Canvas myCanvas;
    public Deck deck;
    public enum Type { Attack, Walk}
    public Type type;
    public List<TextMeshProUGUI> effectSlots;

    protected virtual void Start()
    {
        myCanvas = GetComponentInParent<Canvas>();
        originalSize = transform.localScale;
        mouseOver = false;
    }

    void Update()
    {
        if (mouseOver)
        {
            transform.localScale = originalSize * mouseOverAmplify;
        }
        else
        {
            transform.localScale = originalSize;
        }
    }

    public virtual void Play(PlayerSettings p) { }
    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOver = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        position = transform.position;
    }

    public void OnDrag(PointerEventData data)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
        transform.position = myCanvas.transform.TransformPoint(pos);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
       

        if(transform.localPosition.y > 300)
        {
            deck.PlayCard(this);
        }
        else
            transform.position = position;
    }
}
