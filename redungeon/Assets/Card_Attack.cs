public class Card_Attack : Card
{
    public int nShots = 2;
    public float maxForce = 10f;
    public float chargeSpeed = 20f;
    public float maxAimAngle = 40f; 

    protected override void Start()
    {
        base.Start();
        type = Type.Attack;

        effectSlots[0].text = nShots.ToString();
        effectSlots[1].text = maxForce.ToString();
        effectSlots[2].text = chargeSpeed.ToString();
        effectSlots[3].text = maxAimAngle.ToString();
    }

    public override void Play(PlayerSettings p)
    {
        p.EnterAimMode();

        p.shooting.maxForce = maxForce;
        p.shooting.maxAimAngle = maxAimAngle;
        p.shooting.chargeSpeed = chargeSpeed;
        p.shooting.nShots = nShots;
    }

}
