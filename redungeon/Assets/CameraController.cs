using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private GameObject[] targets;
    public Vector3 offset;
    public float smoothTime = .5f;
    private Vector3 velocity;
    private CinemachineVirtualCamera[] otherCameras;
    private CinemachineVirtualCamera myCamera;

    public float speed;
    public float minDistance = 10;
    public float maxDistance = 3;

    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Player");
        myCamera = GetComponent<CinemachineVirtualCamera>();
        otherCameras = GameObject.FindObjectsOfType<CinemachineVirtualCamera>();
    }

    private void Update()
    {
        bool otherCamerasEnabled = false;
        foreach (CinemachineVirtualCamera c in otherCameras)
        {
            if (c.enabled) { 
                otherCamerasEnabled = true;
                break;
            }
        }
        myCamera.enabled = !otherCamerasEnabled;
    }
    void LateUpdate()
    {
        if (targets.Length == 0)
            return;

        Vector3 center = CalculateCenter();
        Vector3 newPos = center + offset;

        //transform.position = Vector3.SmoothDamp(transform.position, newPos, ref velocity, smoothTime);
        transform.LookAt(center);

        if (Input.GetMouseButtonDown(1)) {
            foreach(GameObject g in targets)
            {
                g.GetComponent<Movement>().canMove = false;
            }
        }
        if (Input.GetMouseButton(1))
        {
            float v = Input.GetAxis("Vertical");
            float d = Vector3.Distance(transform.position, center);

            if (v > 0)
            {
                if (d > maxDistance)
                    transform.position = Vector3.MoveTowards(transform.position, center, Input.GetAxis("Vertical"));
            }
            else if (v < 0)
            {
                if (d < minDistance)
                    transform.position = Vector3.MoveTowards(transform.position, center, Input.GetAxis("Vertical"));
            }

            transform.RotateAround(center, transform.up, -Input.GetAxis("Horizontal") * speed);
        }
        /*else
            transform.RotateAround(center, transform.right, Input.GetAxis("Vertical") * speed);*/
        if (Input.GetMouseButtonUp(1))
        {
            foreach (GameObject g in targets)
            {
                g.GetComponent<Movement>().canMove = true;
            }
        }
    }

    Vector3 CalculateCenter()
    {
        if (targets.Length == 1)
            return targets[0].transform.position;

        var bounds = new Bounds(targets[0].transform.position, Vector3.zero);
        for(int i=0; i<targets.Length; i++)
        {
            bounds.Encapsulate(targets[i].transform.position);
        }

        return bounds.center;

    }
}
