﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class Pawn : Piece
{
    public GameObject promotionPanel;
    public override GameController.Movement ValidMove(Cell cell)
    {
        if (cell == null)
            return null;

        if (cell == currentCell)
            return null;

        if (cell.currentPiece != null)
        {
            if (cell.currentPiece.color == this.color)
                return null;
        }

        Vector2Int dif = cell.coordinates - currentCell.coordinates;
        int factor = (color == GameController.PieceColor.White) ? 1 : -1;

        if (dif.x < 0 && color == GameController.PieceColor.White)
            return null;
        if (dif.x > 0 && color == GameController.PieceColor.Black)
            return null;

        if (dif.y != 0)
        {
            if (Math.Abs(dif.y) == 1 && Math.Abs(dif.x) == 1)   // Diagonal
            {
                if (cell.currentPiece != null)
                {
                    if (cell.currentPiece.color != color)
                        return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
                    else
                        return null;
                }
                else
                {
                    int enPassantPawnCoorX = (color == GameController.PieceColor.White) ? cell.coordinates.x - 1: cell.coordinates.x + 1;
                    Piece p = controller.GetPiece(enPassantPawnCoorX, cell.coordinates.y);

                    if (p != null)
                    {
                        if(p.type == GameController.PieceType.Pawn)
                        {
                            if (p.movementRecord.Count > 0)
                            {
                                GameController.Movement pieceLast, gameLast;
                                pieceLast = p.LastMove();
                                gameLast = controller.LastMove(p.color);

                                bool res1 = pieceLast.type == GameController.MovementType.DoubleStart;
                                bool res2 = GameController.Movement.Equals(pieceLast, gameLast);

                                if (res1 && res2)
                                {
                                    return new GameController.Movement(GameController.MovementType.Passant, this, currentCell, cell, p);
                                }
                            }
                        }
                    }
                    return null;
                }
            }
            else
                return null;
        }

        else if (movementRecord.Count == 0 && Math.Abs(dif.x) == 2)  // Salida Doble
        {
            if (cell.currentPiece == null)
            {
                if (cell.currentPiece == null)
                {
                    return new GameController.Movement(GameController.MovementType.DoubleStart, this, currentCell, cell);
                
                }
            }
            return null;
        }
            
        else if (Math.Abs(dif.x) == 1)
        {
            if(cell.currentPiece == null)
            {
                if((cell.coordinates.x == 7 && color == GameController.PieceColor.White) || (cell.coordinates.x == 0 && color == GameController.PieceColor.Black))
                    return new GameController.Movement(GameController.MovementType.Promotion, this, currentCell, cell);
                else
                {
                    return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
                }
            }
        }

        return null;
    }

    public override void ComputeMovements()
    {
        posibleMovements.Clear();

        if(color == GameController.PieceColor.White)
        {
            List<Cell> candidates = new List<Cell>()
            {
                controller.GetCell(currentCell.coordinates.x+1, currentCell.coordinates.y-1),
                controller.GetCell(currentCell.coordinates.x+1, currentCell.coordinates.y),
                controller.GetCell(currentCell.coordinates.x+1, currentCell.coordinates.y+1),
                controller.GetCell(currentCell.coordinates.x+2, currentCell.coordinates.y)
            };

            foreach(Cell c in candidates)
            {
                GameController.Movement m = ValidMove(c);
                if (m != null)
                {
                    posibleMovements.Add(m);
                }
            }
        }
        else
        {
            List<Cell> candidates = new List<Cell>()
            {
                controller.GetCell(currentCell.coordinates.x-1, currentCell.coordinates.y-1),
                controller.GetCell(currentCell.coordinates.x-1, currentCell.coordinates.y),
                controller.GetCell(currentCell.coordinates.x-1, currentCell.coordinates.y+1),
                controller.GetCell(currentCell.coordinates.x-2, currentCell.coordinates.y)
            };

            foreach (Cell c in candidates)
            {
                GameController.Movement m = ValidMove(c);
                if (m != null)
                {
                    posibleMovements.Add(m);
                }
            }
        }

        base.ComputeMovements();
    }

    public override void MoveTo(GameController.Movement move)
    {
        base.MoveTo(move);
        if(move.type == GameController.MovementType.Promotion)
        {
            promotionPanel.SetActive(true);
            Debug.Log("PROMOTION");
        }
        else if(move.type == GameController.MovementType.Passant)
        {
            move.auxPiece.Die();
        }
    }

    public void PawnPromotion(string piece)
    {
        GameController.PieceType type = GameController.PieceType.Queen;

        switch (piece)
        {
            case "Knight":
                type = GameController.PieceType.Knight;
                break;
            case "Rook":
                type = GameController.PieceType.Rook;
                break;
            case "Bishop":
                type = GameController.PieceType.Bishop;
                break;
            default:
                type = GameController.PieceType.Queen;
                break;
        }

        Piece p = controller.CreatePiece(type, this.color, this.currentCell.coordinates);
        p.ComputeMovements();
        Destroy(gameObject);

    }
}
