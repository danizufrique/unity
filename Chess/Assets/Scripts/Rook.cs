﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : Piece
{
    public override GameController.Movement ValidMove(Cell cell) {
        Vector2Int dif = cell.coordinates - currentCell.coordinates;
        if (dif.x == 0 && dif.y == 0)
            return null;

        Piece cellPiece;
        if (dif.x != 0 && dif.y != 0)
            return null;

        if (dif.x != 0)
        {
            int horFactor = (cell.coordinates.x > currentCell.coordinates.x) ? 1 : -1;
            for(int i=currentCell.coordinates.x+horFactor; i!=cell.coordinates.x; i+=horFactor)
            {   
                cellPiece = controller.GetPiece(i, currentCell.coordinates.y);
                if (cellPiece != null)
                    return null;
            }
        }
        else if(dif.y != 0)
        {
            int verFactor = (cell.coordinates.y > currentCell.coordinates.y) ? 1 : -1;
            for (int i = currentCell.coordinates.y+verFactor; i < cell.coordinates.y; i+=verFactor)
            {
                cellPiece = controller.GetPiece(currentCell.coordinates.x, i);
                if (cellPiece != null)
                    return null;
            }
        }

        cellPiece = controller.GetPiece(cell.coordinates.x, cell.coordinates.y);
        if (cellPiece != null)
        {
            if (cellPiece.color == this.color)
                return null;
            else
                return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
        }

        return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
    }

    public override void ComputeMovements()
    {
        posibleMovements.Clear();
        List<Cell> candidates = new List<Cell>();

        Vector2Int coor = currentCell.coordinates;


        for (int i = coor.x + 1; i < 8; i++)
            candidates.Add(controller.GetCell(i, coor.y));

        for (int i = coor.x - 1; i >= 0; i--)
            candidates.Add(controller.GetCell(i, coor.y));

        for (int i = coor.y + 1; i < 8; i++)
            candidates.Add(controller.GetCell(coor.x, i));

        for (int i = coor.y - 1; i >= 0; i--)
            candidates.Add(controller.GetCell(coor.x, i));

        foreach (Cell c in candidates)
        {
            GameController.Movement m = ValidMove(c);
            if (m != null)
            {
                posibleMovements.Add(m);
            }
        }

        base.ComputeMovements();
    }
}
