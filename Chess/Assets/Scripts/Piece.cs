﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    protected GameController controller;

    public GameController.PieceColor color;
    public GameController.PieceType type;
    public uint points;
    public Cell currentCell;
    public string AN_name;

    public Queue<GameController.Movement> movementRecord;
    private bool selected;
    protected List<GameController.Movement> posibleMovements = new List<GameController.Movement>();
    private MaterialPropertyBlock _propBlock;
    private Renderer _renderer;

    private void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponentInChildren<Renderer>();
        movementRecord = new Queue<GameController.Movement>();
        controller = GameController.instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        selected = false;   
    }

    public virtual GameController.Movement ValidMove(Cell cell) { return null; }

    public virtual GameController.Movement CanMoveTo(Cell cell)
    {
        foreach(GameController.Movement m in posibleMovements)
        {
            if (m.end == cell && m.type != GameController.MovementType.Illegal)
                return m;
        }
        return null;
    }

    public virtual void MoveTo(GameController.Movement move)
    {
        // Comer pieza si la hay
        if (move.type == GameController.MovementType.Eat)
            move.end.currentPiece.Die();

        // Moverse
        currentCell.currentPiece = null;
        move.end.currentPiece = this;
        currentCell = move.end;
        transform.position = move.end.transform.position;

        Disselect();
    }

    public void Select()
    {
        selected = true;
        _renderer.GetPropertyBlock(_propBlock,0);
        _propBlock.SetFloat("_Intensity", 10);
        _renderer.SetPropertyBlock(_propBlock, 0);
        _renderer.materials[0].SetFloat("_Intensity", 10);

        foreach (GameController.Movement m in posibleMovements)
        {
            if(m.type == GameController.MovementType.Illegal)
                m.end.HighLight(m.end.red);
            else
                m.end.HighLight(m.end.green);
        }

    }

    public void Disselect()
    {
        selected = false;
        _renderer.GetPropertyBlock(_propBlock, 0);
        _propBlock.SetFloat("_Intensity", 0);
        _renderer.SetPropertyBlock(_propBlock, 0);
        _renderer.materials[0].SetFloat("_Intensity", 0);

        foreach (GameController.Movement m in posibleMovements)
        {
            m.end.NoHighLight();
        }
    }

    public void Die()
    {
        currentCell.currentPiece = null;
        controller.DeletePiece(this);
        Destroy(gameObject);
    }

    public GameController.Movement LastMove()
    {
        if (movementRecord.Count > 0)
            return movementRecord.Peek();
        else
            return null;
    }

    public virtual void ComputeMovements()
    {
        foreach(GameController.Movement m in posibleMovements)
        {
            GameController.PieceColor enemyColor = (color == GameController.PieceColor.White) ? GameController.PieceColor.Black : GameController.PieceColor.White;
            if (controller.board.KingCanBeKilledAfterMovementBy(m, enemyColor))
            {
                m.type = GameController.MovementType.Illegal;
            }
        }
        
    }

    public bool HasPosibleMovements()
    {
        bool res = false;
        for(int i=0; i<posibleMovements.Count && !res; i++)
        {
            if (posibleMovements[i].type != GameController.MovementType.Illegal)
                res = true;
        }

        return res;
    }
}
