﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
using System.Security.AccessControl;

public class Board
{
    Cell[,] cells;
    public GameController controller;

    public Board()
    {
        cells = new Cell[8, 8];
    }

    public void Reset()
    {
        foreach (Cell c in cells)
            c.currentPiece = null;

    }
    public void SetCell(int x, int y, Cell c)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
            cells[x, y] = c;
        else
            Debug.LogError("SSS");
    }

    public Cell GetCell(int x, int y)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
            return cells[x, y];

        return null;
    }

    public Piece GetPiece(int x, int y)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
            return (cells[x, y].currentPiece);
        else
            return null;
    }

    public void SetPiece(int x, int y, Piece p)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
            cells[x, y].currentPiece = p;
        else
            Debug.LogError("SSS");
    }

    public Vector2Int FindKingCoordinates(GameController.PieceColor color)
    {
        foreach(Cell c in cells)
        {
            if(c.currentPiece != null)
            {
                if(c.currentPiece.type == GameController.PieceType.King)
                {
                    if(c.currentPiece.color == color)
                    {
                        return c.coordinates;
                    }
                }
            }
        }

        return new Vector2Int(-1, -1);
    }
    public bool CellTypeIs(int x, int y, GameController.PieceType type)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            Piece p = GetPiece(x, y);
            if (p != null)  return p.type == type;
            else            return false;
        }
        else
            return false;
    }

    public bool CellAttackedBy(Vector2Int coor, GameController.PieceColor color)
    {
        Cell c = GetCell(coor.x, coor.y);
        if (c == null)
            return false;

        List<Piece> enemies = (color == GameController.PieceColor.White) ? controller.whitePieces : controller.blackPieces;

        foreach (Piece p in enemies)
        {
            if (p.ValidMove(c) != null)
            {
                return true;
            }       
        }

        return false;
    }

    public bool CellAttackedAfterMovementBy(GameController.Movement mov, Vector2Int coor, GameController.PieceColor color)
    {
        bool result = false;
        Piece previousPiece = null;
        
        Vector2Int startCoor = mov.start.coordinates;
        Vector2Int endCoor = mov.end.coordinates;

        // Hacemos movimiento

        previousPiece = GetCell(endCoor.x, endCoor.y).currentPiece;
        GetCell(startCoor.x, startCoor.y).currentPiece = null;
        GetCell(endCoor.x, endCoor.y).currentPiece = mov.piece;

        if (mov.type == GameController.MovementType.Castling)
        {
            if (endCoor.y == 2)  // ENROQUE LARGO
            {
                GetCell(startCoor.x, 3).currentPiece = GetCell(startCoor.x, 0).currentPiece;
                GetCell(startCoor.x, 0).currentPiece = null;
            }
            else if (endCoor.y == 6)  // ENROQUE CORTO
            {
                GetCell(startCoor.x, 5).currentPiece = GetCell(startCoor.x, 7).currentPiece;
                GetCell(startCoor.x, 7).currentPiece = null;
            }
        }

        result = CellAttackedBy(coor, color);

        // Deshacemos movimiento
        GetCell(startCoor.x, startCoor.y).currentPiece = mov.piece;
        GetCell(endCoor.x, endCoor.y).currentPiece = previousPiece;

        if (mov.type == GameController.MovementType.Castling)
        {
            if (endCoor.y == 2)  // ENROQUE LARGO
            {
                GetCell(startCoor.x, 0).currentPiece = GetCell(startCoor.x, 3).currentPiece;
                GetCell(startCoor.x, 3).currentPiece = null;
            }
            else if (endCoor.y == 6)  // ENROQUE CORTO
            {
                GetCell(startCoor.x, 7).currentPiece = GetCell(startCoor.x, 5).currentPiece;
                GetCell(startCoor.x, 5).currentPiece = null;
            }
        }
        
        return result;
    }

    public bool KingCanBeKilledAfterMovementBy(GameController.Movement mov, GameController.PieceColor color)
    {
        Vector2Int kingCoor = (mov.piece.type == GameController.PieceType.King) ? mov.end.coordinates : FindKingCoordinates(mov.piece.color);
        bool res = CellAttackedAfterMovementBy(mov, kingCoor, color);

        return res;
    }
}
