﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public class Movement
    {
        public Movement(GameController.MovementType t, Piece p, Cell s, Cell e, Piece otherPiece = null)
        {
            piece = p;
            start = s;
            end = e;
            type = t;
            auxPiece = otherPiece;
        }


        public static bool Equals(Movement m1, Movement m2)
        {
            if (m2 == null)
                return m1==null;

            bool result = m1.piece.name == m2.piece.name && m1.start.coordinates==m2.start.coordinates && m1.end.coordinates == m2.end.coordinates;
            return result;
        }

        public string Print()
        {
            string res = piece.AN_name;

            switch (type)
            {
                case MovementType.Regular:
                    res += end.textCoordinates;
                    break;
                case MovementType.Eat:
                    res += "x" + end.textCoordinates;
                    break;
                case MovementType.Castling:
                    if(end.coordinates.y == 6)  // Corto
                    {
                        res = "0-0";
                    }
                    else    // Largo
                    {
                        res = "0-0-0";
                    }
                    break;
                case MovementType.DoubleStart:
                    res += end.textCoordinates;
                    break;
                case MovementType.Promotion:
                    res += auxPiece.AN_name + end.textCoordinates;
                    break;
                case MovementType.Check:
                    res += end.textCoordinates + "+";
                    break;
                case MovementType.Checkmate:
                    res += end.textCoordinates + "#";
                    break;
                case MovementType.Passant:
                    res += start.textCoordinates[0] + "x" + end.textCoordinates + "  e.p.";
                    break;
                default:
                    break;
            }
            return res;
        }

        public Piece piece;
        public Cell start;
        public Cell end;
        public GameController.MovementType type;
        public Piece auxPiece;
    }

    public static GameController instance = null;

    public enum PieceColor { White, Black};
    public enum PieceType { Pawn, Rook, Knight, Bishop, Queen, King};
    public enum MovementType { Illegal, Regular, DoubleStart, Eat, Castling, Passant, Promotion, Check, Checkmate};

    public Board board;
    private List<Piece> pieces;
    public List<Piece> whitePieces;
    public List<Piece> blackPieces;
    private Piece selectedPiece;
    public King whiteKing, blackKing;

    [Header("GAME STATS")]
    int nTurn;
    PieceColor playerTurn;
    Stack<Movement> whiteMovementRecord, blackMovementRecord;

    [Header("PREFABS")]
    public GameObject cellPrefab;
    public GameObject pawnPrefab;
    public GameObject rookPrefab;
    public GameObject knightPrefab;
    public GameObject bishopPrefab;
    public GameObject queenPrefab;
    public GameObject kingPrefab;

    [Header("MATERIALS")]
    public Material whiteMat;
    public Material blackMat;

    [Header("UI")]
    public TextMeshProUGUI movementsUI_White;
    public TextMeshProUGUI movementsUI_Black;
    public GameObject infoPanel;
    //public CameraController cameraController;

    private GameObject pieceParent;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        infoPanel.SetActive(false);
        playerTurn = PieceColor.Black;
        board = new Board();
        board.controller = this;
        selectedPiece = null;
        whiteMovementRecord = new Stack<Movement>();
        blackMovementRecord = new Stack<Movement>();
        pieces = new List<Piece>();

        GenerateBoard();
        GeneratePieces();
        NextTurn(null);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {

                if (hit.transform.gameObject.layer == 9)    // PIECE
                {
                    Piece hitPiece = hit.transform.parent.gameObject.GetComponent<Piece>();
                    if (hitPiece.color == playerTurn)
                    {
                        if (selectedPiece != null)
                        {
                            selectedPiece.Disselect();
                            if (selectedPiece == hitPiece)
                            {
                                selectedPiece = null;
                            }
                            else
                            {
                                selectedPiece = hitPiece;
                                hitPiece.Select();
                            }
                        }
                        else
                        {
                            selectedPiece = hitPiece;
                            hitPiece.Select();
                        }
                    }
                }
                else if (hit.transform.gameObject.layer == 10)  // CELL
                {
                    if (selectedPiece != null)
                    {
                        Cell hitCell = hit.transform.gameObject.GetComponentInParent<Cell>();
                        Movement m = selectedPiece.CanMoveTo(hitCell);

                        if (m != null)
                        {
                            selectedPiece.Disselect();

                            if (selectedPiece.color == PieceColor.White)
                                whiteMovementRecord.Push(m);
                            else
                                blackMovementRecord.Push(m);

                            selectedPiece.movementRecord.Enqueue(m);

                            selectedPiece.MoveTo(m);
                            selectedPiece = null;

                            NextTurn(m);
                        }
                    }
                }
            }
        }

    }

    void NextTurn(Movement lastMove)
    {
        foreach (Piece p in pieces)
        {
            p.ComputeMovements();
        }

        playerTurn = (playerTurn == PieceColor.White) ? PieceColor.Black : PieceColor.White;
        King king = (playerTurn == PieceColor.White) ? whiteKing : blackKing;
        List<Piece> turnPieces = (playerTurn == PieceColor.White) ? whitePieces : blackPieces;
        PieceColor otherPlayer = (playerTurn == PieceColor.White) ? PieceColor.Black : PieceColor.White;

        if (board.CellAttackedBy(king.currentCell.coordinates, otherPlayer))
        {
            bool isCheckMate = true;
            for(int i=0; i<turnPieces.Count && isCheckMate; i++)
            {
                if (turnPieces[i].HasPosibleMovements()) {
                    isCheckMate = false;
                }
            }

            if (isCheckMate && lastMove!=null)
            {
                lastMove.type = MovementType.Checkmate;
                EndGame(otherPlayer);
            }
            else
            {
                lastMove.type = MovementType.Check;
                nTurn++;
            }
        }
        else
        {
            //Debug.Log("LA CASILLA " + king.currentCell.coordinates + " NO ES ATACADA POR " + playerTurn);
        }

        if (lastMove != null)
        {
            TextMeshProUGUI uiMoves = (lastMove.piece.color == PieceColor.White) ? movementsUI_White : movementsUI_Black;
            uiMoves.text = lastMove.Print() + "\n" + uiMoves.text;
        }
    }

    public void RestartGame()
    {
        nTurn = 0;
        pieces.Clear();
        foreach (Piece p in whitePieces)
            Destroy(p.gameObject);
        whitePieces.Clear();

        foreach (Piece p in blackPieces)
            Destroy(p.gameObject);

        blackPieces.Clear();
        whiteMovementRecord.Clear();
        blackMovementRecord.Clear();
        selectedPiece = null;
        playerTurn = PieceColor.Black;
        board.Reset();
        GeneratePieces();
        NextTurn(null);
    }

    public Movement LastMove(PieceColor color)
    {
        if (color == PieceColor.White && whiteMovementRecord.Count > 0)
            return whiteMovementRecord.Peek();
        else if (color == PieceColor.Black && blackMovementRecord.Count > 0)
            return blackMovementRecord.Peek();

        return null;
    }

    void EndGame(PieceColor winner)
    {
        infoPanel.SetActive(true);
        infoPanel.GetComponentInChildren<TextMeshProUGUI>().text = "GANADOR: " + winner;
    }

    // Initializes board and its cells
    void GenerateBoard()
    {
        GameObject boardGO = new GameObject("BOARD");
        
        PieceColor currentColor = PieceColor.White;
        for(int i=0; i<8; i++)
        {
            currentColor = (currentColor == PieceColor.Black) ? PieceColor.White : PieceColor.Black;
            for (int j=0; j<8; j++)
            {
                Vector2Int coordinates = new Vector2Int(i, j);
                Cell cell = Instantiate(cellPrefab, new Vector3(-i,0,j), Quaternion.identity).GetComponent<Cell>();

                currentColor = (currentColor == PieceColor.Black) ? PieceColor.White: PieceColor.Black;

                cell.GetComponentInChildren<MeshRenderer>().material = (currentColor==PieceColor.White) ? whiteMat : blackMat;
                cell.color = currentColor;
                
                cell.coordinates = new Vector2Int(i, j);
                cell.currentPiece = null;

                string textCoordinates = "" + (char)((int)'a' + j) + (char)((int)'1' + i);
                cell.gameObject.name = textCoordinates;
                cell.textCoordinates = textCoordinates;

                cell.gameObject.transform.parent = boardGO.transform;

                /*
                if(coordinates == new Vector2Int(3,3))
                    cameraController.target = cell.transform;
                    */
                board.SetCell(i, j, cell);
            }
        }
    }

    void GeneratePieces()
    {
        pieceParent = new GameObject("PIECES");

        for (int j=0; j<8; j++)
        {
            CreatePiece(PieceType.Pawn, PieceColor.White, new Vector2Int(1, j));
            CreatePiece(PieceType.Pawn, PieceColor.Black, new Vector2Int(6, j));
        }

        CreatePiece(PieceType.Rook, PieceColor.Black, new Vector2Int(7, 0));
        CreatePiece(PieceType.Rook, PieceColor.Black, new Vector2Int(7, 7));
        CreatePiece(PieceType.Rook, PieceColor.White, new Vector2Int(0, 0));
        CreatePiece(PieceType.Rook, PieceColor.White, new Vector2Int(0, 7));

        CreatePiece(PieceType.Knight, PieceColor.Black, new Vector2Int(7, 1));
        CreatePiece(PieceType.Knight, PieceColor.Black, new Vector2Int(7, 6));
        CreatePiece(PieceType.Knight, PieceColor.White, new Vector2Int(0, 1));
        CreatePiece(PieceType.Knight, PieceColor.White, new Vector2Int(0, 6));

        CreatePiece(PieceType.Bishop, PieceColor.Black, new Vector2Int(7, 2));
        CreatePiece(PieceType.Bishop, PieceColor.Black, new Vector2Int(7, 5));
        CreatePiece(PieceType.Bishop, PieceColor.White, new Vector2Int(0, 2));
        CreatePiece(PieceType.Bishop, PieceColor.White, new Vector2Int(0, 5));

        CreatePiece(PieceType.Queen, PieceColor.Black, new Vector2Int(7, 3));
        CreatePiece(PieceType.Queen, PieceColor.White, new Vector2Int(0, 3));

        blackKing = CreatePiece(PieceType.King, PieceColor.Black, new Vector2Int(7, 4)).GetComponent<King>();
        whiteKing = CreatePiece(PieceType.King, PieceColor.White, new Vector2Int(0, 4)).GetComponent<King>();

    }

    public void DeletePiece(Piece p)
    {
        pieces.Remove(p);
        if(p.color == PieceColor.White)
        {
            whitePieces.Remove(p);
        }
        else
        {
            blackPieces.Remove(p);
        }

        
    }
    public Piece CreatePiece(PieceType type, PieceColor color, Vector2Int coordinates)
    {
        GameObject prefab;

        switch (type)
        {
            case PieceType.Pawn:
                prefab = pawnPrefab;
                break;
            case PieceType.Rook:
                prefab = rookPrefab;
                break;
            case PieceType.Knight:
                prefab = knightPrefab;
                break;
            case PieceType.Bishop:
                prefab = bishopPrefab;
                break;
            case PieceType.Queen:
                prefab = queenPrefab;
                break;
            default:
                prefab = kingPrefab;
                break;
        }

        Piece piece = Instantiate(prefab, new Vector3(-coordinates.x, 0, coordinates.y), Quaternion.identity).GetComponent<Piece>();
        piece.type = type;
        piece.currentCell = board.GetCell(coordinates.x, coordinates.y);
        piece.currentCell.currentPiece = piece;
        piece.color = color;
        piece.GetComponentInChildren<MeshRenderer>().material = (color == PieceColor.White) ? whiteMat : blackMat;

        if ((type == PieceType.Knight || type == PieceType.Bishop) && color == PieceColor.White)
            piece.gameObject.transform.rotation = Quaternion.Euler(0,180,0);

        piece.gameObject.name = type.ToString() + "[" + coordinates.x + ", " + coordinates.y + "]";
        piece.transform.parent = pieceParent.transform;

        pieces.Add(piece);
        if(color == PieceColor.White)
            whitePieces.Add(piece);
        else
            blackPieces.Add(piece);

        return piece;
    }
    

    public Cell GetCell(int x, int y)
    {
        return board.GetCell(x,y);
    }

    public Piece GetPiece(int x, int y)
    {
        return board.GetPiece(x, y);
    }

    
}
