﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Piece
{
    public override GameController.Movement ValidMove(Cell cell)
    {
        if (cell == null)
            return null;

        if (cell == currentCell)
            return null;

        if (cell.currentPiece != null)
        {
            if (cell.currentPiece.color == this.color)
                return null;
        }

        if (Math.Abs(cell.coordinates.x - currentCell.coordinates.x) == 2)
        {
            if (Math.Abs(cell.coordinates.y - currentCell.coordinates.y) == 1)
            {
                if (cell.currentPiece == null)
                    return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
                else
                    return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
            }
            else
                return null;
        }
        else if (Math.Abs(cell.coordinates.x - currentCell.coordinates.x) == 1)
        {
            if (Math.Abs(cell.coordinates.y - currentCell.coordinates.y) == 2)
            {
                if (cell.currentPiece == null)
                    return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
                else
                    return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
            }
            else
                return null;
        }

        return null;
    }

    public override void ComputeMovements()
    {
        posibleMovements.Clear();


        List<Cell> candidates = new List<Cell>()
        {
            controller.GetCell(currentCell.coordinates.x+1, currentCell.coordinates.y-2),
            controller.GetCell(currentCell.coordinates.x+1, currentCell.coordinates.y+2),
            controller.GetCell(currentCell.coordinates.x+2, currentCell.coordinates.y-1),
            controller.GetCell(currentCell.coordinates.x+2, currentCell.coordinates.y+1),
            controller.GetCell(currentCell.coordinates.x-1, currentCell.coordinates.y-2),
            controller.GetCell(currentCell.coordinates.x-1, currentCell.coordinates.y+2),
            controller.GetCell(currentCell.coordinates.x-2, currentCell.coordinates.y-1),
            controller.GetCell(currentCell.coordinates.x-2, currentCell.coordinates.y+1),

        };

        foreach (Cell c in candidates)
        {
            if (c != null)
            {
                GameController.Movement m = ValidMove(c);
                if (m!= null)
                {
                    posibleMovements.Add(m);
                }
            }
        }

        base.ComputeMovements();
    }
}
