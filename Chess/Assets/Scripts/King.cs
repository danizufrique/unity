﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class King : Piece
{
    
    public override GameController.Movement ValidMove(Cell cell)
    {
        if (cell == null)
            return null;

        if (cell == currentCell)
            return null;

        if (cell.currentPiece != null)
        {
            if (cell.currentPiece.color == this.color)
                return null;
        }

        Vector2Int dif = cell.coordinates - currentCell.coordinates;
        if (Math.Abs(dif.x)>2 || Math.Abs(dif.y) > 2)
            return null;

        Vector2Int coor = currentCell.coordinates;
        if (Math.Abs(cell.coordinates.y - coor.y) == 1 || Math.Abs(cell.coordinates.x - coor.x) == 1)
        {
            if(cell.currentPiece != null)
                return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
            else
                return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
        }
            
        else if (movementRecord.Count == 0) {
            Debug.Log("DEBUG0");
            if (cell.coordinates.y - coor.y == 2)    // ENROQUE CORTO
            {
                Piece rook = controller.GetPiece(coor.x, coor.y + 3);

                if (rook == null)
                    return null;
                else if (rook.GetComponent<Rook>() == null)
                    return null;

                Debug.Log("DEBUG1");
                if (rook.movementRecord.Count != 0)
                    return null;
                Debug.Log("DEBUG2");
                Cell middleCell1 = controller.GetCell(coor.x, coor.y + 1);
                Cell middleCell2 = controller.GetCell(cell.coordinates.x, cell.coordinates.y);
                if (middleCell1.currentPiece == null && middleCell2.currentPiece == null)
                {
                    Debug.Log("DEBUG3");
                    //rook.MoveTo(middleCell1);
                    return new GameController.Movement(GameController.MovementType.Castling, this, currentCell, cell);
                }

                return null;
            }
            else if (cell.coordinates.y - coor.y == -2)  // ENROQUE LARGO
            {
                Piece rook = controller.GetPiece(coor.x, coor.y - 4);

                if (rook == null)
                    return null;
                else if (rook.GetComponent<Rook>() == null)
                    return null;

                if (rook.movementRecord.Count != 0)
                    return null;

                Cell middleCell1 = controller.GetCell(coor.x, coor.y - 1);
                Cell middleCell2 = controller.GetCell(cell.coordinates.x, cell.coordinates.y);
                if (middleCell1.currentPiece == null && middleCell2.currentPiece == null)
                {
                    //rook.MoveTo(middleCell1);
                    return new GameController.Movement(GameController.MovementType.Castling, this, currentCell, cell);
                }

                return null;
            }
            
        }
        

        return null;
    }

    public override void ComputeMovements()
    {
        posibleMovements.Clear();
        Vector2Int coor = currentCell.coordinates;

        List<Cell> candidates = new List<Cell>()
        {
            controller.GetCell(coor.x+1, coor.y-1),
            controller.GetCell(coor.x+1, coor.y),
            controller.GetCell(coor.x+1, coor.y+1),
            controller.GetCell(coor.x-1, coor.y-1),
            controller.GetCell(coor.x-1, coor.y),
            controller.GetCell(coor.x-1, coor.y+1),
            controller.GetCell(coor.x, coor.y-1),
            controller.GetCell(coor.x, coor.y+1),
            controller.GetCell(coor.x, coor.y-2),
            controller.GetCell(coor.x, coor.y+2),

        };

        foreach (Cell c in candidates)
        {
            GameController.Movement m = ValidMove(c);
            if (m != null)
            {
                posibleMovements.Add(m);
            }
        }
        
        base.ComputeMovements();
    }

    /*
    bool CheckColumn(Board board, Vector2Int kingCoor)
    {
        int column = kingCoor.y;

        List<GameController.PieceType> types = new List<GameController.PieceType>() { 
            GameController.PieceType.Rook, 
            GameController.PieceType.Queen 
        };

        for(int i=kingCoor.x; i<7; i++)
        {
            Piece p = board.GetPiece(i+1, column);
            if (IsEnemy(p, types)) {
                if (types.Contains(p))
                    return true;
            } return true;
            p = board.GetPiece(i + -1, column);
            if (IsEnemy(p, types)) return true;
        }

        return false;
    }
    
    
    bool CheckRow(Board board, Vector2Int kingCoor)
    {
        int row = kingCoor.x;

        List<GameController.PieceType> types = new List<GameController.PieceType>() { 
            GameController.PieceType.Rook, 
            GameController.PieceType.Queen 
        };

        for(int i=kingCoor.y; i<7; i++)
        {
            Piece p = board.GetPiece(row, i+1);
            if (IsEnemy(p, types)) return true;
            p = board.GetPiece(row, i-1);
            if (IsEnemy(p, types)) return true;
        }

        return false;
    }
    */
    
    bool IsEnemy(Piece p, List<GameController.PieceType> t)
    {
        
        if (p == null)
            return false;

        if (p.color != this.color)
            return true;

        return false;
    }

    public override void MoveTo(GameController.Movement mov)
    {
        base.MoveTo(mov);
        if (mov.type == GameController.MovementType.Castling)
        {
            Vector2Int endCoor = mov.end.coordinates;
            Vector2Int startCoor = mov.start.coordinates;
            if (endCoor.y == 2)  // ENROQUE LARGO
            {
                Piece rook = controller.GetPiece(startCoor.x, 0);
                Cell rookNewCell = controller.GetCell(startCoor.x, 3);
                rookNewCell.currentPiece = rook;
                rook.currentCell = rookNewCell;
                rook.transform.position = controller.GetCell(startCoor.x, 3).transform.position;
                controller.GetCell(startCoor.x, 0).currentPiece = null;
            }
            else if (endCoor.y == 6)  // ENROQUE CORTO
            {
                Piece rook = controller.GetPiece(startCoor.x, 7);
                Cell rookNewCell = controller.GetCell(startCoor.x, 5);
                rookNewCell.currentPiece = rook;
                rook.currentCell = rookNewCell;
                rook.transform.position = controller.GetCell(startCoor.x, 5).transform.position;
                controller.GetCell(startCoor.x, 7).currentPiece = null;
            }
        }


    }
    
}
