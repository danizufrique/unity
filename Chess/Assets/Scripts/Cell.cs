﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public GameController.PieceColor color;
    public Vector2Int coordinates;
    public string textCoordinates;
    public Piece currentPiece;

    private MaterialPropertyBlock _propBlock;
    private Renderer _renderer;

    [ColorUsage(true, true)]
    public Color green, red;

    private void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponentInChildren<Renderer>();
    }

    public void HighLight(Color color)
    {
        _renderer.GetPropertyBlock(_propBlock, 0);
        _propBlock.SetFloat("_Intensity", 10);
        _propBlock.SetVector("_RangeIntensity", new Vector4(1.5f, 1.8f, 1, 1));
        _propBlock.SetColor("_GlowColor", color);
        _renderer.SetPropertyBlock(_propBlock, 0);
        _renderer.materials[0].SetFloat("_Intensity", 10);
        _renderer.materials[0].SetVector("_RangeIntensity", new Vector4(1.5f, 1.8f, 1, 1));
        _renderer.materials[0].SetColor("_GlowColor", color);
    }

    public void NoHighLight()
    {
        _renderer.GetPropertyBlock(_propBlock, 0);
        _propBlock.SetFloat("_Intensity", 0);
        _renderer.SetPropertyBlock(_propBlock, 0);
        _renderer.materials[0].SetFloat("_Intensity", 0);
    }

    /*
    public static Cell DeepCopy(Cell other)
    {
        Cell temp = new Cell();
        temp.color = other.color;
        temp.coordinates;
        temp.textCoordinates;
        temp.currentPiece;

        private MaterialPropertyBlock _propBlock;
        private Renderer _renderer;

        [ColorUsage(true, true)]
        public Color green, red;
    }
    */
}
