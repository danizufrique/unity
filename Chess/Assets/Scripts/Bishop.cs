﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bishop : Piece
{
    public override GameController.Movement ValidMove(Cell cell)
    {
        if (cell == null)
            return null;

        Vector2Int dif = cell.coordinates - currentCell.coordinates;
        if (Math.Abs(dif.x) != Math.Abs(dif.y))
            return null;

        Piece cellPiece;
        int horFactor = (cell.coordinates.x > currentCell.coordinates.x) ? 1 : -1;
        int verFactor = (cell.coordinates.y > currentCell.coordinates.y) ? 1 : -1;
        int contador = 0;

        for (int i = currentCell.coordinates.x + horFactor; i != cell.coordinates.x; i += horFactor)
        {
            contador+=verFactor;
            cellPiece = controller.GetPiece(i, currentCell.coordinates.y+contador);
            if (cellPiece != null)
                return null;
        }

        cellPiece = controller.GetPiece(cell.coordinates.x, cell.coordinates.y);
        if (cellPiece != null)
        {
            if (cellPiece.color == this.color)
                return null;
            else
                return new GameController.Movement(GameController.MovementType.Eat, this, currentCell, cell);
        }

        return new GameController.Movement(GameController.MovementType.Regular, this, currentCell, cell);
    }

    public override void ComputeMovements()
    {
        posibleMovements.Clear();
        List<Cell> candidates = new List<Cell>();

        Vector2Int coor = currentCell.coordinates;
        int contador = 0;

        for (int i = coor.x + 1; i < 8; i++)
        {
            contador++;
            candidates.Add(controller.GetCell(i, coor.y + contador));
        }

        contador = 0;
        for (int i = coor.x - 1; i > 0; i--)
        {
            contador++;
            candidates.Add(controller.GetCell(i, coor.y + contador));
        }

        contador = 0;
        for (int i = coor.x + 1; i < 8; i++)
        {
            contador++;
            candidates.Add(controller.GetCell(i, coor.y - contador));
        }

        contador = 0;
        for (int i = coor.x - 1; i > 0; i--)
        {
            contador++;
            candidates.Add(controller.GetCell(i, coor.y - contador));
        }

        foreach (Cell c in candidates)
        {
            GameController.Movement m = ValidMove(c);
            if (m != null)
            {
                posibleMovements.Add(m);
            }
        }
        base.ComputeMovements();
    }
}
