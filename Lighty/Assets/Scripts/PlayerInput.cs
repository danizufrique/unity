using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput
{
    public KeyCode jump;
    public KeyCode left, right;
    public KeyCode up, down;

}
