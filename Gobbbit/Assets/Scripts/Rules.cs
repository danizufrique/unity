﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rules : MonoBehaviour
{
    public enum Animals
    {
        fly, camaleon, snake, gorilla
    }

    public enum Colors
    {
        red, yellow, blue
    }

    public static Rules instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogError("MÁS DE UN RULES OBJECT");
        }
    }
}
