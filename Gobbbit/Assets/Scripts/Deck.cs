﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    List<CardInfo> cards;
    [Tooltip("Number of EACH animal in ONE color")]
    public uint nCards = 3;
    public uint nColors = 3;

    void Start()
    {
        cards = new List<CardInfo>();
        
        foreach(Rules.Animals animal in System.Enum.GetValues(typeof(Rules.Animals)))
        {
            foreach (Rules.Colors color in System.Enum.GetValues(typeof(Rules.Colors)))
            {
                for (int i = 0; i < nCards; i++)
                {
                    CardInfo newCard = new CardInfo(animal, color);
                    cards.Add(newCard);
                }
            }
            
        }
        
    }

    void Shuffle()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
