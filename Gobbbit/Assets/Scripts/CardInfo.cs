﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class CardInfo
{
    public Rules.Animals animal;
    public Rules.Colors color;

    public CardInfo(Rules.Animals animal, Rules.Colors color)
    {
        this.animal = animal;
        this.color = color;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
