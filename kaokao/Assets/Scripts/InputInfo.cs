﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputInfo
{

    public Vector2 axis;
    public bool jumpButton;

    public InputInfo()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        axis = new Vector2(x,y);
        jumpButton = Input.GetKey(KeyCode.Space);
    }

    public InputInfo(InputInfo info)
    {
        this.axis = new Vector2(info.axis.x, info.axis.y);
        this.jumpButton = info.jumpButton;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
