﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Animator anim;
    [SerializeField] Interruptor[] interruptors;
    int activeInterruptors;
    bool isOpen = false;

    private void Start()
    {
        anim = GetComponent<Animator>();

        foreach (Interruptor interruptor in interruptors)
            interruptor.door = this;

        activeInterruptors = 0;
    }

    public void InterruptorOn()
    {
        activeInterruptors++;
        if (activeInterruptors == interruptors.Length)
            Open();
    }

    public void InterruptorOff()
    {
        activeInterruptors--;
        if (isOpen && activeInterruptors != interruptors.Length)
            Close();
    }

    public void Open()
    {
        isOpen = true;
        anim.SetBool("isOpen", isOpen);
    }

    public void Close()
    {
        isOpen = false;
        anim.SetBool("isOpen", isOpen);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        foreach (Interruptor interruptor in interruptors)
            Gizmos.DrawLine(transform.position, interruptor.transform.position);
    }
}
