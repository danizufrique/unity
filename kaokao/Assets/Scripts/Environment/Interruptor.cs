﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interruptor : MonoBehaviour
{
    public Door door;
    List<GameObject> objectsColliding;
    [SerializeField] bool destructOnTouch;
    public bool active = false;

    private void Start()
    {
        objectsColliding = new List<GameObject>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.layer == 9)    // Player
        {
            objectsColliding.Add(collision.gameObject);
            door.InterruptorOn();

            active = true;
            if (destructOnTouch)
                Destroy(gameObject);
        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        objectsColliding.Remove(collision.gameObject);

        if (objectsColliding.Count < 1)
        {
            active = false;
            door.InterruptorOff();
        }

    }
}
