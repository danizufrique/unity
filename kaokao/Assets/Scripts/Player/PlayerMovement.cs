﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    PlayerCollision col;
    SpriteRenderer renderer;

    [Header("Input")]
    Vector2 axis;
    Vector2 axisRaw;
    BetterJumping jumpingScript;
    InputInfo currentInput;

    [Header("Stats")]
    [SerializeField] float speed = 10;
    [SerializeField] float jumpForce = 50;
    [SerializeField] float wallJumpLerp = 10;
    [SerializeField] bool isGhost = false;

    [Header("Booleans")]
    public bool canMove = true;
    public bool grounded = false;
    private bool wallJumped = false;

    [Header("EFFECTS")]
    Light2D emmisionLight;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<PlayerCollision>();
        renderer = GetComponentInChildren<SpriteRenderer>();
        jumpingScript = GetComponent<BetterJumping>();
        emmisionLight = GetComponent<Light2D>();
        emmisionLight.color = renderer.color;
    }

    void Update()
    {

        if (col.grounded)
        {
            wallJumped = false;
        }

    }

    private void FixedUpdate()
    {
        if (!isGhost)
        {
            currentInput = new InputInfo();
            Move(currentInput);
        }
    }

    public InputInfo getCurrentInput()
    {
        return new InputInfo(currentInput);
    }

    public void Move(InputInfo inputInfo)
    {
        #region Walk
        Vector2 walkDir = inputInfo.axis;
        Walk(walkDir);
        #endregion

        #region Jump
        if (inputInfo.jumpButton)
        {
            if (col.grounded)
                Jump(Vector2.up);
            else if (col.onWall && !grounded)
                WallJump();
        }

        jumpingScript.ImproveJump(inputInfo);
        #endregion
    }

    private void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        Vector2 velocity = Vector2.zero;

        if (!wallJumped)
            velocity = new Vector2(dir.x * speed, rb.velocity.y);
        else
        {
            Vector2 direction = new Vector2(dir.x * speed, rb.velocity.y);
            velocity = Vector2.Lerp(rb.velocity, direction, wallJumpLerp * Time.deltaTime);
        }

        rb.velocity = velocity;
    }

    private void Jump(Vector2 dir)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
    }

    private void WallJump()
    {
        wallJumped = true;
        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        rb.velocity = Vector2.zero;

        Vector2 wallDir = col.onRightWall ? Vector2.left : Vector2.right;
        Vector2 dir = (Vector2.up / 1.5f) + (wallDir / 1.5f);
        Jump(dir);
    }

    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
}
