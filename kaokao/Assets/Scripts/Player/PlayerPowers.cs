﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPowers : MonoBehaviour
{
    bool isCopy;
    Queue<InputInfo> ghostRepeatInfo;
    [SerializeField] float ghostRecordTime = 5;

    [Header("STATES")]
    [SerializeField] bool recording;

    [Header("PREFABS")]
    [SerializeField] GameObject ghostPrefab;
    [SerializeField] GameObject ghostEffect;

    [Header("COLORS")]
    [SerializeField] Color repeatColor;
    [SerializeField] Color mirrorColor;

    void Start()
    {
        // GHOST
        ghostRepeatInfo = new Queue<InputInfo>();
        ghostEffect.SetActive(false);
        recording = false;
    }

    void Update()
    {
        #region Record and Create GHOST
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!recording)
            {
                ghostRepeatInfo.Clear();
                ghostEffect.SetActive(true);
            }
            else
                ghostEffect.SetActive(false);

            recording = !recording;
        }

        if (Input.GetKeyDown(KeyCode.E)) { 
            GameObject ghost = Instantiate(ghostPrefab, getMousePos(), Quaternion.identity);
            ghost.GetComponent<GhostController>().setColor(repeatColor);
            ghost.GetComponent<GhostController>().setGhostRepeatInfo(ghostRepeatInfo);
            ghost.GetComponent<GhostController>().setState(Controller.GhostState.repeat);
        }
        #endregion

        #region Create MIRROR
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameObject ghost = Instantiate(ghostPrefab, getMousePos(), Quaternion.identity);
            ghost.GetComponent<GhostController>().setColor(mirrorColor);
            ghost.GetComponent<GhostController>().setState(Controller.GhostState.mirror);
            ghost.GetComponent<GhostController>().player = this.GetComponent<PlayerMovement>();
        }
        #endregion

    }

    Vector3 getMousePos()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;

        return mousePos;
    }
    private void FixedUpdate()
    {
        // GHOST
        if (recording)
            RecordGhost();
    }

    private void RecordGhost()
    {
        if (ghostRepeatInfo.Count < Mathf.Round(ghostRecordTime / Time.fixedDeltaTime))
        {
            InputInfo inputInfo = new InputInfo();
            ghostRepeatInfo.Enqueue(inputInfo);
        }
        else
        {
            recording = false;
            ghostEffect.SetActive(false);
        }

    }
}
