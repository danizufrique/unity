﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    
    [Header("REPEAT")]
    public Queue<InputInfo> ghostRepeatInfo, ghostRepeatInfoCopy;
    [SerializeField] bool replay = false;
    [SerializeField] uint uses = 3;

    [Header("MIRROR")]
    public PlayerMovement player;

    [SerializeField] Controller.GhostState state;
    PlayerMovement movement;
    [SerializeField] TextMeshProUGUI text;

    void Start()
    {
        movement = GetComponent<PlayerMovement>();

        updateUI();
    }

    void FixedUpdate()
    {
        #region REPEAT
        if (state == Controller.GhostState.repeat)
        {
            if (ghostRepeatInfoCopy != null)
            {
                if (replay && ghostRepeatInfoCopy.Count > 0)
                {
                    movement.Move(ghostRepeatInfoCopy.Dequeue());
                }
            }
        }
        #endregion

        #region MIRROR
        if (state == Controller.GhostState.mirror)
        {
            movement.Move(player.getCurrentInput());
        }
        #endregion
    }

    private void OnMouseOver()
    {
        #region REPEAT
        if (state == Controller.GhostState.repeat)
        {
            if (Input.GetMouseButtonDown(0) && uses > 0)
            {
                StartReplay();
                uses--;
                updateUI();
            }
            else if (Input.GetMouseButtonDown(1))
                Destroy(gameObject);
        }
        #endregion

        #region MIRROR
        if (Input.GetMouseButtonDown(1))
            Destroy(gameObject);
        #endregion
    }

    public void setGhostRepeatInfo(Queue<InputInfo> info)
    {
        ghostRepeatInfo = copyGhostRepeatInfo(info);
    }

    public void setState(Controller.GhostState newState)
    {
        state = newState;
    }

    void updateUI()
    {
        if (state == Controller.GhostState.repeat)
        {
            text.text = "x" + uses.ToString();
        }
        else if (state == Controller.GhostState.mirror)
        {
            text.text = "";
        }
    }

    public void setColor(Color newColor)
    {
        GetComponentInChildren<SpriteRenderer>().color = newColor;
        GetComponent    <TrailRenderer>().startColor = newColor;
    }

    #region GHOST METHODS
    Queue<InputInfo> copyGhostRepeatInfo(Queue<InputInfo> info)
    {
        Queue<InputInfo> result = new Queue<InputInfo>();

        foreach (InputInfo i in info)
            result.Enqueue(i);

        return result;
    }

    public void StartReplay()
    {
        ghostRepeatInfoCopy = copyGhostRepeatInfo(ghostRepeatInfo);
        replay = true;
    }
    #endregion


}
