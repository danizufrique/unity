using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class SquareEnemy : Enemy
{
    AIDestinationSetter aiDestination;
    AIPath aiPath;
    
    public float repelForce = 7.5f;
    

    protected override void Start() {
        base.Start();
        aiDestination = GetComponent<AIDestinationSetter>();
        aiPath = GetComponent<AIPath>();
        aiPath.maxSpeed = speed;
    }

    /*
    protected override void Move(){
        nearestPlayer = FindNearestPlayer();

        Vector3 aglomerationDir = avoidAglomeration(aglomerationRadius, 9);
        Debug.Log(aglomerationDir);
        rb.AddForce(aglomerationDir*repelForce, ForceMode2D.Impulse);
        
        aiDestination.target = nearestPlayer.transform;
        
    }
    */
}