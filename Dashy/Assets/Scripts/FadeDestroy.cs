﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeDestroy : MonoBehaviour
{
    public float lifeTime;
    float cont;

    void Start()
    {
        cont = 0;
    }

    // Update is called once per frame
    void Update()
    {
        cont += Time.deltaTime;

        float prop = (lifeTime - cont) / lifeTime;
        Color current = GetComponent<MeshRenderer>().material.color;
        Color newColor = new Color(current.r, current.g, current.b, prop);
        GetComponent<MeshRenderer>().material.color = newColor;

        if (cont >= lifeTime)
            Destroy(gameObject);
    }
}
