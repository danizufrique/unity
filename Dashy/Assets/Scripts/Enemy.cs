﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : MonoBehaviour
{
    
    [Header("Info")]
    

    Player[] players;
    protected Player nearestPlayer = null;

    // COMPONENTS
    LineRenderer[] lineRenderers;
    Explodable explodable;
    protected Rigidbody2D rb;
    

    [Header("Stats")]
    string name = "";
    public Dashy.EnemyType enemyType;
    public float dashExplodeForce = 10;
    public float speed = 5;

    public float aglomerationRadius = 2;

    protected virtual void Start()
    {
        #region Get Components
        explodable = GetComponent<Explodable>();
        players = GameObject.FindObjectsOfType<Player>();
        rb = GetComponent<Rigidbody2D>();
        #endregion

        #region Create Line Renderers
        GameObject lineRenderersHolder = new GameObject("lineRenderersHolder");
        lineRenderersHolder.transform.parent = transform;

        lineRenderers = new LineRenderer[players.Length];
        for (int i = 0; i < players.Length; i++)
        {
            GameObject lr = new GameObject("lr" + players[i].GetComponent<Player>().id.ToString());
            lr.transform.parent = lineRenderersHolder.transform;

            lineRenderers[i] = lr.AddComponent<LineRenderer>();
        }
        #endregion   
    }

    protected virtual void Update()
    {
        Move();
        DrawDashLine();
    }

    protected virtual void Move() {; }

    private void DrawDashLine()
    {
        for (int i = 0; i < players.Length; i++)
        {          
            UpdateLineRenderer(i);          
        }
    }

    private void UpdateLineRenderer(int i)
    {
        Movement mov = players[i].GetComponent<Movement>();
        float distance = Vector2.Distance(players[i].transform.position, transform.position);
        if (distance < mov.enemyDashRadius)
        {
            float distanceToNearest;
            if (mov.nearestDashObject != null && mov.nearestDashObject != this.gameObject)
            {
                distanceToNearest = Vector2.Distance(players[i].transform.position, mov.nearestDashObject.transform.position);
                if (distance >= distanceToNearest)
                {
                    lineRenderers[i].enabled = false;
                    return;
                }
            }

            lineRenderers[i].enabled = true;

            mov.nearestDashObject = this.gameObject;
            Vector2 endPos = transform.position + transform.position - players[i].transform.position;
            lineRenderers[i].SetPosition(0, transform.position);
            lineRenderers[i].SetPosition(1, endPos);
        }
        else
        {
            lineRenderers[i].enabled = false;
        }
    }

    protected Player FindNearestPlayer()
    {
        if (players == null)
            return null;

        Player result = players[0].GetComponent<Player>();
        float minDistance = Vector2.Distance(result.transform.position, transform.position);

        for(int i=1; i<players.Length; i++)
        {
            float newDistance = Vector2.Distance(players[i].transform.position, transform.position);
            if (newDistance < minDistance)
            {
                minDistance = newDistance;
                result = players[i].GetComponent<Player>();
            }
        }

        return result;
    }

    protected Vector4 avoidAglomeration(float radius, LayerMask layer)
    {
        Vector3 sum = Vector3.zero;

        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, radius, layer);

        foreach(Collider2D hit in hits) {
            if(hit.gameObject.transform != transform){
                Vector3 difference = transform.position - hit.gameObject.transform.position;
                sum += difference;
            }
        }

        return sum;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Movement playerMov = collision.gameObject.GetComponent<Movement>();

        if (playerMov != null)
            if (playerMov.isDashing)
                Die();
                
    }

    protected virtual void Die()
    {
        explodable.explode(dashExplodeForce);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, aglomerationRadius);
    }
}
