﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{

    [Header("Layers")]
    public LayerMask groundLayer;

    [Header("Transforms")]
    public Transform rightHand;
    public Transform leftHand;
    public Transform feet;
    public float collisionRadius = .25f;

    [Header("States")]
    public bool onGround;
    public bool onWall;
    public bool onWallL;
    public bool onWallR;

    private void Update()
    {
        onGround = Physics2D.OverlapCircle(feet.position, collisionRadius, groundLayer);
        onWall = Physics2D.OverlapCircle(rightHand.position, collisionRadius, groundLayer) ||
            Physics2D.OverlapCircle(leftHand.position, collisionRadius, groundLayer);

        onWallL = Physics2D.OverlapCircle(leftHand.position, collisionRadius, groundLayer);
        onWallR = Physics2D.OverlapCircle(rightHand.position, collisionRadius, groundLayer);


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(rightHand.position, collisionRadius);
        Gizmos.DrawWireSphere(leftHand.position, collisionRadius);
        Gizmos.DrawWireSphere(feet.position, collisionRadius);
    }
}
