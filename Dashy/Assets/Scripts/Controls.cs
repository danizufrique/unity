﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    public bool A, B, X, Y, L1, R1;
    public float horizontalL, horizontalR, verticalL, verticalR, R2, L2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        A = Input.GetButton("A");
        B = Input.GetButton("B");
        X = Input.GetButton("X");
        Y = Input.GetButton("Y");
        L1 = Input.GetButton("L1");
        R1 = Input.GetButton("R1");
        

        horizontalL = Input.GetAxis("Horizontal");
        horizontalR = Input.GetAxis("HorizontalR");
        verticalL = Input.GetAxis("Vertical");
        verticalR = Input.GetAxis("VerticalR");

        L2 = Input.GetAxis("L2");
        R2 = Input.GetAxis("R2");
    }
}
