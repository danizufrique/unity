﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Collision coll;
    private Rigidbody2D rb;
    private AnimationScript anim;

    [Header("Environment")]
    public LayerMask enemyLayer;

    [Space]
    [Header("Stats")]
    public float speed = 10;
    public float jumpForce = 10;
    public int extraJumps = 2;
    public float dashSpeed = 20;
    public float enemyDashRadius;
    public float wallJumpLerp;
    private float initialGravityScale;
    float x, y, xRaw, yRaw;

    [Header("States")]
    public bool canInteract = true;
    public bool canMove = true;
    public bool canDash = true;
    public bool isDashing = false;
    public int jumpCount = 0;
    public float slideSpeed = 5;

    public GameObject nearestDashObject = null;

    [Header("VFX")]
    public TrailRenderer trailRenderer;
    
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<AnimationScript>();

        initialGravityScale = rb.gravityScale;
    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        xRaw = Input.GetAxisRaw("Horizontal");
        yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);

        if (canInteract)
        {
            Walk(dir);
            //anim.SetHorizontalMovement(x, y, rb.velocity.y);

            if (!coll.onGround) //EnemyDash
            {          
                if (Input.GetButtonDown("R1") && nearestDashObject != null)
                {
                    Dash(nearestDashObject.transform.position - transform.position);
                }                                        
            }

            if (Input.GetButtonDown("A"))
            {
                
                if (coll.onGround || jumpCount < extraJumps)
                {
                    jumpCount++;
                    Jump(Vector2.up);            
                }                
            }

            if (coll.onGround || coll.onWall)
            {
                jumpCount = 0;
            }

            if(Input.GetButtonDown("R1") && !isDashing)
            {
                Vector2 dashDir = new Vector2(x, y);
                if (dashDir.magnitude == 0)
                    dashDir = new Vector2(1, 0);

                Dash(dashDir);
            }

            
        }

        if (coll.onWall && !coll.onGround)
        {
            WallSlide();
        }

    }

    /*
    Enemy[] findNearbyEnemies(float radius)
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, enemyDashRadius, enemyLayer);
        Enemy[] result = new Enemy[hits.Length];

        for (int i = 0; i < hits.Length; i++)
            result[i] = hits[i].GetComponent<Enemy>();

        return result;                             
    }

    Enemy findNearestEnemy(Enemy[] enemies)
    {
        if (enemies == null || enemies.Length == 0)
            return null;

        float distance = Vector2.Distance(transform.position, enemies[0].transform.position);
        int result = 0;

        for(int i=1; i<enemies.Length; i++)
        {
            float dif = Vector2.Distance(transform.position, enemies[i].transform.position);
            if (dif < distance)
            {
                result = i;
                distance = dif;
            }               
        }

        return enemies[result];
    }
    */

    private void Jump(Vector2 dir)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;     
    }

    void Dash(Vector2 dir)
    {
        gameObject.layer = 10;  //PlayerDashing
        isDashing = true;
        canInteract = false;

        rb.velocity = Vector2.zero;
        rb.gravityScale = 0;

        rb.velocity += dir.normalized * dashSpeed;

        StartCoroutine(DashWait());
    }

    IEnumerator DashWait()
    {
        trailRenderer.emitting = true;
        yield return new WaitForSeconds(.3f);
        gameObject.layer = 11;  //Player
        trailRenderer.emitting = false;

        isDashing = false;
        canInteract = true;
        canDash = false;

        rb.velocity = Vector2.zero;
        rb.gravityScale = initialGravityScale;

        yield return new WaitForSeconds(.5f);
        canDash = true;
    }
    void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        rb.velocity = new Vector2(dir.x * speed, rb.velocity.y);
    }

    void WallSlide()
    {
        bool isPushing = (coll.onWallL && x < -.1f) || (coll.onWallR && x > .1f);
        if (isPushing)
            rb.velocity = new Vector2(0, -slideSpeed);
              
    }

    IEnumerator DisableMovement(float t)
    {
        canMove = false;
        x = 0;
        y = 0;
        xRaw = 0;
        yRaw = 0;
        yield return new WaitForSeconds(t);
        canMove = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, enemyDashRadius);
    }
}
