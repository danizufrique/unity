﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [HideInInspector]
    public PlayerController pc;
    [HideInInspector]
    public Animator anim;
    public LayerMask targetMask;

    bool attacking = false;

    [HideInInspector]
    public float initialSpeed, newSpeed;
    public float stun = 0.5f;

    private void Awake()
    {     
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !attacking)
            Attack();
    }

    public virtual void Attack()
    {
        initialSpeed = pc.speed;

        attacking = true;
        anim.SetBool("Attacking", attacking);
        pc.speed = newSpeed;
    }

    public virtual void EndAttack()
    {
        attacking = false;
        anim.SetBool("Attacking", attacking);
        pc.speed = initialSpeed;
    }
}
