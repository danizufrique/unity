﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : WeaponController
{
    public float maxSpeed;

    public Transform pointCenter;
    public float radius;

    public int damage = 1;
    
    private void Start()
    {
        newSpeed = maxSpeed;
    }

    public override void Attack()
    {
        base.Attack();
    }

    public override void EndAttack()
    {
        base.EndAttack();
    }

    public void Shoot()
    {
        GameObject target = myLib.NearestTarget(pointCenter.position, radius, targetMask);
        Health targetHealth;

        if (target != null)
        {
            targetHealth = target.GetComponent<Health>();

            targetHealth.hitStun = stun;
            targetHealth.GetDamage(damage);
            
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(pointCenter.position, radius);        
    }
}
