﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChortAttack : Attack
{

    public LayerMask targetMask;
    Collider2D[] targetsInRange;

    public Transform attackPos;
    public float attackRadius;
    

    public override void BeginAttack()
    {
        base.BeginAttack();
    }

    public void Shoot()
    {
        Health health;

        targetsInRange = Physics2D.OverlapCircleAll(attackPos.position, attackRadius, targetMask);

        foreach(Collider2D target in targetsInRange)
        {
            health = target.GetComponent<Health>();
            health.hitStun = playerStun;
            health.GetDamage(damage);
        }
            
    }

    public override void EndAttack()
    {
        base.EndAttack();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRadius);
    }
}
