﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AI))]
public class AI_Editor : Editor
{
    private void OnSceneGUI()
    {        
        AI ai = (AI)target;
        float sign = Mathf.Sign(ai.gameObject.transform.localScale.x);

        Handles.color = Color.cyan;
        Handles.DrawWireArc(ai.transform.position, Vector3.forward, Vector3.up, 360, ai.viewRadius);
        Vector3 viewAngleA = ai.DirFromAngle(-ai.viewAngle / 2, false);
        Vector3 viewAngleB = ai.DirFromAngle(ai.viewAngle / 2, false);

        Handles.color = Color.yellow;
        Handles.DrawWireArc(ai.transform.position, Vector3.forward, Vector3.up, 360, ai.patrolRadius);
        Handles.color = Color.grey;
        Handles.DrawWireArc(ai.transform.position, Vector3.forward, Vector3.up, 360, ai.scapeRadius);

        Handles.color = Color.blue;
        Handles.DrawLine(ai.transform.position, ai.transform.position + sign * viewAngleA * ai.viewRadius);
        Handles.DrawLine(ai.transform.position, ai.transform.position + sign * viewAngleB * ai.viewRadius);

        Handles.color = Color.red;
        foreach (Transform visibleTarget in ai.visibleTargets)
            Handles.DrawLine(ai.transform.position, visibleTarget.position);
    }

}
