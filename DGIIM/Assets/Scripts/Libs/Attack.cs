﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    Animator anim;
    AI ai;

    public int damage;
    public bool isAttacking = false;
    public float playerStun;

    void Awake()
    {
        anim = GetComponent<Animator>();
        ai = GetComponent<AI>();
    }

    public virtual void BeginAttack()
    {
        isAttacking = true;
        anim.SetBool("Attacking", isAttacking);
        ai.canMove = false;

    }

    public virtual void EndAttack()
    {
        isAttacking = false;
        anim.SetBool("Attacking", isAttacking);
        ai.canMove = true;

    }


}
