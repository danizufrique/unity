﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    #region Components
    Rigidbody2D rb;
    Animator anim;
    Attack attack;
    #endregion

    #region FOV
    [Header("FOV")]
    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;
    public float scapeRadius;

    [Header("Detection")]
    public bool playerDetected = false;
    public float refreshDelay = .2f;

    public LayerMask targetMask;
    public LayerMask obstacle;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    Transform target = null;
    #endregion

    #region Patrol
    
    [Header("Patrol")]
    public float patrolSpeed;
    public float patrolRadius;

    float xRan;
    float yRan;
    #endregion

    #region Chase
    [Header("Chase")]
    public float chaseSpeed;
    public bool canMove = true;
    public float caughtDistance;
    #endregion

    #region UI
    [Header("UI")]
    public GameObject exclamation;
    #endregion

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        attack = GetComponent<Attack>();

        StartCoroutine(FindTargetsWithDelay(refreshDelay));

        xRan = Random.Range(transform.position.x - patrolRadius, transform.position.x + patrolRadius);
        yRan = Random.Range(transform.position.y - patrolRadius, transform.position.y + patrolRadius);
    }

    void Update()
    {
        anim.SetFloat("Speed", rb.velocity.magnitude);
        
        if (rb.velocity.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else if (rb.velocity.x > 0)
            transform.localScale = Vector3.one;

        if (canMove)
        {
            if (!playerDetected)
            {
                Patrol();
                anim.speed = 1;
            }
            else
                Chase();
        }
        else
            rb.velocity = Vector2.zero;
    }

    #region FOV
    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            playerDetected = FindVisibleTargets();
            
        }
    }

    bool FindVisibleTargets()
    {
        if (!playerDetected)
        {
            Collider2D[] targetsInViewRadius;
            Vector3 dirToTarget = Vector3.zero;
            float angle;

            targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, targetMask);

            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                Transform iTarget = targetsInViewRadius[i].transform;

                dirToTarget = (iTarget.position - transform.position).normalized;
                angle = Vector3.Angle(transform.localScale.x * transform.right, dirToTarget);

                if (angle < viewAngle / 2)
                {
                    float distanceToTarget = Vector3.Distance(transform.position, iTarget.position);

                    if (!Physics2D.Raycast(transform.position, dirToTarget, distanceToTarget, obstacle))
                    {
                        visibleTargets.Add(iTarget);

                        if (visibleTargets.Count == 1)
                            target = visibleTargets[0];
                        else if (visibleTargets.Count > 1)
                            target = FindNearestTarget();
                       
                        //exclamation.SetActive(true);
                    }
                }
            }
        }
        else if ((target.transform.position - transform.position).magnitude > scapeRadius)
        {
            target = null;
            visibleTargets.Clear();
            //exclamation.SetActive(false);
        }

        return visibleTargets.Count > 0;
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool isGlobal)
    {
        if (!isGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0);
    }
    #endregion

    #region Movement
    void Patrol(){
        float delta = .1f;
        
        Vector2 goal = new Vector2(xRan, yRan);
        Vector2 currentPos = new Vector2(transform.position.x, transform.position.y);

        if((currentPos-goal).magnitude < delta) //Ha llegado
        {
            xRan = Random.Range(transform.position.x - patrolRadius, transform.position.x + patrolRadius);
            yRan = Random.Range(transform.position.y - patrolRadius, transform.position.y + patrolRadius);
        }
        else //No ha llegado
        {
            Vector3 dir = (new Vector3(xRan, yRan) - transform.position).normalized;
            rb.velocity = dir * patrolSpeed;
        }                 
    }

    void Chase(){
        anim.speed = chaseSpeed / patrolSpeed;

        Vector3 dir = (target.transform.position - transform.position);
        float magnitude = dir.magnitude;

        if (magnitude > caughtDistance)
        {           
            rb.velocity = dir.normalized * chaseSpeed;
        }
        else
        {
            rb.velocity = Vector2.zero;
            attack.BeginAttack();
        }

    }

    Transform FindNearestTarget(){
        Transform nearest = visibleTargets[0];
        Transform thisPosition = transform;
        float bestMagnitude = (visibleTargets[0].transform.position - thisPosition.transform.position).magnitude;

        for(int i=1; i<visibleTargets.Count; i++){
          float magnitude = (visibleTargets[i].transform.position - thisPosition.transform.position).magnitude;
          if(magnitude < bestMagnitude){
              bestMagnitude = magnitude;
              nearest = visibleTargets[i];
          }
        }

        return nearest;
    }

    #endregion

}
