﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myLib : MonoBehaviour
{
    static Rigidbody2D rb;
    static Animator anim;

    public static void InitController(Rigidbody2D rb2d, Animator animator)
    {
        rb = rb2d;
        anim = animator;
    }

    public static void Run(float h, float v, float speed)
    {
        if (Mathf.Abs(h) > 0 || Mathf.Abs(v) > 0)
        {
            Vector2 force = new Vector2(h, v).normalized * speed;

            rb.velocity = force;

            if (rb.velocity.x > 0)
                rb.gameObject.transform.localScale = Vector3.one;

            else if (rb.velocity.x < 0)
                rb.gameObject.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
            rb.velocity = Vector2.zero;

        anim.SetFloat("Speed", rb.velocity.magnitude);

    }

    public static GameObject NearestTarget(Vector3 point, float radius, int layer)
    {
        GameObject target = null;
        int length = 0;

        Collider2D[] overlapTargets = Physics2D.OverlapCircleAll(point, radius, layer);
        length = overlapTargets.Length;

        if (length > 0)
        {
            if (length == 1)
                target = overlapTargets[0].gameObject;
            else
                target = FindNearestInList(overlapTargets, point);
        }

        return target;

    }

    static GameObject FindNearestInList(Collider2D[] list, Vector3 position)
    {
        GameObject nearest = list[0].gameObject;

        float bestMagnitude = (list[0].transform.position - position).magnitude;

        for (int i = 1; i < list.Length; i++)
        {
            float magnitude = (list[i].transform.position - position).magnitude;
            if (magnitude < bestMagnitude)
            {
                bestMagnitude = magnitude;
                nearest = list[i].gameObject;
            }
        }

        return nearest;
    }



}
