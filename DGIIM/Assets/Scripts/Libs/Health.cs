using UnityEngine;
using System.Collections;

public class Health: MonoBehaviour{


    #region Controllers     
    public Animator anim;
    AI ai;
    PlayerController pc;
    #endregion

    #region Parameters
    public int health;
    public bool canGetDamage;
    [HideInInspector]
    public float hitStun;

    
    public bool isHit;
    public bool isDead;
    #endregion

    private void Start()
    {
        ai = GetComponent<AI>();
        pc = GetComponent<PlayerController>();
    }

    public virtual void GetDamage(int damage){
        health -= damage;

        if(canGetDamage){
            if(health <= 0)
                Die();
            else
                Hit();
        }
    }

    public virtual void Die(){
        Debug.Log(gameObject.name + "was killed");
        isDead = true;

        if (pc != null)
            pc.canMove = false;
        else if (ai != null)
            ai.canMove = false;

        anim.SetBool("Dead", isDead);
    }

    public  virtual void Hit(){
        Debug.Log(gameObject.name + "was hit");
        StartCoroutine(WaitToMove(hitStun));
        isHit = true;
        anim.SetBool("Hit", isHit);
    }

    public IEnumerator WaitToMove(float stun)
    {
        if (pc != null)
        {
            pc.canMove = false;
            yield return new WaitForSeconds(stun);
            pc.canMove = true;
            isHit = false;
            anim.SetBool("Hit", isHit);
        }
        else if (ai != null)
        {
            ai.canMove = false;
            yield return new WaitForSeconds(stun);
            ai.canMove = true;
            isHit = false;
            anim.SetBool("Hit", isHit);
        }
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
