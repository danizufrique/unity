﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    Health healthComponent;

    void Awake()
    {
        healthComponent = GetComponent<Health>();
    }

    public override void Hit()
    {       
        base.Hit();      
    }

    public override void Die()
    {
        base.Die();      
    }
}
