﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Components
    Rigidbody2D rb;
    Animator anim;
    WeaponController wc;
    #endregion

    #region Stats
    [Header("Stats")]
    public float speed;
    public bool canMove = true;
    #endregion

    #region Combat
    [Header("Combat")]
    private GameObject currentWeapon;
    public GameObject[] weapons;
    public Transform weaponHinge;
    #endregion

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();

        myLib.InitController(rb, anim);
    }

    void Start()
    {
        ChangeWeapon(0);     
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            myLib.Run(h, v, speed);
        }
    }

    void ChangeWeapon(int index)
    {
        if(currentWeapon != null)
            Destroy(currentWeapon);

        currentWeapon = Instantiate(weapons[index], weaponHinge.transform.position, Quaternion.identity);
        currentWeapon.transform.parent = transform;

        wc = currentWeapon.GetComponent<WeaponController>();
        wc.pc = this;

        
    }
}
