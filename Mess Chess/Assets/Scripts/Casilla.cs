﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casilla : MonoBehaviour
{
    GameController controller;

    public char letter;
    public int index;
    public bool free = true;
    public new string name;
    public Pieza currentPiece;
    public Vector2Int coordinates;

    public int coorX, coorY;
    public Color originalColor;

    // SPECIAL STATES
    int enumLength = 0;
    int turnsToEnd = 0;
    public bool specialActive = false;
    State currentState;

    // Components
    BoxCollider col;
    MeshRenderer meshRenderer;
    
    public enum State{
        Idle, Kill, Shield, DoubleMov 
    };

    private void Start()
    {
        controller = GameController.instance;
        col = GetComponent<BoxCollider>();

        meshRenderer = GetComponent<MeshRenderer>();
        originalColor = meshRenderer.material.GetColor("_BaseColor");

        currentState = State.Idle;

        enumLength = System.Enum.GetNames(typeof(State)).Length;
    }

    public void SetName(char l, int i)
    {
        letter = l;
        index = i;
        name = letter + index.ToString();
        gameObject.name = name;
    }

    private void Update()
    {
        if (CheckClick() && controller.selectedPiece) {
            controller.selectedPiece.CheckMove(this);
        }
    }

    bool CheckClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (col.Raycast(ray, out hit, 100))
                return true;
        }

        return false;
    }

    public void ChangeColor(Color newColor, bool emission)
    {
        if(emission){
            meshRenderer.material.EnableKeyword("_EMISSION");
            meshRenderer.material.SetColor("_EmissionColor", newColor);
        }
        else
            meshRenderer.material.DisableKeyword("_EMISSION");

        meshRenderer.material.SetColor("_BaseColor", newColor);
    }

    public void SetState(State state){ 
        Debug.Log("SETTING STATE TO " + state);

        if(currentState != State.Idle && state == State.Idle)
            controller.activeBox--;
        else if(currentState == State.Idle){
            specialActive = true;
            controller.activeBox++;
        }

        currentState = state;
        turnsToEnd = GetStateTurns(state);

        switch(state){
            case State.Idle:
                specialActive = false;
                ChangeColor(originalColor, false);
                break;
            case State.Kill:
                ChangeColor(Color.red, true);
                break;
            case State.Shield:
                ChangeColor(Color.blue, true);
                break;
            case State.DoubleMov:
                ChangeColor(Color.magenta, true);
                break;
            default:
                break;
        }
    }

    int GetStateTurns(State state){
        switch(state){
            case State.Idle:
                return 0;
            case State.Kill:
                return controller.killTurns;
            case State.Shield:
                return controller.shieldTurns;
            case State.DoubleMov:
                return controller.doubleMovTurns;
            default:
                return 0;
        } 
    }

    int GetStateId(State state){
        switch(state){
            case State.Idle:
                return 0;
            case State.Kill:
                return 1;
            case State.Shield:
                return 2;
            case State.DoubleMov:
                return 3;
            default:
                return 0;
        }
    }

    State GetState(int id){
        switch(id){
            case 0:
                return State.Idle;
            case 1:
                return State.Kill;
            case 2:
                return State.Shield;
            case 3:
                return State.DoubleMov;
            default:
                return State.Idle;
        }
    }
    public void newTurn(){

        if (specialActive)
            turnsToEnd--;

        if (turnsToEnd<=0 && specialActive)
            SetState(State.Idle);
    }

    public void RandomState(){
        int random = Random.Range(0, enumLength);
        State randState = GetState(random);

        SetState(randState);
    }

    public void PiecePlaced()
    {
        switch(currentState){
            case State.Idle:
                break;
            case State.Kill:
                Kill();
                break;
            case State.Shield:
                Shield();
                break;
            case State.DoubleMov:
                DoubleMov();
                break;
            default:
                break;
        }
        SetState(State.Idle);
    }

    void Kill(){
        StartCoroutine(currentPiece.DieEffect(this, false, true));
    }

    void Shield(){
        currentPiece.SetShield(true);
    }

    void DoubleMov(){}
}
