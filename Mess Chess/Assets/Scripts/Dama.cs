﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dama : Pieza
{
    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        // BISHOP
        int difX = Mathf.Abs(box.coordinates.x - currentBox.coordinates.x);
        int difY = Mathf.Abs(box.coordinates.y - currentBox.coordinates.y);

        if (difX == difY) {
            if(box.currentPiece == null || box.currentPiece.playerId != playerId)
                StartCoroutine(Move(box, true));
        } 
            

        // ROOK
        if (box.coordinates.x == currentBox.coordinates.x || box.coordinates.y == currentBox.coordinates.y){
            Pieza boxPiece = box.currentPiece;

            if(boxPiece == null || boxPiece.playerId != playerId)
                StartCoroutine(Move(box, true));
        }

    }

    public override bool FreePath(Casilla box){

        // BISHOP
        bool freeBishop = true;
        int sumX = 0, sumY = 0;

        sumX = (box.coordinates.x >= currentBox.coordinates.x) ? 1 : -1;
        sumY = (box.coordinates.y >= currentBox.coordinates.y) ? 1 : -1;

        for(int i=currentBox.coordinates.x + 1; i<box.coordinates.x && freeBishop; i++){

            if(controller.table[currentBox.coordinates.x + sumX, currentBox.coordinates.y + sumY].currentPiece != null)
                freeBishop = false;

            sumX = (sumX > 0) ? sumX+1 : sumX-1;
            sumY = (sumY > 0) ? sumY+1 : sumY-1;
        }

        // ROOK
        bool freeRook = true;
        int sum = 0;

        if(box.coordinates.x == currentBox.coordinates.x){
            sum = (box.coordinates.x >= currentBox.coordinates.x) ? 1 : -1;

            for(int i=currentBox.coordinates.x+1; i<box.coordinates.x && freeRook; i++)
                if(controller.table[i, currentBox.coordinates.y].currentPiece != null)
                    freeRook = false;
        }
        else if(box.coordinates.y == currentBox.coordinates.y){
            sum = (box.coordinates.y >= currentBox.coordinates.y) ? 1 : -1;

            for(int i=currentBox.coordinates.y+1; i<box.coordinates.y && freeRook; i++)
                if(controller.table[currentBox.coordinates.x, i].currentPiece != null)
                    freeRook = false;
        }
        else
            freeRook = false;

        return freeBishop || freeRook;

    }
}
