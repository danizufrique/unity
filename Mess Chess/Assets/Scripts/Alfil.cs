﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alfil : Pieza
{
    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        int difX = Mathf.Abs(box.coordinates.x - currentBox.coordinates.x);
        int difY = Mathf.Abs(box.coordinates.y - currentBox.coordinates.y);

        if(difX==difY){
            if(box.currentPiece == null || box.currentPiece.playerId != playerId)
                StartCoroutine(Move(box, true));

        }

    }

    public override bool FreePath(Casilla box){
        bool free = true;
        int sumX = 0, sumY = 0;

        sumX = (box.coordinates.x >= currentBox.coordinates.x) ? 1 : -1;
        sumY = (box.coordinates.y >= currentBox.coordinates.y) ? 1 : -1;

        for(int i=currentBox.coordinates.x + 1; i<box.coordinates.x && free; i++){

            if(controller.table[currentBox.coordinates.x + sumX, currentBox.coordinates.y + sumY].currentPiece != null)
                free = false;

            sumX = (sumX > 0) ? sumX+1 : sumX-1;
            sumY = (sumY > 0) ? sumY+1 : sumY-1;
        }

        return free;
    }
}
