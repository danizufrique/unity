﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rey : Pieza
{
    bool hasMoved = false;

    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        int difX = Mathf.Abs(box.coordinates.x - currentBox.coordinates.x);
        int difY = Mathf.Abs(box.coordinates.y - currentBox.coordinates.y);

        if( (difX <= 1 && difY <= 1) && box != currentBox){ // Movimiento normal
          if(box.currentPiece == null){
              StartCoroutine(Move(box, true));
              hasMoved = true;
          }
          else if(box.currentPiece.playerId != playerId){
            StartCoroutine(Move(box, true));
            hasMoved = true;
          }

        }
        else if(difX == 0 && !hasMoved && box.currentPiece == null)
        {
            Debug.Log("ENROQUE 1");
            int direction = (box.coordinates.y > currentBox.coordinates.y) ? 1 : -1;

            Casilla kingNewBox = controller.table[currentBox.coorX, currentBox.coorY + 2 * direction];
            Pieza rook = (direction == 1) ? 
                controller.table[currentBox.coorX, 7].currentPiece : 
                controller.table[currentBox.coorX, 0].currentPiece;
            
            if(rook.name == "Rook")
            {
                Debug.Log("ENROQUE 2");
                if (!rook.GetComponent<Torre>().hasMoved)
                {
                    
                    Casilla rookNewBox = (direction == 1) ?
                        controller.table[currentBox.coorX, rook.currentBox.coorY - 2] :
                        controller.table[currentBox.coorX, rook.currentBox.coorY + 3];

                    Debug.Log(rookNewBox.coorX + ", " + rookNewBox.coorY );

                    if (rookNewBox.currentPiece == null)
                    {
                        Debug.Log("ENROQUE 4");
                        StartCoroutine(Move(box, false));
                        StartCoroutine(rook.GetComponent<Torre>().Move(rookNewBox, true));
                    }
                }
          
            }
        }
    }
    

    public override bool FreePath(Casilla box){
        bool free = true;
        int sumX = 0, sumY = 0;

        sumX = (box.coordinates.x >= currentBox.coordinates.x) ? 1 : -1;
        sumY = (box.coordinates.y >= currentBox.coordinates.y) ? 1 : -1;

        for(int i=currentBox.coordinates.x + 1; i<box.coordinates.x && free; i++){

            if(controller.table[currentBox.coordinates.x + sumX, currentBox.coordinates.y + sumY].currentPiece != null)
                free = false;

            sumX = (sumX > 0) ? sumX+1 : sumX-1;
            sumY = (sumY > 0) ? sumY+1 : sumY-1;
        }

        return free;
    }
}

