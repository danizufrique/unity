﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caballo : Pieza
{
    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        float difX = Mathf.Abs(box.coordinates.x - currentBox.coordinates.x);
        float difY = Mathf.Abs(box.coordinates.y - currentBox.coordinates.y);

        if ((difX == 2 && difY == 1) || (difX == 1 && difY == 2) ){
          Pieza boxPiece = box.currentPiece;

            
          if(box.currentPiece == null || box.currentPiece.playerId != playerId)
            StartCoroutine(Move(box, true));
        }
    }
}
