﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    #region Singleton
    public static GameController instance;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("MORE THAN ONE GAME CONTROLER!!!");
            return;
        }

        instance = this;
    }
    #endregion

    [Header("BOX")]
    Material whiteBoxMat, blackBoxMat;


    [Header("Players")]
    Player p1, p2;
    public Pieza selectedPiece;
    public int turn = 1;

    public GameObject boxPrefab;
    public Casilla[,] table;
    public int moves;
    public int activeBox = 0;

    [Header("Box Turns")]
    public int killTurns;
    public int shieldTurns;
    public int doubleMovTurns;
    
    // PREFABS
    public Material p1Material, p2Material;
    public GameObject pawnPrefab, rookPrefab, knightPrefab, bishopPrefab;
    public GameObject queenPrefab, kingPrefab;

    public Color p1Color, p2Color;

    Vector3 spawnOffSet = new Vector3(0, 0.75f, 0);

    enum PieceType{
        Pawn, Rook, Knight, Bishop, Queen, King
    };
    
    void Start()
    {
        LoadResources();
        p1 = GameObject.FindGameObjectWithTag("P1").GetComponent<Player>();
        p2 = GameObject.FindGameObjectWithTag("P2").GetComponent<Player>();
        selectedPiece = null;

        CreateTable();
        StartCoroutine(SpawnPieces());
        
    }

    void LoadResources()
    {
        whiteBoxMat = Resources.Load<Material>("Materials/White_Mat");
        blackBoxMat = Resources.Load<Material>("Materials/Black_Mat");
    }

    // Instantiate Boxes and Add them to table array
    void CreateTable()
    {
        Transform holder = GameObject.FindGameObjectWithTag("Table").transform;
        table = new Casilla[8,8];

        float posX = 0;
        float posZ = 0;
        float dim = boxPrefab.GetComponent<BoxCollider>().size.x * boxPrefab.transform.localScale.x;
        Material currentMat = blackBoxMat;

        char currentLetter = 'a';
        for (int i = 0; i < 8; i++)
        {
            posX = 0;
            int currentIndex = 1;

            for (int j = 0; j < 8; j++)
            {
                // GameObject
                GameObject instBox =Instantiate(boxPrefab, new Vector3(posX, 0, posZ), Quaternion.identity);
                instBox.transform.parent = holder;
                instBox.GetComponent<MeshRenderer>().material = currentMat;

                // Casilla
                Casilla box = instBox.GetComponent<Casilla>();
                box.SetName(currentLetter, currentIndex);
                box.coordinates = new Vector2Int(i, j);
                box.coorX = i;
                box.coorY = j;
                table[i, j] = box;

                // Change Material
                if (currentIndex < 8)
                {
                    if (currentMat == blackBoxMat) currentMat = whiteBoxMat;
                    else currentMat = blackBoxMat;
                }

                currentIndex++;
                posX += dim;
            }

            posZ += dim;
            currentLetter++;
        }
    }

    // Spawns Players Pieces
    IEnumerator SpawnPieces(){
        // Pawns
        for(int i=0; i<8; i++){
            StartCoroutine(SpawnPiece(pawnPrefab, 1, i, 1, p1Color));
            StartCoroutine(SpawnPiece(pawnPrefab, 6, i, 2, p2Color));
        }

        yield return new WaitForSeconds(0.25f);
        // Rooks
        StartCoroutine(SpawnPiece(rookPrefab, 0, 0, 1, p1Color));
        StartCoroutine(SpawnPiece(rookPrefab, 0, 7, 1, p1Color));

        StartCoroutine(SpawnPiece(rookPrefab, 7, 0, 2, p2Color));
        StartCoroutine(SpawnPiece(rookPrefab, 7, 7, 2, p2Color));

        yield return new WaitForSeconds(0.25f);
        // Knights
        StartCoroutine(SpawnPiece(knightPrefab, 0, 1, 1, p1Color));
        StartCoroutine(SpawnPiece(knightPrefab, 0, 6, 1, p1Color));

        StartCoroutine(SpawnPiece(knightPrefab, 7, 1, 2, p2Color));
        StartCoroutine(SpawnPiece(knightPrefab, 7, 6, 2, p2Color));

        yield return new WaitForSeconds(0.25f);
        // Bishops
        StartCoroutine(SpawnPiece(bishopPrefab, 0, 2, 1, p1Color));
        StartCoroutine(SpawnPiece(bishopPrefab, 0, 5, 1, p1Color));

        StartCoroutine(SpawnPiece(bishopPrefab, 7, 2, 2, p2Color));
        StartCoroutine(SpawnPiece(bishopPrefab, 7, 5, 2, p2Color));

        yield return new WaitForSeconds(0.25f);
        // Queens
        StartCoroutine(SpawnPiece(queenPrefab, 0, 3, 1, p1Color));
        StartCoroutine(SpawnPiece(queenPrefab, 7, 3, 2, p2Color));

        yield return new WaitForSeconds(0.25f);
        // Kings
        StartCoroutine(SpawnPiece(kingPrefab, 0, 4, 1, p1Color));
        StartCoroutine(SpawnPiece(kingPrefab, 7, 4, 2, p2Color));
    }

    // Instantiate and Initialize Pieces
    public IEnumerator SpawnPiece(GameObject prefab, int boxX, int boxY, int playerId, Color color)
    {
        // GameObject
        GameObject instPiece = Instantiate(prefab, table[boxX, boxY].transform.position + spawnOffSet, Quaternion.identity); 
        Pieza piece = instPiece.GetComponent<Pieza>();

        // Initialize Piece Component        
        piece.playerId = playerId;
        piece.currentBox = table[boxX, boxY];
        piece.originalColor = (playerId == 1) ? p1Color : p2Color;
        piece.originalMat = (playerId == 1) ? p1Material : p2Material;
        
        // Adds piece to Player and Box
        table[boxX, boxY].currentPiece = piece;
        Player piecePlayer = (playerId == 1) ? p1 : p2;
        piecePlayer.AddPiece(piece);

        // Detect Mesh Renderers
        foreach (Transform child in instPiece.transform)
        {
            if (child.gameObject.GetComponent<MeshRenderer>())
            {
                child.gameObject.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", color);
                piece.meshes.Add(child.gameObject.GetComponent<MeshRenderer>());
            }
            else
            {
                int children = child.childCount;
                if (children > 0)
                {
                    for (int i = 0; i < children; i++)
                    {
                        if (child.GetChild(i).GetComponent<MeshRenderer>())
                        {
                            child.GetChild(i).GetComponent<MeshRenderer>().material.SetColor("_BaseColor", color);
                            piece.meshes.Add(child.GetChild(i).GetComponent<MeshRenderer>());
                        }
                    }
                }
            }
        }

        // Spawn Effect
        StartCoroutine(piece.SpawnEffect());

        while (!piece.auxBool)
            yield return null;
    }

    // Updates moves, calls every box, new Box effect
    public void newTurn(){
        moves++;
        turn = (turn == 1) ? 2 : 1;

        // New turn for each Box
        foreach(Casilla box in table)
            box.newTurn();

        // Box effects
        float random = Random.Range(0,1.0f);
        float prob = 1;

        Casilla randBox;        

        if(random < prob){   
            int randRow = Random.Range(0, 8);
            int randCol = Random.Range(0,8);

            randBox = table[randRow, randCol];

            if(randBox.currentPiece == null && randBox.specialActive == false)
                randBox.RandomState();          
        }       
    }
}
