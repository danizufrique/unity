﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : Pieza
{
    public bool hasMoved = false;

    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        if(box.coordinates.x == currentBox.coordinates.x || box.coordinates.y == currentBox.coordinates.y){
            Pieza boxPiece = box.currentPiece;

            if(boxPiece == null || boxPiece.playerId != playerId){
              StartCoroutine(Move(box, true));
              hasMoved = true;
            }
        }
    }

    public override bool FreePath(Casilla box){
        bool free = true;
        int sum = 0;

        if(box.coordinates.x == currentBox.coordinates.x){
            sum = (box.coordinates.y >= currentBox.coordinates.y) ? 1 : -1;

            for(int i=currentBox.coordinates.y+1; i<box.coordinates.y && free; i++)
                if (controller.table[currentBox.coordinates.x, i].currentPiece != null)         
                    free = false;
        }
        else if(box.coordinates.y == currentBox.coordinates.y){
            sum = (box.coordinates.x >= currentBox.coordinates.x) ? 1 : -1;

            for(int i=currentBox.coordinates.x+1; i<box.coordinates.x && free; i++)
                if (controller.table[i, currentBox.coordinates.y].currentPiece != null)
                    free = false;
        }
        else
            free = false;

        return free;
    }
}
