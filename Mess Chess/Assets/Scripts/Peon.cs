﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peon : Pieza
{
    bool comidaPaso = false;
    Pieza diePiece;
    int doubleStartTurn = -1;
    public GameObject promoteHud;

    new void Start()
    {
        base.Start();
        diePiece = new Pieza();
    }

    new void Update()
    {
        base.Update();
    }

    public override void CheckMove(Casilla box)
    {
        int playerMod = (playerId == 1) ? 1 : -1;

        if (box.currentPiece == null)    // MOVER HACIA DELANTE
        {
            Vector2Int newCoordinates = new Vector2Int(currentBox.coordinates.x + playerMod, currentBox.coordinates.y);
            Vector2Int newCoordinatesDoubleStart = new Vector2Int(currentBox.coordinates.x + 2*playerMod, currentBox.coordinates.y);

            if (box.coordinates == newCoordinates)  // NORMAL
            {
                bool promotion = (playerId == 1) ? box.coorX == 7 : box.coorX == 0;

                if (promotion)
                {  // PROMOTION
                    StartCoroutine(MoveToBox(box, false));
                    promoteHud.SetActive(true);
                    auxBool = false;
                }
                else
                    StartCoroutine(Move(box, true));
            }
            else if (box.coordinates == newCoordinatesDoubleStart && DoubleStartPos())
            {  // SALIDA DOBLE
                doubleStartTurn = controller.moves;
                StartCoroutine(Move(box, true));
            }
            else if (CapturaAlPaso(box))    // COMIDA AL PASO
                StartCoroutine(PawnMove(box, diePiece));

        }
        else    // COMER PIEZA
        {
            Debug.Log("POALAL");
            if(box.currentPiece.playerId != playerId){
                if ((int)Mathf.Abs(currentBox.coordinates.x - box.coordinates.x) == 1 && (int)Mathf.Abs(currentBox.coordinates.y - box.coordinates.y) == 1)
                    StartCoroutine(Move(box, true));
            }
        }

    }

    bool DoubleStartPos()
    {
        int x = currentBox.coordinates.x;

        if (playerId == 1)
        {
            if (x == 1)
                return true;
        }
        else if (x == 6)
            return true;

        return false;
    }

    bool CapturaAlPaso(Casilla box)
    {
        // El peón que come debe estar en la 5 fila
        bool correctRow = (playerId == 1) ? currentBox.coordinates.x == 4 : currentBox.coordinates.x == 3;

        // Columna adyacente
        bool correctColumn = (int)Mathf.Abs(currentBox.coordinates.y - box.coordinates.y) == 1;

        // Deben estar en la misma fila
        Vector2Int coor = box.coordinates;

        diePiece = (playerId == 1) ?
            controller.table[coor.x - 1, coor.y].currentPiece :
            controller.table[coor.x + 1, coor.y].currentPiece;

        bool side = false;
        bool correctTurn = false;
        if (diePiece != null){
             side = diePiece.name == "Pawn";
             if(side && diePiece.playerId != playerId)
                correctTurn = (controller.moves - diePiece.GetComponent<Peon>().doubleStartTurn == 1);
         }

        Debug.Log(correctRow);
        Debug.Log(correctColumn);
        Debug.Log(side);
        Debug.Log(correctRow);

        comidaPaso = correctRow && correctColumn && side && correctTurn;
        return comidaPaso;
    }

    IEnumerator PawnMove(Casilla box, Pieza destroyPiece)
    {
        Disselect();

        if (comidaPaso)   // COMER PIEZA
        {
            // Material set-up
            Debug.Log("diemat: " + dieMat);

            if(destroyPiece.playerId != 0){
                destroyPiece.ChangeMat(dieMat);

                foreach (Renderer mesh in destroyPiece.meshes)
                    mesh.material.SetColor("_Color", destroyPiece.originalColor);

                // Die effect, replace piece in box
                StartCoroutine(DieEffect_Passant(destroyPiece.currentBox));
            }
        }

        currentBox = box;

        while (!auxBool)
        {
            yield return null;
        }

        StartCoroutine(MoveToBox(box, true));
        controller.turn = (playerId == 1 ) ? 2 : 1;
    }

    public IEnumerator Promote(Casilla box, GameObject piece)
    {
        GameObject oldPiece = this.gameObject;
        Vector3 startPos = transform.position;
        Vector3 endPos = new Vector3(box.transform.position.x, transform.position.y, box.transform.position.z);

        float i = 0.0f;
        float rate = 1.0f / 0.3f;  // El 0.3 es un valor para el tiempo que dura

        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            this.transform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }

        
        StartCoroutine(DieEffect(currentBox, true, false));

        while (!auxBool)
        {
            yield return null;
        }

        StartCoroutine(controller.SpawnPiece(piece, box.coorX, box.coorY, playerId, originalColor));
        Destroy(oldPiece.gameObject);
        auxBool = true;
        controller.newTurn();

    }

    IEnumerator DieEffect_Passant(Casilla box){
        float cont = 0;

        while (cont < 1) {
            foreach (Renderer mesh in box.currentPiece.meshes)
                mesh.material.SetFloat("_Progress", cont);

            yield return new WaitForSeconds(.0025f);
            cont += .008f;
        }

        // Replace piece in box
        Destroy(box.currentPiece.gameObject);
    }

}
