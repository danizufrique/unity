﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int id;
    public List<Pieza> activePieces;
    public List<Pieza> deletedPieces;

    GameObject pawns, rooks, knights, bishops, royalty;

    private void Awake()
    {
        CreatePieceHolders();
    }
    private void Start()
    {
        activePieces = new List<Pieza>();
        deletedPieces = new List<Pieza>();

        
    }

    public void AddPiece(Pieza piece)
    {
        activePieces.Add(piece);
        piece.gameObject.name = id.ToString() + "_" + piece.name;
        piece.player = this;

        switch (piece.name)
        {
            case "Pawn":
                piece.gameObject.transform.parent = pawns.transform;
                break;

            case "Rook":
                piece.gameObject.transform.parent = rooks.transform;
                break;

            case "Knight":
                piece.gameObject.transform.parent = knights.transform;
                break;

            case "Bishop":
                piece.gameObject.transform.parent = bishops.transform;
                break;

            case "King":
                piece.gameObject.transform.parent = royalty.transform;
                break;

            case "Queen":
                piece.gameObject.transform.parent = royalty.transform;
                break;
        }
    }

    void CreatePieceHolders()
    {
        pawns = new GameObject();
        pawns.name = "Pawns";
        pawns.transform.parent = transform;

        rooks = new GameObject();
        rooks.name = "Rooks";
        rooks.transform.parent = transform;

        knights = new GameObject();
        knights.name = "Knights";
        knights.transform.parent = transform;

        bishops = new GameObject();
        bishops.name = "Bishops";
        bishops.transform.parent = transform;

        royalty = new GameObject();
        royalty.name = "Royalty";
        royalty.transform.parent = transform;
    }
}
