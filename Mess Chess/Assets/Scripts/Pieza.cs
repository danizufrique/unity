﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    public class Pieza : MonoBehaviour
    {
        // PAREMATERS
        public new string name;
        public int playerId;
        public Casilla currentBox;
        public Player player;
        public bool active = true;

        [Header("VFX")]
        public List<MeshRenderer> meshes;
        GameObject shieldEffect;
        GameObject instShield;

        public Color actualColor;
        public Color originalColor;

        public Material originalMat;
        public Material actualMaterial;
        public Material highlightMat;
        public Material dieMat;


        [Header("States")]
        public bool selected = false;
        public bool shield = false;

        public bool auxBool;

        // COMPONENTS
        BoxCollider col;
        public GameController controller;
        Light light;



        public void Start()
        {
            controller = GameController.instance;
            col = GetComponent<BoxCollider>();
            light = GetComponentInChildren<Light>();
            light.gameObject.SetActive(false);

            auxBool = true;

            LoadResources();
            //SetShield(true);
        }

        public Pieza()
        {
            name = "new pieza";
            playerId = 0;
        }

        public Pieza(string n, int p)
        {
            name = n;
            playerId = p;
        }

        public void Update()
        {
            if (CheckClick() && controller.turn == playerId)
            {
                selected = !selected;
                if (selected)
                    Select();
                else
                    Disselect();
            }
        }

        bool CheckClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (col.Raycast(ray, out hit, 100))
                    return true;
            }

            return false;
        }

        public virtual void CheckMove(Casilla box)
        {
        }

        public virtual IEnumerator Move(Casilla box, bool newTurn)
        {
            Disselect();

            if (FreePath(box))
            {
                Debug.Log("MOVIENDO " + name + " a " + box.name);
                if (box.currentPiece != null)   // CASILLA NO VACÍA
                {
                    Debug.Log("COMER PIEZA EN POSICION " + box.coordinates);

                    // Die effect, replace piece in box
                    if (!box.currentPiece.shield)
                        StartCoroutine(DieEffect(box, newTurn, true));
                    else
                    {
                        box.currentPiece.SetShield(false);
                        controller.newTurn();
                    }
                }
                else
                    StartCoroutine(MoveToBox(box, newTurn));

                yield return null;
            }
            else
                Debug.Log("NO SE PUDO MOVER " + name + " a " + box.name);
        }

        public IEnumerator DieEffect(Casilla box, bool newTurn, bool destroy)
        {
            auxBool = false;
            float cont = 0;

            // Material set-up
            box.currentPiece.ChangeMat(dieMat);

            foreach (Renderer mesh in box.currentPiece.meshes)
                mesh.material.SetColor("_Color", box.currentPiece.originalColor);

            while (cont < 1)
            {
                foreach (Renderer mesh in box.currentPiece.meshes)
                    mesh.material.SetFloat("_Progress", cont);

                yield return new WaitForFixedUpdate();
                cont += .05f;
            }

            auxBool = true;

            // Replace piece in box
            if (destroy)
            {
                Destroy(box.currentPiece.gameObject);
                StartCoroutine(MoveToBox(box, newTurn));
            }


        }

        public IEnumerator SpawnEffect()
        {
            foreach (Renderer mesh in meshes)
                mesh.enabled = false;

            yield return new WaitForSeconds(0.5f);

            foreach (Renderer mesh in meshes)
                mesh.enabled = true;

            auxBool = false;
            float cont = 1;

            // Material set-up
            ChangeMat(dieMat);

            foreach (Renderer mesh in meshes)
                mesh.material.SetColor("_Color", originalColor);

            while (cont > 0)
            {
                foreach (Renderer mesh in meshes)
                    mesh.material.SetFloat("_Progress", cont);

                yield return new WaitForFixedUpdate();
                cont -= .05f;
            }

            ChangeMat(originalMat);
            auxBool = true;
        }

        public IEnumerator MoveToBox(Casilla box, bool newTurn)
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = new Vector3(box.transform.position.x, transform.position.y, box.transform.position.z);

            float i = 0.0f;
            float rate = 1.0f / 0.85f;  // El 0.3 es un valor para el tiempo que dura

            if (auxBool)
            {
                while (i < 1.0f)
                {
                    i += Time.fixedDeltaTime * rate;
                    this.transform.position = Vector3.Lerp(startPos, endPos, i);
                    yield return null;
                }

                currentBox.currentPiece = null;
                box.currentPiece = this;
                currentBox = box;

                currentBox.PiecePlaced();

                if (newTurn)
                {
                    controller.newTurn();
                }
            }
        }

        public void ChangeMat(Material newMat)
        {
            foreach (MeshRenderer mesh in meshes)
            {
                mesh.material = newMat;
            }

            actualMaterial = newMat;
            Debug.Log(actualMaterial);
        }

        public void LoadResources()
        {
            highlightMat = Resources.Load<Material>("Materials/Highlight_Mat");
            dieMat = Resources.Load<Material>("Materials/Die_Mat");

            shieldEffect = Resources.Load<GameObject>("Particles/Shield_PS");
        }

        public void Select()
        {
            selected = true;
            if (controller.selectedPiece)
                controller.selectedPiece.Disselect();

            controller.selectedPiece = this;
            ChangeMat(highlightMat);
        }

        public void Disselect()
        {
            selected = false;
            controller.selectedPiece = null;
            ChangeMat(originalMat);
        }

        public virtual bool FreePath(Casilla box)
        {
            return true;
        }

        public void SetShield(bool state)
        {

            if (instShield == null)
            {
                instShield = Instantiate(shieldEffect, transform.position, Quaternion.Euler(90, 0, 0));
                instShield.transform.parent = transform;
            }



            if (state)
            {
                instShield.GetComponent<ParticleSystem>().startColor = Color.green;
                //light.gameObject.SetActive(true);
                //light.color = Color.green;
                instShield.GetComponent<ParticleSystem>().loop = true;
                instShield.GetComponent<ParticleSystem>().emissionRate = 500;
                instShield.GetComponent<ParticleSystem>().Play();
                instShield.transform.parent = transform;
                shield = true;
            }
            else
            {
                instShield.GetComponent<ParticleSystem>().loop = false;
                //light.gameObject.SetActive(false);
                instShield.GetComponent<ParticleSystem>().emissionRate = 0;
                shield = false;
            }
        }

        public void PawnPromotion(GameObject piece)
        {
            if (name == "Pawn")
                StartCoroutine(GetComponent<Peon>().Promote(currentBox, piece));
        }
    }
}
