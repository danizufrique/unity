﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainCreator : MonoBehaviour
{
    Player player;
    Queue<GameObject> terrain;

    void Start()
    {
        player = Player.instance;
        terrain = new Queue<GameObject>();

        StartCoroutine(GenerateTerrain(1));
    }

    void Update()
    {
        
    }


    IEnumerator GenerateTerrain(float time)
    {
        yield return new WaitForSeconds(time);
        Vector2 pos = player.transform.position - new Vector3(0, player.GetComponent<CapsuleCollider2D>().size.y / 2, 0);
        for (int i = 0; i < 20; i++)
        {
            GameObject ground = new GameObject("Ground");
            Terrain groundScript = new Terrain(1);
            ground.AddComponent(groundScript);

            ground.transform.position = pos;
            pos += new Vector2(ground.size.x, 0);
            Debug.Log(ground.size.x);
        }
    }
}
