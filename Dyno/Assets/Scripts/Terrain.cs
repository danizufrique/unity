﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour
{
    public Vector2 size;
    public BoxCollider2D col;
    public Rigidbody2D rb;

    [SerializeField]
    Sprite[] sprites;
    SpriteRenderer renderer;
     

    public Terrain(int index)
    {

        renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = sprites[index];

        col = gameObject.AddComponent<BoxCollider2D>();
        size = col.size;
        rb = gameObject.AddComponent<Rigidbody2D>();
    }
}
