﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
  public string alias;
  public int maxHealth;
  public  int currentHealth;
  public int waitTurn;
  public int damage;
  public int size;
  public int id;
  public Cell cell;
  public Text HUB_Health;



  void Start(){
    currentHealth = maxHealth;
    HUB_Health.text = currentHealth.ToString();
  }

  public virtual void Attack(){
    Debug.Log(alias + "is attacking");
  }

  public void Heal(int hp){
    currentHealth += hp;
    HUB_Health.text = currentHealth.ToString();
  }
}
