﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
  public Vector2Int gridPosition;
  public Vector2Int gridPositionNormalized;
  public Vector3 worldPosition;
  public string name;
  public bool occupied;
  public Character host;

  public Cell(){
    name = "";
    gridPosition = Vector2Int.zero;
    worldPosition = Vector3.zero;
    occupied = false;
    host = null;
  }

  public Cell(Vector2Int tPos, Vector3 wPos){
    name = "";
    gridPosition = tPos;
    worldPosition = wPos;
    occupied = false;
    host = null;
  }

  public void FreeCell(){
    UnityEngine.Object.Destroy(host.gameObject);
    occupied = false;
    host = null;
  }
}
