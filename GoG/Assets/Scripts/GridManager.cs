﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridManager : MonoBehaviour
{
    // TABLE
    Tilemap tileMap = null;
    List<Cell> cells;
    Cell[,] grid;
    int filas = 0, columnas = 0;

    public Character King;
    public Character Wall;

    int xScale;

    void Start(){
      LoadResources();
      tileMap = GetComponent<Tilemap>();
      xScale = (gameObject.tag == "P1_Grid") ? 1 : -1;
      DetectTiles();
    }

    void LoadResources(){
      //Texture2D cursorTexture = Resources.Load<Texture2D>("Textures/Cursor");
      //Cursor.SetCursor(cursorTexture, Vector3.zero, CursorMode.Auto);
    }

    void DetectTiles(){
      // FIND TILES
      cells = new List<Cell>();

      for (int n = tileMap.cellBounds.xMin; n < tileMap.cellBounds.xMax; n++)
      {
        for (int p = tileMap.cellBounds.yMin; p < tileMap.cellBounds.yMax; p++)
        {
          Vector3Int localPlace = new Vector3Int(n, p, (int)tileMap.transform.position.y);
          Vector3 place = tileMap.CellToWorld(localPlace);
          place += new Vector3(0.48f, 0.48f, 0);

          if (tileMap.HasTile(localPlace))
          {
            Vector3Int cellPos = tileMap.WorldToCell(place);
            Cell cell = new Cell(new Vector2Int(cellPos.x, cellPos.y), place);
            cells.Add(cell);
          }
        }
      }

      // CALCULATE ROWS AND COLUMNS
      for(int i=0; i<cells.Count; i++){
        int x,y;
        x=cells[i].gridPosition.x;
        y=cells[i].gridPosition.y;

        bool found = false;
        for(int j=0; j<i && !found; j++){
          if(x == cells[j].gridPosition.x)
            found = true;
        }
        if(!found)
          columnas++;

        found = false;
        for(int j=0; j<i && !found; j++){
          if(y == cells[j].gridPosition.y)
            found = true;
        }
        if(!found)
          filas++;
      }

      // GET GRID
      grid = new Cell[filas, columnas];

      cells[0].name = "00";
      grid[0,0] = cells[0];

      int fila = 0, columna = 0;
      for(int i=1; i<cells.Count; i++){
        if(cells[i-1].gridPosition.x != cells[i].gridPosition.x)
          columna++;

        if(cells[i-1].gridPosition.y != cells[i].gridPosition.y)
          if(fila<filas-1)
            fila++;
          else
            fila = 0;

        cells[i].gridPositionNormalized = new Vector2Int(fila,columna);
        cells[i].name = fila.ToString() + columna.ToString();
        grid[fila, columna] = cells[i];
      }
    }

    // Devuelve coordenadas normalizadas de la casilla seleccionada
    bool GetMouseCellNormalized(ref Vector2Int sol){
      Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      Vector2Int coordinate = (Vector2Int)tileMap.WorldToCell(mouseWorldPos);

      bool found = false;
      for(int i=0; i<cells.Count && !found; i++){
        if(cells[i].gridPosition == coordinate){
          found = true;
          sol = cells[i].gridPositionNormalized;
        }
      }
      return found;
    }

    // Devuelve coordenadas normalizadas de la posición que puede ocupar un personaje dado su tamaño
    bool FindFreeCell(int size, ref Cell sol){
      int selectedColumn = -1;
      Vector2Int mouseCell = Vector2Int.zero;
      Vector3 pos = Vector3.zero;
      bool found = false;

      if(GetMouseCellNormalized(ref mouseCell)){  // Obtenemos coordenadas
        for(int i=0; i<grid.GetLength(1) && !found; i++){ // Obtenemos columna de la cuadrícula
          if(grid[0,i].gridPositionNormalized.y == mouseCell.y){
            selectedColumn = mouseCell.y;
            found = true;
          }
        }

        found = false;
        if(selectedColumn >= 0 && size == 1){ // Obtenemos la fila de la cuadrícula
          if(xScale == -1){ // P2
            for(int i=0; i<grid.GetLength(0) && !found; i++){
              if(grid[i,selectedColumn].occupied == false){
                sol = grid[i,selectedColumn];
                found = true;
                grid[i,selectedColumn].occupied = true;
              }
            }
          }else if(xScale == 1){  // P1
            for(int i=grid.GetLength(0)-1; i>=0 && !found; i--){
              if(grid[i,selectedColumn].occupied == false){
                sol = grid[i,selectedColumn];
                found = true;
                grid[i,selectedColumn].occupied = true;
              }
            }
          }
        }
      }

      return found;
    }

    // Coloca un personaje en la columna seleccionada
    void PlaceCharacter(ref Character c){
      Cell cell = new Cell();
      if(Input.GetMouseButtonDown(0) && FindFreeCell(1, ref cell)){
        Character instance = Instantiate(c, cell.worldPosition, Quaternion.identity);
        InitInstance(ref instance, ref cell);
        CheckWalls();
      }
    }

    // Inicializa el personaje de una casilla
    void InitInstance(ref Character instance, ref Cell cell){
      cell.occupied = true;
      string idS = cell.gridPositionNormalized.x.ToString() + cell.gridPositionNormalized.y.ToString();
      instance.transform.localScale = (xScale == 1) ? Vector3.one : new Vector3(-1,1,1);
      instance.id = int.Parse(idS);
      instance.gameObject.name = instance.alias + " " + idS;
      instance.transform.parent = transform;
      instance.cell = cell;
      cell.host = instance;
    }

    // Comprueba si hay 3 personajes en fila
    void CheckWalls(){
      for(int i=0; i<grid.GetLength(0); i++){
        for(int j=1; j<grid.GetLength(1)-1; j++){
          Cell cell = grid[i,j];
          Cell cellL = grid[i,j-1];
          Cell cellR = grid[i,j+1];

          Character c = cell.host;

          // Creamos y colocamos muros
          if(cell.occupied && cellL.occupied && cellR.occupied){
            if(cell.host.size == 1){
              if( (cellL.host.alias == c.alias) && (cellR.host.alias == c.alias) ){
                ReplaceForWall(ref cell);
                ReplaceForWall(ref cellL);
                ReplaceForWall(ref cellR);
              }
            }
          }
        }
      }
    }

    // Cambia la instancia de un personaje por un muro y comprueba si se puede combinar
    void ReplaceForWall(ref Cell cell){
      cell.FreeCell();
      bool combined = false;

      Vector2Int pos = cell.gridPositionNormalized;
      int playerMod = (xScale == 1) ? 1 : -1;

      // Vemos si en la siguiente casilla tiene otro muro
      if( (pos.x < grid.GetLength(0)-1 && xScale == 1) || (pos.x>0 && xScale == -1) ){
        Character posibleWall = grid[pos.x+playerMod, pos.y].host;
        if(posibleWall != null){
          if(posibleWall.alias == "Wall"){
            posibleWall.Heal(posibleWall.maxHealth);
            posibleWall.maxHealth += posibleWall.maxHealth;
            combined = true;
            Debug.Log("grid: " + grid[pos.x+playerMod, pos.y].host.currentHealth);
          }
        }
      }

      Debug.Log(combined);
      if(!combined){
        Debug.Log("polla: ");
        Character instance = Instantiate(Wall, cell.worldPosition, Quaternion.identity);
        InitInstance(ref instance, ref cell);
      }
    }

    void Update()
    {
      PlaceCharacter(ref King);
    }
}
